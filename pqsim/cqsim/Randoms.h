/**
 * @author Andrey Larionov
 */
#ifndef CQSIM_RANDOMS_H
#define CQSIM_RANDOMS_H

#include <random>
#include <vector>

namespace cqsim {

void *createEngine();
void *createEngineWith(unsigned seed);
void destroyEngine(void *engine);


struct MarkedInterval {
  double interval;
  int mark;
};


class RandomVariable;
class MarkedProcessVariable;


class Randoms {
  public:
    Randoms();
    Randoms(unsigned seed);
    ~Randoms();

    RandomVariable *createPhaseType(
      const std::vector<std::vector<double>>& s,
      const std::vector<double>& p);

    RandomVariable *createUniform(double a, double b);

    MarkedProcessVariable *createMarkedMap(
      const std::vector<std::vector<std::vector<double>>>& d,
      const std::vector<double>& p0);

  private:
    std::default_random_engine *engine_ = nullptr;
};


class RandomVariable {
  public:
    explicit RandomVariable(void *engine);
    virtual ~RandomVariable() = default;

    inline std::default_random_engine *engine() const { return engine_; }

    virtual double eval() = 0;
  private:
    std::default_random_engine *engine_ = nullptr;
};


class MarkedProcessVariable {
  public:
    explicit MarkedProcessVariable(void *engine);
    virtual ~MarkedProcessVariable() = default;

    inline std::default_random_engine *engine() const { return engine_; }

    virtual MarkedInterval eval() = 0;
  private:
    std::default_random_engine *engine_ = nullptr;
};


class UniformVariable : public RandomVariable {
  public:
    UniformVariable(void *engine, double a, double b);
    ~UniformVariable() override = default;

    double eval() override;
  private:
    std::uniform_real_distribution<double> dist_;
};

class PhaseTypeVariable : public RandomVariable {
  public:
    PhaseTypeVariable(
      void *engine,
      const std::vector<std::vector<double>>& s,
      const std::vector<double>& p);

    ~PhaseTypeVariable() override = default;

    double eval() override;
  private:
    std::vector<std::exponential_distribution<double>> states_;
    int absorbState_;
    std::discrete_distribution<int> p0_;
    std::vector<std::discrete_distribution<int>> transitions_;
};


class MarkedMarkovArrivalVariable : public MarkedProcessVariable {
  public:
    MarkedMarkovArrivalVariable(
      void *engine,
      const std::vector<std::vector<std::vector<double>>>& d,
      const std::vector<double>& p);

    ~MarkedMarkovArrivalVariable() override = default;

    MarkedInterval eval() override;
  private:
    std::vector<std::exponential_distribution<double>> states_;
    std::vector<std::discrete_distribution<int>> transitions_;
    size_t order_;
    size_t numMatrices_;
    int state_;
};

}

#endif //CQSIM_RANDOMS_H
