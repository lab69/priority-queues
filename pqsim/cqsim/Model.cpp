#include <iostream>
#include <vector>

#include "Model.h"
#include "Statistics.h"
#include "Marshal.h"

namespace cqsim {

Results Simulate(const Params& params, int maxPackets) {
    size_t numPacketTypes = params.services.size();
    auto randomsFactory = new Randoms;

    auto state = new State(
        params.services.size(), params.numServers, randomsFactory);
    auto journal = new Journal(numPacketTypes, params.numServers);

    // Initialize:
    initialize(state, journal, params);

    // Main loop:
    while (journal->numPacketsGenerated().value() < maxPackets) {
        int server = findMinServiceEndServer(state);
        if (server < 0 || state->serviceEnd[server] > state->nextArrivalTime) {
            state->time = state->nextArrivalTime;
            state->nextArrivalTime = -1;
            handleArrival(state, journal, params);
        } else {
            state->time = state->serviceEnd[server];
            state->serviceEnd[server] = -1;
            handleServiceEnd(server, state, journal, params);
        }
    }

    // Finalize:
    journal->commit(state->time);
    auto results = Results();
    writeResults(journal, &results);
    // std::cout << toYaml(results);

    delete state;
    delete journal;
    delete randomsFactory;

    return results;
}


void scheduleArrival(State *state, Journal *journal, const Params& params) {
    auto interval = params.arrival->eval();
    state->nextArrivalTime = state->time + interval.interval;
    state->nextArrivalType = interval.mark;
    journal->arrivals().record(interval.interval);
    journal->arrivalsOf(interval.mark).record(interval.interval);
    journal->numPacketsGenerated().inc();
}


void dropPacket(Packet *packet, Journal *journal) {
    journal->numDroppedOf(packet->type).inc();
    delete packet;
}


void initialize(State *state, Journal *journal, const Params& params) {
    scheduleArrival(state, journal, params);
}


void handleArrival(State *state, Journal *journal, const Params& params) {
    auto packet = new Packet;
    packet->createdAt = state->time;
    packet->type = state->nextArrivalType;
    int type = packet->type;
    scheduleArrival(state, journal, params);
    int server = findEmptyServer(state);
    size_t queueSize = state->queue.size();
    if (server < 0) {
        // No empty server found => enqueue or drop
        if (queueSize >= params.capacity) {
            dropPacket(packet, journal);
        }
        else if (state->u01->eval() >= params.enqueueProbs[type-1][queueSize]) {
            dropPacket(packet, journal);
        }
        else {
            enqueuePacket(state, journal, packet);
            journal->numAcceptedOf(type).inc();
        }
    }
    else {
        // Found empty server => start serving
        startService(packet, server, state, journal, params);
        journal->numAcceptedOf(type).inc();
    }

    // Update statistics
    updateSizeStats(state, journal, params);
}


void handleServiceEnd(
        int server,
        State *state,
        Journal *journal,
        const Params& params) {
    // Handle service end event
    auto packet = state->servers[server];
    state->servers[server] = nullptr;

    // Record response time (end-to-end delay):
    double responseInterval = state->time - packet->createdAt;
    journal->responseTime().record(responseInterval);
    journal->responseTimeOf(packet->type).record(responseInterval);

    // Remove the packet
    delete packet;

    // If there are other packet in the queue, start serving them:
    if (!state->queue.empty()) {
        packet = state->queue.front();
        state->queue.pop_front();
        startService(packet, server, state, journal, params);
    } else {
        journal->serverDuty(server).record(state->time, 0);
    }

    // Update statistics
    updateSizeStats(state, journal, params);
}


void startService(
        Packet *packet,
        int server,
        State *state,
        Journal *journal,
        const Params& params) {
    // Start serving a packet
    auto interval = params.services[packet->type - 1]->eval();
    state->serviceEnd[server] = state->time + interval;
    state->servers[server] = packet;

    // Record service statistics
    journal->services().record(interval);
    journal->servicesOf(packet->type).record(interval);

    // Record packet waiting time
    double waitTime = state->time - packet->createdAt;
    journal->waitTime().record(waitTime);
    journal->waitTimeOf(packet->type).record(waitTime);

    // Record server duty
    journal->serverDuty(server).record(state->time, packet->type);
}


void enqueuePacket(State *state, Journal *journal, Packet *packet) {
    // Find the place and put the packet to the queue:
    auto pos = findInsertIndex(packet->type, state->queue);
    state->queue.insert(pos, packet);
}

/*
 * Update overall and per-packet system size and queue size.
 */
void updateSizeStats(State *state, Journal *journal, const Params& params) {
    auto time = state->time;
    auto numQueued = state->queue.size();
    auto numServing = countPackets(
        state->servers.begin(), state->servers.end());
    journal->systemSize().record(state->time, numQueued + numServing);
    journal->queueSize().record(state->time, numQueued);
    journal->busyServers().record(state->time, numServing);
    for (int type = 1; type <= params.services.size(); ++type) {
        numQueued = countPackets(
            state->queue.begin(), state->queue.end(), type);
        numServing = countPackets(
            state->servers.begin(), state->servers.end(), type);
        journal->systemSizeOf(type).record(state->time, numQueued + numServing);
        journal->queueSizeOf(type).record(state->time, numQueued);
    }
}


int findMinServiceEndServer(State *state) {
    int minServer = -1;
    double minTime = 0.0;
    for (size_t server = 0; server < state->servers.size(); ++server) {
        double serviceEndAt = state->serviceEnd[server];
        Packet *packet = state->servers[server];
        if (packet && (minServer < 0 || minTime > serviceEndAt)) {
            minTime = serviceEndAt;
            minServer = server;
        }
    }
    return minServer;
}


int findEmptyServer(State *state) {
    for (int server = 0; server < state->servers.size(); ++server) {
        if (!(state->servers[server])) {
            return server;
        }
    }
    return -1;
}

std::list<Packet*>::const_iterator
findInsertIndex(int packetType, const std::list<Packet*>& queue) {
    auto pos = queue.begin();
    while (pos != queue.end() && (*pos)->type <= packetType) {
        pos++;
    }
    return pos;
}

void writeResults(Journal *journal, Results *results) {
    size_t numPacketTypes = journal->numPacketTypes();
    size_t numServers = journal->numServers();

    // Write statistics those don't depend on particular packet types:
    results->systemSize = SizeDist(journal->systemSize().pmf());
    results->queueSize = SizeDist(journal->queueSize().pmf());
    results->busyServers = SizeDist(journal->busyServers().pmf());
    results->waitTime = VarData(journal->waitTime());
    results->responseTime = VarData(journal->responseTime());
    results->arrivals = VarData(journal->arrivals());
    results->services = VarData(journal->services());

    // Write statistics depending on packet types:
    for (size_t i = 1; i <= numPacketTypes; ++i) {
        results->systemSizeOf[i] = SizeDist(journal->systemSizeOf(i).pmf());
        results->queueSizeOf[i] = SizeDist(journal->queueSizeOf(i).pmf());
        results->waitTimeOf[i] = VarData(journal->waitTimeOf(i));
        results->responseTimeOf[i] = VarData(journal->responseTimeOf(i));
        results->arrivalsOf[i] = VarData(journal->arrivalsOf(i));
        results->servicesOf[i] = VarData(journal->servicesOf(i));
    }

    // Compute server duty PMF:
    auto serverDutyPmf = std::vector<double>(numPacketTypes + 1, 0.0);
    for (size_t i = 0; i < numServers; ++i) {
        std::vector<double> pmf = journal->serverDuty(i).pmf();
        size_t nIters = std::min(serverDutyPmf.size(), pmf.size());
        for (size_t j = 0; j < nIters; ++j) {
            serverDutyPmf[j] += pmf[j];
        }
    }
    if (numServers > 1) {
        for (size_t i = 0; i < serverDutyPmf.size(); ++i) {
            serverDutyPmf[i] /= numServers;
        }
    }
    results->serverDuty = SizeDist(serverDutyPmf);

    // Compute loss probability:
    int nAcceptedTotal = 0;
    int nDroppedTotal = 0;
    for (size_t i = 1; i <= numPacketTypes; ++i) {
        int nAccepted = journal->numAcceptedOf(i).value();
        int nDropped = journal->numDroppedOf(i).value();
        int nPackets = nAccepted + nDropped;
        results->lossProbOf[i] = nPackets > 0 ?
            static_cast<double>(nDropped) / nPackets : 0.0;
        nAcceptedTotal += nAccepted;
        nDroppedTotal += nDropped;
    }
    int nPacketsTotal = nAcceptedTotal + nDroppedTotal;
    results->lossProb = nPacketsTotal > 0 ?
        static_cast<double>(nDroppedTotal) / nPacketsTotal : 0.0;
}

//
// State methods
// ---------------------------------------------------------------------------
State::State(size_t numPacketTypes, size_t numServers, Randoms *factory) {
    time = 0.0;
    servers = std::vector<Packet*>(numServers, nullptr);
    queue = std::list<Packet*>();
    serviceEnd = std::vector<double>(numServers, -1.0);
    nextArrivalTime = -1;
    nextArrivalType = 0;
    u01 = factory->createUniform(0, 1);
}

State::~State() {
    for (auto pkt: servers) {
        delete pkt;
    }
    for (auto pkt: queue) {
        delete pkt;
    }
    delete u01;
}

}
