/**
 * @author Andrey Larionov
 */
#ifndef CQSIM_JOURNAL_H
#define CQSIM_JOURNAL_H

#include <map>
#include <vector>
#include "Statistics.h"


namespace cqsim {

class Journal : public Object {
  public:
    Journal(size_t numPacketTypes, size_t numServers,
            size_t numMoments = 3, size_t windowSize = 100);
    ~Journal() override;

    TimeSizeSeries& systemSize() { return *systemSize_; }
    TimeSizeSeries& systemSizeOf(int type) { return *(systemSizeOf_.at(type)); }
    TimeSizeSeries& queueSize() { return *queueSize_; }
    TimeSizeSeries& queueSizeOf(int type) { return *(queueSizeOf_.at(type)); }
    TimeSizeSeries& busyServers() { return *busyServers_; }
    TimeSizeSeries& serverDuty(int server) { return *(serverDuty_.at(server)); }
    Series& services() { return *services_; }
    Series& servicesOf(int type) { return *(servicesOf_.at(type)); }
    Series& arrivals() { return *arrivals_; }
    Series& arrivalsOf(int type) { return *(arrivalsOf_.at(type)); }
    Series& waitTime() { return *waitTime_; }
    Series& waitTimeOf(int type) { return *(waitTimeOf_.at(type)); }
    Series& responseTime() { return *responseTime_; }
    Series& responseTimeOf(int type) { return *(responseTimeOf_.at(type)); }
    Counter& numAcceptedOf(int type) { return *(numAcceptedOf_.at(type)); }
    Counter& numDroppedOf(int type){ return *(numDroppedOf_.at(type)); }
    Counter& numPacketsGenerated() { return *numPacketsGenerated_; }

    void commit(double time);

    size_t numPacketTypes() const { return numPacketTypes_; }
    size_t numServers() const { return numServers_; }

    std::string toString() const override;

  private:
    size_t numPacketTypes_;
    size_t numServers_;
    TimeSizeSeries* systemSize_;
    std::map<int, TimeSizeSeries*> systemSizeOf_;
    TimeSizeSeries* queueSize_;
    std::map<int, TimeSizeSeries*> queueSizeOf_;
    TimeSizeSeries* busyServers_;
    std::vector<TimeSizeSeries*> serverDuty_;
    Series* services_;
    std::map<int, Series*> servicesOf_;
    Series* arrivals_;
    std::map<int, Series*> arrivalsOf_;
    Series* waitTime_;
    std::map<int, Series*> waitTimeOf_;
    Series* responseTime_;
    std::map<int, Series*> responseTimeOf_;
    std::map<int, Counter*> numAcceptedOf_;
    std::map<int, Counter*> numDroppedOf_;
    Counter *numPacketsGenerated_;
};


}

#endif // CQSIM_JOURNAL_H