/**
 * @author Andrey Larionov
 */
#include "Randoms.h"
#include <chrono>
#include <algorithm>
#include <stdexcept>

namespace cqsim {

// Internal helpers
// ---------------------------------------------------------------------------
bool isNegative(double x) { return x <= 0; }
bool isNonPositive(double x) { return x < 0; }


// Functions
// ---------------------------------------------------------------------------
void *createEngine() {
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    return createEngineWith(seed);
}

void *createEngineWith(unsigned seed) {
    return static_cast<void*>(new std::default_random_engine(seed));
}

void destroyEngine(void *engine) {
    delete static_cast<std::default_random_engine*>(engine);
}


// Randoms
// --------------------------------------------------------------------------
Randoms::Randoms()
: engine_(static_cast<std::default_random_engine*>(createEngine()))
{}

Randoms::Randoms(unsigned seed)
: engine_(static_cast<std::default_random_engine*>(createEngineWith(seed)))
{}

Randoms::~Randoms() {
    destroyEngine(static_cast<void*>(engine_));
}

RandomVariable *Randoms::createPhaseType(
        const std::vector<std::vector<double>>& s,
        const std::vector<double>& p) {
    return new PhaseTypeVariable(engine_, s, p);
}

MarkedProcessVariable *Randoms::createMarkedMap(
        const std::vector<std::vector<std::vector<double>>>& d,
        const std::vector<double>& p0) {
    return new MarkedMarkovArrivalVariable(engine_, d, p0);
}

RandomVariable *Randoms::createUniform(double a, double b) {
    return new UniformVariable(engine_, a, b);
}


// RandomVariable and MarkedProcessVariable
// ---------------------------------------------------------------------------
RandomVariable::RandomVariable(void *engine)
: engine_(static_cast<std::default_random_engine*>(engine)){}

MarkedProcessVariable::MarkedProcessVariable(void *engine)
: engine_(static_cast<std::default_random_engine*>(engine)){}


// UniformVariable
// ---------------------------------------------------------------------------
UniformVariable::UniformVariable(void *engine, double a, double b)
: RandomVariable(engine) {
    dist_ = std::uniform_real_distribution<>(a, b);
}

double UniformVariable::eval() {
    return dist_(*engine());
}


// PhaseTypeVariable
// ---------------------------------------------------------------------------
PhaseTypeVariable::PhaseTypeVariable(
        void *engine,
        const std::vector<std::vector<double>>& s,
        const std::vector<double>& p
) : RandomVariable(engine) {
    auto order = s.size();
    auto order_of = [order](const std::vector<double>& x) { return x.size(); };

    // 1) Validate sizes
    if (std::any_of(s.begin(), s.end(),
            [=](const std::vector<double>& x) {return x.size() != order; })) {
        throw std::runtime_error("all S rows must have the same order");
    }
    if (p.size() != order) {
        throw std::runtime_error("P vector must have the same order as S");
    }

    // 2) Validate that non-diagonal elements are non-negative, and
    //    diagonal elements are non-positive;
    //    validate that P elements are non-negative

    // FIXME: add flag to bypass validation
    // COMMENTED VALIDATION TO AVOID ERRORS IN CYTHON CALLS.
    // MATRICES ARE ALREADY VALIDATED IN PYTHON CODE.

//    for (size_t i = 0; i < order; ++i) {
//        // S[i] row:
//        for (size_t j = 0; j < order; ++j) {
//            if (i == j && s[i][j] > 0) {
//                throw std::runtime_error(
//                    "diagonal elements of S must be non-positive");
//            } else if (i != j && s[i][j] < 0) {
//                throw std::runtime_error(
//                    "non-diagonal elements of S must be non-negative");
//            }
//        }
//        // P[i]:
//        if (p[i] < 0) {
//            throw std::runtime_error("P elements must be non-negative");
//        }
//    }

    // 3) Compute state exit and absorbing rates
    std::vector<double> absorbRates;
    std::vector<double> rates;
    for (size_t i = 0; i < order; ++i) {
        double rowSum = std::accumulate(s[i].begin(), s[i].end(), 0.0);
//        if (rowSum > 0) {
//            throw std::runtime_error("row sums of S must be non-positive");
//        }
        rates.push_back(-s[i][i]);
        absorbRates.push_back(-rowSum);
    }

    // 4) Compute transition probabilities
    std::vector<std::vector<double>> transitionProbs(order);
    for (size_t i = 0; i < order; ++i) {
        std::vector<double>& transRow = transitionProbs.at(i);
        const std::vector<double>& ratesRow = s.at(i);
        double rate = rates[i];
        for (size_t j = 0; j < order; ++j) {
            double prob = (i == j) ? 0.0 : ratesRow[j] / rate;
            transRow.push_back(prob);
        }
        // Add last transition: from (i) to absorbing state (order)
        transRow.push_back(absorbRates[i] / rate);
    }

    // 5) Build and store internal random variables for transitions and time:
    absorbState_ = order;
    for (size_t i = 0; i < order; i++) {
        states_.push_back(std::exponential_distribution<double>(rates[i]));
        transitions_.push_back(std::discrete_distribution<int>(
            transitionProbs[i].begin(), transitionProbs[i].end()));
    }
    p0_ = std::discrete_distribution<int>(p.begin(), p.end());
}

double PhaseTypeVariable::eval() {
    auto enginePtr = engine();
    int state = p0_(*enginePtr);
    double value = 0.0;
    while (state != absorbState_) {
        value += states_[state](*enginePtr);
        state = transitions_[state](*enginePtr);
    }
    return value;
}


// MarkedMarkovArrivalVariable
// ---------------------------------------------------------------------------
MarkedMarkovArrivalVariable::MarkedMarkovArrivalVariable(
        void *engine,
        const std::vector<std::vector<std::vector<double>>>& d,
        const std::vector<double>& p)
: MarkedProcessVariable(engine) {
    auto numMatrices = d.size();
    if (numMatrices <= 1) {
        throw std::runtime_error("at least two matrices D0, D1 required");
    }
    auto order = d[0].size();

    // 1) Validate that all matrices have the same order
    for (size_t i = 0; i < numMatrices; i++) {
        const std::vector<std::vector<double>>& mat = d[i];
        if (mat.size() != order || std::any_of(mat.begin(), mat.end(),
                [=](const std::vector<double>& x) {return x.size() != order; }))
        {
            throw std::runtime_error("all Di must be square with same order");
        }
    }
    if (p.size() != order) {
        throw std::runtime_error("initial PMF order must be same as of Di");
    }

    // 2) Validate that D0 diagonal elements are non-positive and the
    //    rest of Di matrices elements are non-negative

    // FIXME: add flag to bypass validation
    // COMMENTED THIS: CHECKING IS MADE IN PYTHON
    // IN PARALLEL EXECUTION IT IS HARD TO CATCH C++ ERRORS FROM CYTHON

//    for (size_t k = 0; k < numMatrices; k++) {
//        const std::vector<std::vector<double>>& mat = d[k];
//        for (size_t i = 0; i < order; i++) {
//            for (size_t j = 0; j < order; j++) {
//                if ((i != j || k > 0) && mat[i][j] < 0) {
//                    throw std::runtime_error("negative matrix element found");
//                } else if (k == 0 && i == j && mat[i][j] > 0) {
//                    throw std::runtime_error("positive D0 diagonal element");
//                }
//            }
//        }
//    }

    // 3) Validate that for each row K sums of Di[K] over all i equal 0.0

    // FIXME: add flag to bypass validation

//    for (size_t i = 0; i < order; ++i) {
//        double sum = 0.0;
//        for (size_t k = 0; k < numMatrices; ++k) {
//            sum += std::accumulate(d[k][i].begin(), d[k][i].end(), 0.0);
//        }
//        if (std::abs(sum) > 1e-3) {
//            throw std::runtime_error("sum of Dk columns must be zero");
//        }
//    }

    // 4) Extract rates as negated D0 diagonal
    std::vector<double> rates(order, 0.0);
    for (size_t i = 0; i < order; ++i) {
        rates[i] = std::abs(d[0][i][i]);
    }

    // 5) Build transitions concatenated matrix
    std::vector<std::vector<double>> transProbs(order);
    for (size_t i = 0; i < order; ++i) {
        transProbs.at(i).resize(order * numMatrices, 0.0);
        double rate = rates[i];
        for (size_t k = 0; k < numMatrices; ++k) {
            const std::vector<double>& row = d[k][i];
            size_t base = k * order;
            for (size_t j = 0; j < order; ++j) {
                double el = row[j];
                double prob = (k == 0 && i == j) ? 0.0 : el / rate;
                transProbs[i][base + j] = prob;
            }
        }
    }

    // 6) Build and store distributions
    for (size_t i = 0; i < order; i++) {
        states_.push_back(std::exponential_distribution<double>(rates[i]));
        transitions_.push_back(std::discrete_distribution<int>(
            transProbs[i].begin(), transProbs[i].end()));
    }
    auto initProbs = std::discrete_distribution<int>(p.begin(), p.end());
    state_ = initProbs(*static_cast<std::default_random_engine*>(engine));
    order_ = order;
    numMatrices_ = numMatrices;
}

MarkedInterval MarkedMarkovArrivalVariable::eval() {
    auto enginePtr = engine();
    double value = 0.0;
    int packetType = 0;
    while (packetType == 0) {
        int column = transitions_[state_](*enginePtr);
        value += states_[state_](*enginePtr);
        state_ = column % order_;
        packetType = column / order_;
    }
    return MarkedInterval{value, packetType};
}

}
