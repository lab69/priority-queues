#include <iostream>
#include <algorithm>
#include "Randoms.h"
#include "Model.h"
#include "Marshal.h"

using namespace cqsim;

void generateSomMmapValues();
void generateSomePhValues();


int main(int argc, char **argv) {
    auto factory = new Randoms();
    auto params = Params();
    int CAPACITY = 4;
    params.arrival = factory->createMarkedMap(
        // {{{-2.0}}, {{1.5}}, {{0.5}}},
        {{{-2.0}}, {{2.0}}},
        {1.0}
    );
    params.services = {
        factory->createPhaseType({{-5.0}}, {1.0})
        // factory->createPhaseType({{-10.0}}, {1.0})
    };
    params.enqueueProbs = {
        std::vector<double>(CAPACITY + 1, 1.0)
        // std::vector<double>(CAPACITY + 1, 1.0)
    };
    params.capacity = CAPACITY;
    params.numServers = 1;

    auto results = Simulate(params, 100000);
    std::cout << toYaml(results) << std::endl;

    delete factory;
    delete params.arrival;
    for (auto service: params.services) {
        delete service;
    }
    // generateSomePhValues();
    // generateSomMmapValues();
    // std::cout << "Hello from cqsim!" << std::endl;
}


void generateSomePhValues() {
    auto factory = new Randoms();
    auto ph = factory->createPhaseType({{-5.0}}, {1.0});

    const size_t NUM_PACKETS = 100000;
    std::vector<double> intervals;
    for (int i = 0; i < NUM_PACKETS; ++i) {
        intervals.push_back(ph->eval());
    }
    std::cout << "Generated " << NUM_PACKETS << " PH packets." << std::endl;
    double avg = std::accumulate(intervals.begin(), intervals.end(), 0.0) /
        intervals.size();
    std::cout << "- average: " << avg << std::endl;

    delete ph;
    delete factory;
}


void generateSomMmapValues() {
    auto factory = new Randoms();
    auto mmap = factory->createMarkedMap(
        {{{-2.0}}, {{1.5}}, {{0.5}}},
        {1.0}
    );
    const size_t NUM_TYPES = 2;
    const size_t NUM_PACKETS = 100000;
    std::vector<int> hits(NUM_TYPES, 0);
    std::vector<std::vector<double>> intervals(NUM_TYPES);
    for (int i = 0; i < NUM_PACKETS; ++i) {
        auto ret = mmap->eval();
        hits[ret.mark - 1]++;
        intervals[ret.mark - 1].push_back(ret.interval);
    }
    std::cout << "Generated " << NUM_PACKETS << " MMAP packets." << std::endl;
    for (size_t k = 0; k < NUM_TYPES; ++k) {
        double avg = std::accumulate(
                intervals[k].begin(), intervals[k].end(), 0.0
            ) / intervals[k].size();

        std::cout << "- type " << (k + 1) << ": " <<
            "p=" << static_cast<double>(hits[k]) / NUM_PACKETS << ", " <<
            "m=" << avg << std::endl;
    }
    delete mmap;
    delete factory;
}
