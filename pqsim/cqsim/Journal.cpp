#include "Journal.h"
#include <sstream>

namespace cqsim {

Journal::Journal(
    size_t numPacketTypes, size_t numServers,
    size_t numMoments, size_t windowSize
) : numPacketTypes_(numPacketTypes), numServers_(numServers) {
    systemSize_ = new TimeSizeSeries(0.0, 0);
    queueSize_ = new TimeSizeSeries(0.0, 0);
    busyServers_ = new TimeSizeSeries(0.0, 0);
    services_ = new Series(numMoments, windowSize);
    arrivals_ = new Series(numMoments, windowSize);
    waitTime_ = new Series(numMoments, windowSize);
    responseTime_ = new Series(numMoments, windowSize);
    for (size_t i = 1; i <= numPacketTypes; ++i) {
        systemSizeOf_[i] = new TimeSizeSeries(0.0, 0);
        queueSizeOf_[i] = new TimeSizeSeries(0.0, 0);
        arrivalsOf_[i] = new Series(numMoments, windowSize);
        servicesOf_[i] = new Series(numMoments, windowSize);
        waitTimeOf_[i] = new Series(numMoments, windowSize);
        responseTimeOf_[i] = new Series(numMoments, windowSize);
        numAcceptedOf_[i] = new Counter(0);
        numDroppedOf_[i] = new Counter(0);
    }
    for (size_t i = 0; i < numServers; ++i) {
        serverDuty_.push_back(new TimeSizeSeries(0.0, 0));
    }
    numPacketsGenerated_ = new Counter(0);
}

Journal::~Journal() {
    delete systemSize_;
    delete queueSize_;
    delete busyServers_;
    delete services_;
    delete arrivals_;
    delete waitTime_;
    delete responseTime_;
    delete numPacketsGenerated_;
    for (size_t i = 1; i <= numPacketTypes_; ++i) {
        delete systemSizeOf_[i];
        delete queueSizeOf_[i];
        delete servicesOf_[i];
        delete arrivalsOf_[i];
        delete waitTimeOf_[i];
        delete responseTimeOf_[i];
        delete numAcceptedOf_[i];
        delete numDroppedOf_[i];
    }
    for (size_t i = 0; i < numServers_; ++i) {
        delete serverDuty_[i];
    }
}

void Journal::commit(double time) {
    systemSize_->commit(time);
    queueSize_->commit(time);
    busyServers_->commit(time);
    services_->commit();
    arrivals_->commit();
    waitTime_->commit();
    responseTime_->commit();
    for (size_t i = 1; i <= numPacketTypes_; ++i) {
        systemSizeOf_[i]->commit(time);
        queueSizeOf_[i]->commit(time);
        servicesOf_[i]->commit();
        arrivalsOf_[i]->commit();
        waitTimeOf_[i]->commit();
        responseTimeOf_[i]->commit();
    }
    for (size_t i = 0; i < numServers_; ++i) {
        serverDuty_[i]->commit(time);
    }
}

template<typename T>
void writeTypedValues_(
    std::stringstream* ssPtr,
    const std::string& title,
    T *obj,
    const std::map<int, T*>& aMap,
    size_t numPacketTypes
) {
    (*ssPtr) << "- " << title << ":" << std::endl;
    if (obj != nullptr) {
        (*ssPtr) << "\ttotal: " << obj->toString() << std::endl;
    }
    for (size_t i = 1; i <= numPacketTypes; ++i) {
        (*ssPtr) << "\t" << i << ": " << aMap.at(i)->toString() << std::endl;
    }
}

std::string Journal::toString() const {
    std::stringstream ss;

    ss << "Journal:" << std::endl;
    writeTypedValues_<TimeSizeSeries>(
        &ss, "system size", systemSize_, systemSizeOf_, numPacketTypes_);
    writeTypedValues_<TimeSizeSeries>(
        &ss, "queue size", systemSize_, systemSizeOf_, numPacketTypes_);
    ss << "- busy servers: " << busyServers_->toString() << std::endl;
    ss << "- server duty:" << std::endl;
    for (size_t i = 0; i < numServers_; ++i) {
        ss << "\t" << i << ": " << serverDuty_[i]->toString() << std::endl;
    }
    writeTypedValues_<Series>(
        &ss, "arrivals", arrivals_, arrivalsOf_, numPacketTypes_);
    writeTypedValues_<Series>(
        &ss, "services", services_, servicesOf_, numPacketTypes_);
    writeTypedValues_<Series>(
        &ss, "wait time", waitTime_, waitTimeOf_, numPacketTypes_);
    writeTypedValues_<Series>(
        &ss, "response time", responseTime_, responseTimeOf_, numPacketTypes_);
    writeTypedValues_<Counter>(
        &ss, "num accepted packets", nullptr, numAcceptedOf_, numPacketTypes_);
    writeTypedValues_<Counter>(
        &ss, "num dropped packets", nullptr, numDroppedOf_, numPacketTypes_);
    ss << "- num generated packets: " << numPacketsGenerated_->value()
        << std::endl;

    return ss.str();
}

}
