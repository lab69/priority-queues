/**
 * @author Andrey Larionov
 */
#include <map>
#include "Marshal.h"

namespace cqsim {


template<typename T>
void _writeYamlValue(
    std::stringstream& ss,
    const T& value,
    const std::string& label,
    const std::string& indent,
    bool inlined = false
) {
    ss << indent << label << ":";
    if (!inlined) {
        ss << std::endl << toYaml(value, indent + "    ", false);
    } else {
        ss << " " << toYaml(value);
    }
}


template<typename T>
void _writeYamlValueMap(
    std::stringstream& ss,
    const std::map<int, T>& valueMap,
    const std::string& label,
    const std::string& indent,
    const std::string& keyLabel = "type"
) {
    auto nextIndent = indent + "    ";
    auto nextNextIndent = nextIndent + "  ";
    ss << indent << label << ":" << std::endl;
    if (keyLabel == "") {
        for (auto& kv: valueMap) {
            ss << nextIndent << kv.first << ": " << toYaml(kv.second)
               << std::endl;
        }
    } else {
        for (auto& kv: valueMap) {
            ss << nextIndent << "- " << keyLabel << ": " << kv.first
               << std::endl << toYaml(kv.second, nextNextIndent, false);
        }
    }
}


std::string toYaml(const Results& value, const std::string& indent, bool first)
{
    std::stringstream ss;
    if (first) {
        ss << "---" << std::endl;
    }
    _writeYamlValue(ss, value.systemSize, "systemSize", indent);
    _writeYamlValueMap(ss, value.systemSizeOf, "systemSizeOf", indent);
    _writeYamlValue(ss, value.queueSize, "queueSize", indent);
    _writeYamlValueMap(ss, value.queueSizeOf, "queueSizeOf", indent);
    _writeYamlValue(ss, value.busyServers, "busyServers", indent);
    _writeYamlValue(ss, value.serverDuty, "serverDuty", indent);
    _writeYamlValue(ss, value.waitTime, "waitTime", indent);
    _writeYamlValueMap(ss, value.waitTimeOf, "waitTimeOf", indent);
    _writeYamlValue(ss, value.responseTime, "responseTime", indent);
    _writeYamlValueMap(ss, value.responseTimeOf, "responseTimeOf", indent);
    _writeYamlValue(ss, value.arrivals, "arrivals", indent);
    _writeYamlValueMap(ss, value.arrivalsOf, "arrivalsOf", indent);
    _writeYamlValue(ss, value.services, "services", indent);
    _writeYamlValueMap(ss, value.servicesOf, "servicesOf", indent);
    _writeYamlValue(ss, value.lossProb, "lossProb", indent, true);
    _writeYamlValueMap(ss, value.lossProbOf, "lossProbOf", indent, "");

    return ss.str();
}

std::string toYaml(
        const SizeDist& value,
        const std::string& indent,
        bool first) {
    std::stringstream ss;
    if (first)
        ss << "---" << std::endl;
    ss << indent << "mean: " << value.mean() << std::endl
        << indent << "std: " << value.std() << std::endl
        << indent << "var: " << value.var() << std::endl
        << indent << "pmf: " << toYaml(value.pmf()) << std::endl;
    return ss.str();
}

std::string toYaml(
        const VarData& value,
        const std::string& indent,
        bool first) {
    std::stringstream ss;
    if (first)
        ss << "---" << std::endl;
    ss << indent << "mean: " << value.mean << std::endl
        << indent << "std: " << value.std << std::endl
        << indent << "var: " << value.var << std::endl
        << indent << "count: " << value.count << std::endl
        << indent << "moments: " << toYaml(value.moments) << std::endl;
    return ss.str();
}

std::string toYaml(
    const std::vector<double>& value,
    const std::string& indent,
    bool first
) {
    return cqsim::toString(value);
}


}