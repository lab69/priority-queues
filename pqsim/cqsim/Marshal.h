/**
 * Module with routines for marshalling and unmarshalling of model data
 * and results.
 *
 * Formats for marshalling results:
 *
 * - YAML
 *
 * Unmarshalling is not implemented yet.
 *
 * @author Andrey Larionov
 */
#ifndef CQSIM_MARSHAL_H
#define CQSIM_MARSHAL_H

#include "Model.h"

namespace cqsim {

std::string toYaml(
    const Results& value,
    const std::string& indent = "",
    bool first = true);

std::string toYaml(
    const VarData& value,
    const std::string& indent = "",
    bool first = false);

std::string toYaml(
    const SizeDist& value,
    const std::string& indent = "",
    bool first = false);

template<typename T>
std::string toYaml(T value, const std::string& indent = "", bool first = false)
{
    return std::to_string(value);
}

std::string toYaml(
    const std::vector<double>& value,
    const std::string& indent = "",
    bool first = false
);

}

#endif //CQSIM_MARSHAL_H