//
// @author Andrey Larionov
//
#ifndef CQSIM_MODEL_H
#define CQSIM_MODEL_H

#include <vector>
#include <map>
#include <list>

#include "Base.h"
#include "Randoms.h"
#include "Statistics.h"
#include "Journal.h"


namespace cqsim {

struct Params {
    MarkedProcessVariable *arrival;
    std::vector<RandomVariable*> services;
    std::vector<std::vector<double>> enqueueProbs;
    size_t capacity;
    size_t numServers;
};


struct Results {
    double time;

    SizeDist systemSize;
    std::map<int, SizeDist> systemSizeOf;
    SizeDist queueSize;
    std::map<int, SizeDist> queueSizeOf;
    SizeDist busyServers;
    SizeDist serverDuty;
    VarData waitTime;
    std::map<int, VarData> waitTimeOf;
    VarData responseTime;
    std::map<int, VarData> responseTimeOf;
    VarData arrivals;
    std::map<int, VarData> arrivalsOf;
    VarData services;
    std::map<int, VarData> servicesOf;
    double lossProb;
    std::map<int, double> lossProbOf;
};


struct Packet {
    double createdAt;
    int type;
};


struct State {
    State(size_t numPacketTypes, size_t numServers, Randoms *factory);
    virtual ~State();

    double time;
    std::vector<Packet*> servers;
    std::list<Packet*> queue;
    std::vector<double> serviceEnd;
    double nextArrivalTime;
    double nextArrivalType;

    RandomVariable *u01;
};


// EXPORTED FUNCTIONS
// ------------------

Results Simulate(const Params& params, int maxPackets);


// MUTABLE HELPERS
// -----------------
//
// Functions those handle events, push or pop packets from the queue,
// write statistics and do other stuff that change current state or journal.

void writeResults(Journal *journal, Results *results);
void initialize(State *state, Journal *journal, const Params& params);
void handleArrival(State *state, Journal *journal, const Params& params);

void handleServiceEnd(
    int server,
    State *state,
    Journal *journal,
    const Params& params);

void startService(
    Packet *packet,
    int server,
    State *state,
    Journal *journal,
    const Params& params);

void enqueuePacket(State *state, Journal *journal, Packet *packet);
void updateSizeStats(State *state, Journal *journal, const Params& params);

// IMMUTABLE HELPERS
// -----------------
//
// Various search functions, getters and other helper routines
// those do not change state or write anything to journal

int findMinServiceEndServer(State *state);
int findEmptyServer(State *state);

std::list<Packet*>::const_iterator
findInsertIndex(int packetType, const std::list<Packet*>& queue);


template<class Iterator>
int countPackets(Iterator first, Iterator last, int type = 0) {
    int num = 0;
    while (first != last) {
        Packet *packet = *first++;
        if (packet != nullptr && (type == 0 || packet->type == type)) {
            ++num;
        }
    }
    return num;
}

}

#endif //CQSIM_MODEL_H