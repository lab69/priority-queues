import json
from dataclasses import dataclass, field
from dataclasses_json import dataclass_json, config, LetterCase
from typing import Sequence, Tuple, Union
import numpy as np
import marshmallow as mm

from pyqumo.fitting.acph2 import fit_acph2
from pyqumo.fitting.johnson89 import fit_mern2
from pyqumo.arrivals import MarkovArrival
from pyqumo.random import PhaseType
from pyqumo.stats import get_noncentral_m2, get_noncentral_m3
from pyqumo.errors import BoundsError

from pqsim.stochastic import Mmap, Ph
from pqsim.model import Params as ModelParams


#
# INPUT DATA DEFINITION
# ---------------------

ALL_APPROX_FUNCTIONS = (
    "linear",
    "parabola",
    "hyperbola",
    "exponent",
    "logarithm",
)

ALL_PROBS_FUNCTIONS = (
    "always",
    "linear",
    "hyperbola",
    "bingeom"
)


# IMPLICIT AND GENERIC DATA
# -------------------------
#
# Implicit inputs define service and arrival rates via approximation functions
# (see ALL_APPROX_FUNCTIONS and build_approx_rates()).
# Rates are specified with two boundary points (for lowest priority and highest
# priority traffic) and number of classes. Rates of intermediate traffic
# classes are computed using the approximation function defined.
#
# Enqueue probabilities of any traffic except the lowest priority are always
# equal to 1.0. For the lowest priority traffic, probabilities can be
# either constant, or decrease with a given function (see ALL_PROBS_FUNCTIONS)
# Here we do not provide means for defining enqueue probabilities explicitly.
#
@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class DistInputData:
    cv: float
    skew: float
    rate_fn: str
    rate_hp: float
    rate_lp: float


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class ArrivalInputData(DistInputData):
    lag: float = 0.


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class QueueInputData:
    capacity: int
    lp_fn: str = "always"


@dataclass
class ModelInputDataBase:
    num_servers: int
    queue: QueueInputData


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class ModelInputData(ModelInputDataBase):
    num_classes: int
    arrival: ArrivalInputData
    service: DistInputData


# EXPLICIT DATA
# -------------
# In explicit case, rates are defined explicitly with an array.
# Queue specification and other parameters are the same.
#
@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class ExplicitDistInputData:
    cv: float
    skew: float
    rates: Sequence[float] = field(default=(), metadata=config(
        decoder=tuple,
        mm_field=mm.fields.List(mm.fields.Float)
    ))


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class ExplicitArrivalInputData(ExplicitDistInputData):
    lag: float = 0.


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class ExplicitModelInputData(ModelInputDataBase):
    arrival: ExplicitArrivalInputData
    service: ExplicitDistInputData


# This is the main input data class.
# Use it as the input and to deserialize JSON strings.
#
# We define a separate schema for it, since dataclasses-json adds __type
# field for Union-type fields, and this seems to be non-overridable.
@dataclass
class InputData:
    model: Union[ModelInputData, ExplicitModelInputData]
    max_packets: int = 100000


class _AnyModelSchemaField(mm.fields.Field):
    def _serialize(self, value, attr: str, obj, **kwargs):
        if isinstance(value, ModelInputData):
            schema = ModelInputData.schema
        elif isinstance(value, ExplicitModelInputData):
            schema = ExplicitModelInputData.schema
        else:
            raise TypeError(f"unsupported model type {type(value)}")
        return schema().dump(value)

    def _deserialize(self, value, attr, data, **kwargs):
        try:
            return ExplicitModelInputData.schema().load(value)
        except mm.ValidationError:
            return ModelInputData.schema().load(value)


class InputDataSchema(mm.Schema):
    model = _AnyModelSchemaField()
    max_packets = mm.fields.Integer(data_key='maxPackets')

    @mm.post_load
    def make_object(self, data, **kwargs):
        return InputData(**data)


# MODEL BUILDERS
# --------------
def build_model_params(
        data: Union[ModelInputData, ExplicitModelInputData]
) -> ModelParams:
    """
    Build pqsim.model.Params from the input data.

    Parameters
    ----------
    data : ModelInputData or ExplicitModelInputData

    Returns
    -------
    params : pqsim.model.Params
    """
    if isinstance(data, ModelInputData):
        num_classes = data.num_classes
        arrival_rates = approximate_between(
            fn=data.arrival.rate_fn,
            y1=data.arrival.rate_hp,
            yn=data.arrival.rate_lp,
            n=num_classes)
        service_rates = approximate_between(
            fn=data.service.rate_fn,
            y1=data.service.rate_hp,
            yn=data.service.rate_lp,
            n=num_classes)
    else:
        arrival_rates = data.arrival.rates
        service_rates = data.service.rates
        num_classes = len(arrival_rates)
        if num_classes != len(service_rates):
            raise ValueError("sizes of arrival rates and service rates "
                             "should match")

    arrival = build_mmap(
        rates=arrival_rates,
        cv=data.arrival.cv,
        skew=data.arrival.skew,
        lag=data.arrival.lag
    )

    services = build_services(
        rates=service_rates,
        cv=data.service.cv,
        skew=data.service.skew
    )

    enqueue_probs = build_enqueue_probs(
        fn=data.queue.lp_fn,
        capacity=data.queue.capacity,
        num_classes=num_classes
    )

    return ModelParams(
        arrival=arrival,
        services=services,
        enqueue_prob=enqueue_probs,
        capacity=data.queue.capacity,
        num_servers=data.num_servers)


def build_mmap(
        rates: Sequence[float], cv: float, skew: float, lag: float) -> Mmap:
    """
    Build MMAP arrival process for a given input data and number of classes.
    """
    _validate_rates(rates)
    ph = _fit_ph(cv, skew)
    map_ = MarkovArrival.phase_type(ph.s, ph.p)
    matrices = [sum(rates) * map_.d0] + [k * map_.d1 for k in rates]
    return Mmap(*matrices)


def build_services(
        rates: Sequence[float], cv: float, skew: float) -> Tuple[Ph]:
    """
    Build a tuple of Ph services for a given input data and number of classes.
    """
    _validate_rates(rates)
    ph = _fit_ph(cv, skew)
    return tuple(Ph(ph.s * rate, ph.p) for rate in rates)


# noinspection PyTypeChecker
def build_enqueue_probs(
        fn: str, capacity: int, num_classes: int) -> np.ndarray:
    """
    Build enqueue probabilities matrix.

    All traffic classes except the lowest priority traffic will always be
    enqueued with probability 1.

    Packets with the lowest priority will be enqueued with probabilities
    approximated using `approximate_enqueue_probs()` routine. It uses
    `data.lp_fn` as the function type.
    """
    # Validate num_classes is a positive integer:
    _validate_positive_integer(num_classes)
    lp_probs = approximate_enqueue_probs(fn=fn, capacity=capacity)
    return np.vstack((np.ones((num_classes - 1, capacity + 1)), lp_probs))


# RANDOM INPUT GENERATORS
# -----------------------
@dataclass
class RandomInputBounds:
    num_classes_min: int = 1
    num_classes_max: int = 5
    num_servers_min: int = 1
    num_servers_max: int = 10
    capacity_min: int = 0
    capacity_max: int = 10
    arrival_rate_min: float = 0.1
    arrival_rate_max: float = 10.0
    arrival_cv_min: float = 0.1
    arrival_cv_max: float = 10.0
    arrival_skew_max: float = 100.0
    arrival_lag_min: float = 0.0
    arrival_lag_max: float = 1.0
    arrival_rate_fns: Sequence[str] = tuple(ALL_APPROX_FUNCTIONS)
    service_rate_min: float = 0.1
    service_rate_max: float = 10.0
    service_cv_min: float = 0.1
    service_cv_max: float = 10.0
    service_skew_max: float = 100.
    service_rate_fns: Sequence[str] = tuple(ALL_APPROX_FUNCTIONS)
    enqueue_probs_fns: Sequence[str] = tuple(ALL_PROBS_FUNCTIONS)


def generate_random_input(
        bounds: RandomInputBounds,
        explicit: bool = False
) -> Union[ModelInputData, ExplicitModelInputData]:
    """
    Generate random model input data with the given boundaries.

    The function can produce either ModelInputData with approximated rates,
    or ExplicitModelInputData with explicit rates.

    Parameters
    ----------
    bounds: RandomInputBounds
    explicit: bool, optional (default: False)

    Returns
    -------
    data: ModelInputData or ExplicitModelInputData
        type of return value depends on `explicit` flag
    """
    num_classes = np.random.randint(
        bounds.num_classes_min,
        bounds.num_classes_max + 1
    )
    num_servers = np.random.randint(
        bounds.num_servers_min,
        bounds.num_servers_max + 1
    )
    capacity = np.random.randint(
        bounds.capacity_min,
        bounds.capacity_max + 1
    )

    def build_dist_kwargs(cv_min, cv_max, skew_max, rate_min, rate_max,
                          rate_fns=(), lag_min=None, lag_max=None):
        cv = np.random.uniform(cv_min, cv_max)
        kwargs = {
            'cv': cv,
            'skew': np.random.uniform(cv - 1/cv, skew_max),
        }
        if lag_min is not None and lag_max is not None:
            kwargs['lag'] = np.random.uniform(lag_min, lag_max)
        if explicit:
            kwargs['rates'] = np.random.uniform(
                rate_min, rate_max, size=num_classes)
        else:
            kwargs['rate_fn'] = np.random.choice(rate_fns)
            kwargs['rate_lp'] = np.random.uniform(rate_min, rate_max)
            kwargs['rate_hp'] = np.random.uniform(rate_min, rate_max)
        return kwargs

    arrival_args = build_dist_kwargs(
        cv_min=bounds.arrival_cv_min,
        cv_max=bounds.arrival_cv_max,
        skew_max=bounds.arrival_skew_max,
        rate_min=bounds.arrival_rate_min,
        rate_max=bounds.arrival_rate_max,
        rate_fns=bounds.arrival_rate_fns,
        lag_min=bounds.arrival_lag_min,
        lag_max=bounds.arrival_lag_max
    )
    service_args = build_dist_kwargs(
        cv_min=bounds.service_cv_min,
        cv_max=bounds.service_cv_max,
        skew_max=bounds.service_skew_max,
        rate_min=bounds.service_rate_min,
        rate_max=bounds.service_rate_max,
        rate_fns=bounds.service_rate_fns
    )

    queue = QueueInputData(
        capacity=capacity,
        lp_fn=np.random.choice(bounds.enqueue_probs_fns))

    if explicit:
        return ExplicitModelInputData(
            num_servers=num_servers,
            arrival=ExplicitArrivalInputData(**arrival_args),
            service=ExplicitDistInputData(**service_args),
            queue=queue
        )

    return ModelInputData(
        num_classes=num_classes,
        num_servers=num_servers,
        arrival=ArrivalInputData(**arrival_args),
        service=DistInputData(**service_args),
        queue=queue
    )


#
# HELPERS
# -------
def _validate_rates(rates: Sequence[float]) -> None:
    """
    Validate that rates sequence contains at least one positive rate,
    isn't empty and doesn't contain negative rates.
    """
    if len(rates) == 0:
        raise ValueError("rates should be non-empty")
    rates = np.asarray(rates)
    if (rates < 0).any():
        raise ValueError("rates must be non-negative")
    if (rates < 1e-12).all():
        raise ValueError("at least one rate must be positive")


def _validate_positive_integer(value: Union[float, int]) -> None:
    """Validate that value is a positive integer, or raise ValueError."""
    if value <= 0 or np.modf(value)[0] > 0:
        raise ValueError(f"expected positive integer, but {value} found")


def _validate_non_negative_integer(value: Union[float, int]) -> None:
    """Validate that value is a non-negative integer, or raise ValueError."""
    if value < 0 or np.modf(value)[0] > 0:
        raise ValueError(f"expected positive integer, but {value} found")


def approximate_between(fn: str, y1: float, yn: float, n: int) -> np.ndarray:
    """
    Build a grid of function values `f(1), f(2), ..., f(n)`.

    As routine arguments, function types, grid size and values at `x = 1`
    and `x = n` need to be specified.

    Supported function types (`fn` values) are:

    - `"linear"`: `f(x) = A*(x-1) + b`, where:

        - `A = (yn - y1)/(n - 1)`
        - `b = y1`

    - `"parabola"`: `f(x) = A*(x-1)^2 + b`, where:

        - `A = (yn - y1)/(n-1)^2`
        - `b = y1`

    - `"hyperbola"`: `f(x) = A/x + b`, where:

        - `A = n*(y1 - yn) / (n-1)`
        - `b = (n*yn - y1) / (n-1)`

    - `"exponent"`: function `f(x) = b*(A^x)`, where:

        - `A = (yn/y1)^(1/(n-1))`
        - `b = y1 * (y1/yn)^(1/(n-1))`

        It is easier to write function as `f(x) = y1 * (yn/y1)^((x-1)/(n-1))`

    - `"logarithm"`: function `f(x) = A*ln(x) + b`, where:

        - `A = (yn - y1)/ln(n)`
        - `b = y1`

    Parameters
    ----------
    fn : str
        function type (see table above).
    y1 : float
        value at x = 1: `y1 = f(1)`.
    yn : float
        value at x = n: `yn = f(n)`.
    n : int
        number of points (x1 = 1, xn = n). If n = 1, then function
        returns array `[y1]`.

    Returns
    -------
    y : numpy.ndarray
        values at `f(1), f(2), ..., f(n)`
    """
    _validate_positive_integer(n)

    if n == 1:
        return np.asarray([y1])

    x = np.arange(1, n+1)
    if fn == "linear":
        a = (float(yn) - y1) / (n-1)
        b = y1
        return a * (x - 1) + b

    if fn == "parabola":
        a = (float(yn) - y1) / ((n-1)**2)
        b = y1
        return a * (x - 1)**2 + b

    if fn == "hyperbola":
        a = float(n) * (y1 - yn) / (n - 1)
        b = (float(n) * yn - y1) / (n - 1)
        return a / x + b

    if fn == "exponent":
        return y1 * np.power(float(yn) / y1, (x - 1) / (n - 1))

    if fn == "logarithm":
        a = (yn - y1) / np.log(n)
        b = y1
        return a * np.log(x) + b

    raise ValueError(f"unsupported approximation function '{fn}'")


def approximate_enqueue_probs(fn: str, capacity: int) -> np.ndarray:
    """
    Build a grid of enqueue probabilities `p(0), p(1), ..., p(n)`.

    Probability function `p(x)` depends on the queue capacity and function
    type. Supported functions are:

    - `"always"` or `"none"`: function `f(x) = 1.0`
    - `"linear"`: function `f(x) = -x/n + 1`
    - `"hyperbola"`: function `f(x) = 1/(x+1)`
    - `"bingeom"`: function `f(x) = 1/(2^x)`

    Parameters
    ----------
    fn : str
        function type, see above
    capacity : int
        queue capacity (`n` value in formulas above)

    Returns
    -------
    probs : numpy.ndarray
        values at `p(0), p(1), ..., p(n)`.
    """
    _validate_non_negative_integer(capacity)

    if capacity == 0:
        return np.ones(1)

    if fn == "always" or fn == "none":
        return np.ones(capacity + 1)

    # Define X = 0, 1, ..., N
    x = np.arange(capacity + 1)

    if fn == "linear":
        return -x / capacity + 1

    if fn == "hyperbola":
        return 1 / (x + 1)

    if fn == "bingeom":
        return 1 / (2**x)

    raise ValueError(f"unsupported enqueue probabilities function '{fn}'")


def _fit_ph(cv: float, skew: float) -> PhaseType:
    """
    Fit PH-distribution by three moments using either ACPH(2) or MERN(2)
    method. See `pyqumo` documentation for details.

    Here mean value is assumed to be equal to 1.

    Parameters
    ----------
    cv : float
        coefficient of variation
    skew : float
        skewness value

    Returns
    -------
    ph : pyqumo.random.PhaseType
    """
    m1 = 1.0
    m2 = get_noncentral_m2(m1, cv)
    m3 = get_noncentral_m3(m1, cv, skew)

    try:
        ph = fit_acph2([m1, m2, m3])[0]
    except BoundsError:
        her2 = fit_mern2([m1, m2, m3], strict=False, max_shape_inc=10)[0]
        ph = her2.as_ph()
    return ph






