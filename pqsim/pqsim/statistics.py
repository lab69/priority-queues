from typing import Optional, Sequence

import numpy as np


def est_series_moment(order: int, value: float, window: np.ndarray, n: int):
    if n <= 0:
        return value
    acc = (window**order).sum()
    return (value * (n - len(window)) + acc) / n


def est_std(moments):
    return (moments[1] - (moments[0]**2))**0.5


class Series:
    def __init__(
            self,
            num_moments: int = 3,
            window_size: int = 100):
        self.moments = np.zeros(num_moments)
        self.window = np.zeros(window_size)
        self.next = 0
        self.n = 0

    def record(self, x: float) -> None:
        window_size = len(self.window)
        self.window[self.next] = x
        self.next += 1
        self.n += 1
        if self.next == window_size:
            self.commit()

    def commit(self):
        window = self.window[:self.next]
        self.moments = np.asarray([
            est_series_moment(i + 1, m, window, self.n)
            for i, m in enumerate(self.moments)
        ])
        self.next = 0

    @property
    def num_samples(self):
        return self.n

    def as_stats(self) -> 'VarData':
        return VarData(self.moments, self.n)

    def __repr__(self):
        moments = "(" + ", ".join(f"{x:.2f}" for x in self.moments) + ")"
        return f"(Series: moments={moments}, n={self.n})"


class TimeSeries:
    def __init__(self):
        self.intervals = [0.0]
        self.prev_value: int = 0
        self.prev_time = 0.0

    def _add_interval(self, time: float):
        if (length := len(self.intervals)) <= self.prev_value:
            self.intervals = self.intervals + \
                [0.0] * (self.prev_value - length + 1)
        self.intervals[self.prev_value] += time - self.prev_time
        self.prev_time = time

    def record(self, time: float, value: int) -> None:
        if value != self.prev_value:
            self._add_interval(time)
            self.prev_value = value

    def commit(self, time: Optional[float] = None):
        if time is not None:
            self._add_interval(time)

    @property
    def pmf(self):
        intervals = np.asarray(self.intervals)
        return intervals / self.prev_time


class SeriesFactory:
    def __init__(self, window_size):
        self.window_size = window_size

    def create_series(self):
        return Series(2, window_size=self.window_size)

    # noinspection PyMethodMayBeStatic
    def create_time_series(self):
        return TimeSeries()


# noinspection PyUnresolvedReferences
class VarDataMixin:
    def moment(self, k: int):
        raise NotImplementedError

    @property
    def var(self):
        return self.moment(2) - self.moment(1)**2

    @property
    def std(self):
        return pow(self.var, 0.5)

    @property
    def mean(self):
        return self.moment(1)

    @property
    def avg(self):
        return self.mean


class VarData(VarDataMixin):
    def __init__(self, moments: Sequence[float], num_samples: int):
        if len(moments) < 1:
            raise ValueError("at least one moment required")
        self._moments = np.asarray(moments)
        self._num_samples = num_samples

    @property
    def moments(self):
        return self._moments

    @property
    def num_samples(self):
        return self._num_samples

    def moment(self, k: int):
        return self._moments[k-1]

    @property
    def avg(self):
        return self.moment(1)

    @property
    def std(self):
        return pow(self.moment(2) - self.moment(1)**2, 0.5)

    def __repr__(self):
        return f"(Stats: moments={self._moments})"


class FiniteDistribution(VarDataMixin):
    def __init__(self, pmf: Sequence[float]):
        self._pmf = np.asarray(pmf)

    @property
    def pmf(self):
        return self._pmf

    def moment(self, k: int):
        koefs = np.power(np.arange(len(self._pmf)), k)
        return np.dot(koefs, self._pmf)

    def __repr__(self):
        return f"(FiniteDist: pmf={self._pmf})"
