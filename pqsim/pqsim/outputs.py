from dataclasses import dataclass, field
from typing import Optional, Sequence, Dict
import marshmallow as mm

from dataclasses_json import dataclass_json, LetterCase, config
from pqsim.inputs import InputData
from pqsim.model import Results
from pqsim.schemas import RoundFloat, ShortInputDataSchema
from pyqumo.stats import get_skewness
from pqsim.statistics import VarData, FiniteDistribution


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class VarOutputData:
    avg: float
    std: float
    skew: Optional[float]

    @staticmethod
    def create(value: VarData) -> 'VarOutputData':
        """Build VarOutputData from a VarData object."""
        try:
            moments = [value.moment(k) for k in (1, 2, 3)]
            skew = get_skewness(*moments)
        except IndexError:
            skew = None
        return VarOutputData(avg=value.avg, std=value.std, skew=skew)


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class DistOutputData(VarOutputData):
    pmf: Sequence[float] = field(default=(), metadata=config(
        decoder=tuple,
        mm_field=mm.fields.List(mm.fields.Float)
    ))

    @staticmethod
    def create(value: FiniteDistribution) -> 'DistOutputData':
        """Build DistOutputData from a FiniteDistribution object."""
        return DistOutputData(
            avg=value.avg,
            std=value.std,
            skew=get_skewness(*[value.moment(k) for k in (1, 2, 3)]),
            pmf=value.pmf)


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class FullTrafficOutputData:
    loss_prob: float
    system_size: DistOutputData
    queue_size: DistOutputData
    response_time: VarOutputData
    wait_time: VarOutputData
    arrivals: VarOutputData
    services: VarOutputData

    @staticmethod
    def create(data: Results, tc: int) -> 'FullTrafficOutputData':
        """Build FullTrafficOutputData for a given traffic class."""
        return FullTrafficOutputData(
            loss_prob=data.loss_prob_of[tc],
            system_size=DistOutputData.create(data.system_size_of[tc]),
            queue_size=DistOutputData.create(data.queue_size_of[tc]),
            response_time=VarOutputData.create(data.response_time_of[tc]),
            wait_time=VarOutputData.create(data.wait_time_of[tc]),
            arrivals=VarOutputData.create(data.arrivals_of[tc]),
            services=VarOutputData.create(data.services_of[tc])
        )


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class FullResultData(FullTrafficOutputData):
    time: float
    busy_servers: DistOutputData
    server_duty: DistOutputData
    traffic_classes: Dict[int, FullTrafficOutputData]

    @staticmethod
    def create(data: Results) -> 'FullResultData':
        """Build FullResultData from modeling results."""
        all_traffic_classes = list(data.system_size_of.keys())
        return FullResultData(
            time=data.time,
            loss_prob=data.loss_prob,
            system_size=DistOutputData.create(data.system_size),
            queue_size=DistOutputData.create(data.queue_size),
            busy_servers=DistOutputData.create(data.busy_servers),
            server_duty=DistOutputData.create(data.server_duty),
            response_time=VarOutputData.create(data.response_time),
            wait_time=VarOutputData.create(data.wait_time),
            arrivals=VarOutputData.create(data.arrivals),
            services=VarOutputData.create(data.services),
            traffic_classes={
                tc: FullTrafficOutputData.create(data, tc)
                for tc in all_traffic_classes
            },
        )


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class FullOutputData:
    inp: InputData
    out: FullResultData


# SHORT RESULTS REPRESENTATIONS
# -----------------------------

@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class TrafficOutputData:
    loss_prob: float = field(metadata=config(
        mm_field=RoundFloat(data_key="pl")))

    system_size: float = field(metadata=config(
        mm_field=RoundFloat(data_key="ss")))

    queue_size: float = field(metadata=config(
        mm_field=RoundFloat(data_key="qs")))

    num_busy: float = field(metadata=config(
        mm_field=RoundFloat(data_key="nb")))

    response_time: float = field(metadata=config(
        mm_field=RoundFloat(data_key="rt")))

    @staticmethod
    def create(data: Results, tc: int) -> 'TrafficOutputData':
        """Create TrafficOutputData for a given traffic class."""
        return TrafficOutputData(
            loss_prob=data.loss_prob_of[tc],
            system_size=data.system_size_of[tc].avg,
            queue_size=data.queue_size_of[tc].avg,
            num_busy=data.server_duty.pmf[tc],
            response_time=data.response_time_of[tc].avg
        )


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class ResultData(TrafficOutputData):
    hp: TrafficOutputData
    lp: TrafficOutputData

    @staticmethod
    def create(data: Results) -> 'ResultData':
        """Create ResultData for a given results data."""
        num_classes = len(data.system_size_of)
        return ResultData(
            loss_prob=data.loss_prob,
            system_size=data.system_size.avg,
            queue_size=data.queue_size.avg,
            num_busy=data.busy_servers.avg,
            response_time=data.response_time.avg,
            hp=TrafficOutputData.create(data, 1),
            lp=TrafficOutputData.create(data, num_classes)
        )


@dataclass_json(letter_case=LetterCase.CAMEL)
@dataclass
class OutputData:
    inp: InputData = field(metadata=config(
        mm_field=mm.fields.Nested(ShortInputDataSchema, data_key="i")
    ))
    out: ResultData = field(metadata=config(field_name="o"))
