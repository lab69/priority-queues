from typing import Mapping, Optional, Any

from marshmallow import Schema, fields, post_load, ValidationError
from marshmallow.fields import Field
from pqsim import inputs


class RoundFloat(fields.Float):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._precision = kwargs.get('precision', 4)

    def _serialize(self, value, attr, obj, **kwargs):
        if value is None:
            return "null"
        return round(value, self._precision)


class ShortDistInputDataSchema(Schema):
    cv = RoundFloat(data_key='cv')
    skew = RoundFloat(data_key='sk')
    rate_fn = fields.Str(data_key='fn')
    rate_hp = RoundFloat(data_key='hp')
    rate_lp = RoundFloat(data_key='lp')

    @post_load
    def make_object(self, data, **kwargs):
        return inputs.DistInputData(**data)


class ShortArrivalInputDataSchema(ShortDistInputDataSchema):
    lag = RoundFloat(data_key='lg', default=0.0)

    @post_load
    def make_object(self, data, **kwargs):
        return inputs.ArrivalInputData(**data)


class ShortQueueInputDataSchema(Schema):
    capacity = fields.Integer(data_key='sz')
    lp_fn = fields.Str(data_key='fn')

    @post_load
    def make_object(self, data, **kwargs):
        return inputs.QueueInputData(**data)


class ShortExplicitDistInputDataSchema(Schema):
    cv = RoundFloat(data_key='cv')
    skew = RoundFloat(data_key='sk')
    rates = fields.List(RoundFloat, data_key='rs')

    @post_load
    def make_object(self, data, **kwargs):
        return inputs.ExplicitDistInputData(**data)


class ShortExplicitArrivalInputDataSchema(ShortExplicitDistInputDataSchema):
    lag = RoundFloat(data_key='lg', default=0.0)

    @post_load
    def make_object(self, data, **kwargs):
        return inputs.ExplicitArrivalInputData(**data)


class ShortModelInputDataSchema(Schema):
    num_servers = fields.Integer(data_key='ns')
    num_classes = fields.Integer(data_key='nc')
    queue = fields.Nested(ShortQueueInputDataSchema, data_key='q')
    arrival = fields.Nested(ShortArrivalInputDataSchema, data_key='a')
    service = fields.Nested(ShortDistInputDataSchema, data_key='s')

    @post_load
    def make_object(self, data, **kwargs):
        return inputs.ModelInputData(**data)


class ShortExplicitModelInputDataSchema(Schema):
    num_servers = fields.Integer(data_key='ns')
    queue = fields.Nested(ShortQueueInputDataSchema, data_key='q')
    arrival = fields.Nested(ShortExplicitArrivalInputDataSchema, data_key='a')
    service = fields.Nested(ShortExplicitDistInputDataSchema, data_key='s')

    @post_load
    def make_object(self, data, **kwargs):
        return inputs.ExplicitModelInputData(**data)


class AnyModelSchemaField(Field):
    def _serialize(self, value, attr: str, obj, **kwargs):
        if isinstance(value, inputs.ModelInputData):
            schema = ShortModelInputDataSchema
        elif isinstance(value, inputs.ExplicitModelInputData):
            schema = ShortExplicitModelInputDataSchema
        else:
            raise TypeError(f"unsupported model type {type(value)}")
        return schema().dump(value)

    def _deserialize(
            self, value, attr: Optional[str], data: Optional[Mapping[str, Any]],
            **kwargs):
        try:
            return ShortExplicitModelInputDataSchema().load(value)
        except ValidationError:
            return ShortModelInputDataSchema().load(value)


class ShortInputDataSchema(Schema):
    model = AnyModelSchemaField(data_key='m')
    max_packets = fields.Integer(data_key='mp')

    @post_load
    def make_object(self, data, **kwargs):
        return inputs.InputData(**data)
