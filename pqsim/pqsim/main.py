import logging
from logging import getLogger
from multiprocessing import Pool
from pathlib import Path
from typing import Union, List, Optional, Tuple

import click

from pqsim.inputs import build_model_params, InputData, RandomInputBounds, \
    generate_random_input, InputDataSchema
from pqsim.model import Params as ModelParams, Results as ModelResults
from pqsim.cqsim import model
from pqsim.outputs import OutputData, ResultData, FullOutputData, \
    FullResultData
from tqdm import tqdm


# noinspection PyTypeChecker
def _run_simulation(args: Tuple[
    Union[InputData, List[InputData]],
    type,
    type,
    int,
    Optional[int],
    bool
]) -> Optional[Union[
    OutputData,
    FullOutputData,
    List[OutputData],
    List[FullOutputData]]
]:
    """
    Run simulation with a given InputData sample or list of samples.

    If more than one sample provided (args[0] is a list), then multiprocessing
    pool of workers will be used.

    This function accepts a single argument, thus it may be used without
    the need for closures in multiprocessing pool.

    Parameters
    ----------
    args : tuple
        0: InputData or list of InputData
            single sample or a list of samples
        1: OutputData or FullOutputData
            type of output data
        2: ResultData or FullResultData
            type of results data
        3: int
            number of processes to use if input data is a list
        4: int, optional
            maximum number of packets to generate. If given, override
            the value from the input data.
        5: bool
            verbose flag

    Returns
    -------
    output : OutputData, FullOutputData or list
        if input (args[0]) is a single instance, then this will be a single
        data instance also. Otherwise, it will be a list.
    """
    input_data: Union[InputData, List[InputData]] = args[0]
    output_data_class: type = args[1]
    result_data_class: type = args[2]
    num_processes: int = args[3]
    max_packets: Optional[int] = args[4]
    verbose: bool = args[5]

    if isinstance(input_data, list):
        all_inputs = [(
            sample,
            output_data_class,
            result_data_class,
            num_processes,
            max_packets,
            verbose
        ) for sample in input_data]

        # Run multiprocess simulation:
        if verbose:
            print(f"* Prepared input for parallel execution on {num_processes} "
                  f"processors")
            # output_data = process_map(_run_simulation, all_inputs,
            #                           max_workers=num_processes,
            #                           chunksize=1)

        output_data = []
        with Pool(processes=num_processes) as pool:
            iterable = tqdm(pool.imap(_run_simulation, all_inputs),
                            total=len(all_inputs))

            for result in iterable:
                output_data.append(result)

        return output_data

    # Execute simulation on a single instance.
    # If any error arise, we log information about the input
    # to logger along with information regarding the error itself.
    # In case of error, return None and filter afterwards.
    try:
        params: ModelParams = build_model_params(input_data.model)
        if max_packets is not None:
            input_data.max_packets = max_packets

        # Launch simulation
        result: ModelResults = model.simulate(
            params,
            max_packets=input_data.max_packets)

    except Exception as exc:
        logging.getLogger('main').error(
            f"Simulation error '{str(exc)}' at\n"
            f"{InputDataSchema().dumps(input_data, indent=4)}")
        return None

    # If everything is OK, then build output and return it (single instance):

    # noinspection PyArgumentList,PyUnresolvedReferences
    return output_data_class(
        inp=input_data,
        out=result_data_class.create(result))


def _configure_logging(log_file_name: Optional[str]) -> None:
    if log_file_name is not None:
        logging.basicConfig(
            level=logging.DEBUG,
            filename=log_file_name,
            filemode='w',
            format='%(asctime)-15s [%(levelname)s] %(message)s'
        )
        console = logging.StreamHandler()
        console.setLevel(logging.CRITICAL)
        logging.getLogger('').addHandler(console)
    else:
        logging.basicConfig(
            format='%(asctime)-15s [%(levelname)s] %(message)s'
        )


@click.group()
def cli():
    pass


@cli.command(help="Run simulation with JSON input from file FILE_NAME.")
@click.argument("file_name")
@click.option('--full/--short', default=False,
              help="In full mode command will output JSON with all collected "
                   "data, while in short mode (default) only key values")
@click.option('--pprint', 'is_pprint', is_flag=True, default=False,
              help="Print output in human-readable manner, with intends and "
                   "newlines.")
@click.option('--max-packets', '-m', required=False, type=int, default=None,
              help="Override maximum number of packets from JSON_FILE")
@click.option('-o', 'out_file_name', required=False, type=str, default=None,
              help="Write result to the file instead of STDOUT.")
@click.option('-j', 'num_workers', default=1,
              help="Number of workers to launch if input is a JSON array.")
@click.option('-v', '--verbose', default=False, is_flag=True,
              help="Print more details during computation.")
@click.option('--log', "log_file_name", required=False, default=None,
              help="Write log to file instead of STDERR")
def simulate(
        log_file_name,
        verbose,
        num_workers,
        out_file_name,
        max_packets,
        is_pprint,
        full,
        file_name
):
    # First, load JSON from FILE_NAME. This JSON can contain either a single
    # sample, or a list.
    # FIXME: possible source of errors:
    # We distinguish them just by the first non-space char
    # in the loaded string
    _configure_logging(log_file_name)

    if verbose:
        print(f"* Loading data from {file_name}")

    input_string: str = Path(file_name).read_text()
    input_string = input_string.strip()
    many = input_string[0] == '['
    input_data: Union[InputData, List[InputData]] = \
        InputDataSchema().loads(input_string, many=many)

    if verbose:
        if not many:
            print(f"==> loaded single record")
        else:
            print(f"==> loaded array with {len(input_data):,} records")

    # Select proper output classes:
    # - in full mode, use data classes those provide complete data
    # - otherwise, store only data regarding low and high priority traffic
    #   and overall system properties.
    if full:
        result_data_class = FullResultData
        output_data_class = FullOutputData
    else:
        result_data_class = ResultData
        output_data_class = OutputData
    output_schema = output_data_class.schema()

    #
    # EXECUTE SIMULATION
    #
    if verbose:
        print(f"* Start simulation")

    output_data = _run_simulation((
        input_data,
        output_data_class,
        result_data_class,
        num_workers,
        max_packets,
        verbose
    ))

    if verbose:
        print(f"* Finished simulation")

    if isinstance(output_data, list):
        output_data = [item for item in output_data if item is not None]

    if verbose:
        dest_str = "STDOUT" if out_file_name is None else out_file_name
        if many:
            print(f"* Writing {len(output_data):,} records to {dest_str}")
        else:
            print(f"Writing output to {dest_str}")

    # Prepare dump arguments:
    dump_kwargs = {'many': many}
    if not is_pprint:
        dump_kwargs.update({'separators': (',', ':')})
    else:
        dump_kwargs.update({'indent': 4})

    # Produce output string
    output_string = output_schema.dumps(output_data, **dump_kwargs)

    if out_file_name:
        Path(out_file_name).write_text(output_string)
    else:
        print(output_string)


@cli.command(help="Generate NUM_SAMPLES random inputs")
@click.argument('num_samples', type=int)
@click.option('--max-packets', '-m', default=1000000,
              help="Max. number of packets")
@click.option('--pprint', '-p', 'is_pprint', is_flag=True, default=False,
              help="Write results in human-readable manner, with indents and "
                   "newlines.")
@click.option('-o', 'out_file_name', required=False, type=str, default=None,
              help="Write result to the file instead of STDOUT.")
def generate(out_file_name, is_pprint, max_packets, num_samples):
    bounds = RandomInputBounds()
    samples = [generate_random_input(bounds) for _ in range(num_samples)]
    inputs = [InputData(sample, max_packets) for sample in samples]

    dumps_kwargs = {}
    if is_pprint:
        dumps_kwargs.update({'indent': 4})
    else:
        dumps_kwargs.update({'separators': (',', ':')})

    output_string = InputDataSchema().dumps(inputs, many=True, **dumps_kwargs)

    if out_file_name:
        Path(out_file_name).write_text(output_string)
    else:
        print(output_string)


if __name__ == '__main__':
    cli()
