import numpy as np
from typing import Optional, Tuple, Sequence
from functools import cached_property

# from pqsim.randoms import RandomsFactory as PyRandomsFactory
from pqsim.cqsim.randoms import RandomsFactory as CyRandomsFactory


default_randoms_factory = CyRandomsFactory()


class Mmap:
    def __init__(self, *d, factory=None):
        # Validate MMAP:
        # - at least two matrices are provided
        # - all matrices have square shape
        # - all matrices have the same shape
        # - D0 is a subinfinitesimal matrix
        # - Di elements are non-negative for all i >= 1
        # - sum of matrices is an infinitesimal generator
        if len(d) <= 1:
            raise ValueError('At least D0 and D1 must be provided')

        # Before further validations, convert and store matrices and order:
        self._d = [np.asarray(di) for di in d]
        self._order = order_of(self._d[0])

        if not all(is_square(di) for di in self._d):
            raise ValueError('D0 and D1 must be square matrices')
        if not all(order_of(di) == self._order for di in self._d):
            raise ValueError("All matrices must have the same order")
        if not is_subinfinitesimal(self._d[0]):
            raise ValueError("D0 must be subinfinitesimal")
        if any((di < 0).any() for di in self._d[1:]):
            raise ValueError("All Di elements must be non-negative for i >= 1")

        # Store infinitesimal generator:
        self._g = sum(self._d)

        if not is_infinitesimal(self._g):
            raise ValueError("Sum of Di must be infinitesimal")

        # Compute rates and initial PMF
        self._rates = -self._d[0].diagonal()
        a = np.vstack((
            self._g.transpose()[:-1],  # remove one row as redundant
            np.ones(self._order)       # add normalization
        ))
        b = np.asarray([0] * (self._order - 1) + [1])
        self._state_pmf = np.linalg.solve(a, b)

        # Store randoms factory
        self.__factory = factory if factory is not None else \
            default_randoms_factory

    @staticmethod
    def exponential(rate, p=None, nc=1) -> 'Mmap':
        if p is None:
            p = [1/nc] * nc
        else:
            nc = len(p)

        d0 = [[-rate]]
        di = [[[rate * p[i]]] for i in range(nc)]
        return Mmap(d0, *di)

    @staticmethod
    def erlang(rate, scale, p=None, nc=1) -> 'Mmap':
        if p is None:
            p = [1/nc] * nc
        else:
            nc = len(p)

        d0 = np.diag([-rate] * scale) + np.diag([rate] * scale, 1)[:-1, :-1]
        ds = []
        for i in range(nc):
            di = np.zeros((scale, scale))
            di[scale-1, 0] = p[i] * rate
            ds.append(di)
        return Mmap(d0, *ds)

    def d(self, i):
        return self._d[i]

    @property
    def generator(self):
        return self._g

    @property
    def trans_pmf(self):
        return self._trans_pmf

    @property
    def state_pmf(self):
        return self._state_pmf

    @property
    def rates(self):
        return self._rates

    @property
    def order(self):
        return self._order

    @property
    def num_types(self):
        return len(self._d) - 1

    @cached_property
    def rnd(self):
        return self.__factory.createMarkedMap(self._d, self._state_pmf)

    def __call__(self) -> Tuple[int, float]:
        return self.rnd.eval()


class Ph:
    def __init__(self, s, p, factory=None):
        # Convert:
        self._s = np.asarray(s)
        self._init_pmf = np.asarray(p).flatten()

        # Validate data:
        # -------------
        if not is_subinfinitesimal(self._s):
            raise ValueError("matrix S must be sub-infinitesimal")
        if (self._init_pmf < 0).any() or sum(self._init_pmf) != 1.0:
            raise ValueError("initial PMF must be stochastic")
        if order_of(self._init_pmf) != order_of(self._s):
            raise ValueError("initial PMF must have the same order as S")

        # Build internal representations for transitions PMFs and rates:
        # --------------------------------------------------------------
        self._order = order_of(self._init_pmf)
        self._rates = -self._s.diagonal()

        # Store randoms factory
        self.__factory = factory if factory is not None else \
            default_randoms_factory

    @staticmethod
    def exponential(rate) -> 'Ph':
        s = [[-rate]]
        p = [1.0]
        return Ph(s, p)

    @staticmethod
    def erlang(rate, scale) -> 'Ph':
        s = np.diag([-rate] * scale) + np.diag([rate] * scale, 1)[:-1, :-1]
        p = [1.0] + [0.0] * (scale - 1)
        return Ph(s, p)

    @property
    def subgenerator(self) -> np.ndarray:
        return self._s

    @property
    def s(self) -> np.ndarray:
        return self._s

    @property
    def init_pmf(self) -> np.ndarray:
        return self._init_pmf

    @property
    def order(self) -> int:
        return self._order

    @property
    def rates(self) -> np.ndarray:
        return self._rates

    @cached_property
    def rnd(self):
        return self.__factory.createPhaseType(self.s, self._init_pmf)

    def __call__(self) -> float:
        return self.rnd.eval()


def str_array(array: np.ndarray):
    if len(array.shape) == 1:
        # Array is a 1-D vector
        return ", ".join(str(x) for x in array)
    if len(array.shape) > 2:
        raise ValueError('only 1-D and 2-D arrays are supported')
    # Array is 2-D
    return "; ".join(
        ", ".join(str(x) for x in array[i])
        for i in range(array.shape[0]))


def parse_array(string: str, ndim: Optional[int] = None):
    data = []
    for row in string.split(';'):
        cols = [s for s in row.split(',') if s]
        data.append([float(x) for x in cols])

    if ndim is None:
        is_vector = len(data) <= 1 or len(data) > 1 and len(data[0]) == 1
        ndim = 1 if is_vector else 2

    array = np.asarray(data)

    if len(array.shape) == 2 and ndim == 1:
        return array.reshape((array.shape[0] * array.shape[1],))
    elif len(array.shape) == 1 and ndim == 2:
        return array.reshape((1, array.shape[0]))

    return array


def is_square(matrix):
    """Checks that a given matrix has square shape.

    Args:
        matrix (numpy.ndarray, list or tuple): a matrix to test

    Returns:
        bool: True if the matrix as a square shape
    """
    matrix = np.asarray(matrix)
    return len(matrix.shape) == 2 and matrix.shape[0] == matrix.shape[1]


def is_vector(vector):
    """Checks that a given matrix represents a vector. One-dimensional
    lists, tuples or numpy arrays are vectors as well as two-dimensional,
    where one dimension equals 1 (e.g. row-vector or column-vector).

    Args:
        vector: a matrix to be tested

    Returns:
        True if the matrix represents a vector
    """
    vector = np.asarray(vector)
    return len(vector.shape) == 1 or (len(vector.shape) == 2 and (
            vector.shape[0] == 1 or vector.shape[1] == 1))


def order_of(matrix):
    """Returns an order of a square matrix or a vector - a number of rows
    (eq. columns) for square matrix and number of elements for a vector.

    Args:
        matrix: a square matrix or a vector

    Returns:
        A number of rows (eq. columns) for a square matrix;
        a number of elements for a vector
    """
    matrix = np.asarray(matrix)
    if is_square(matrix):
        return matrix.shape[0]
    elif is_vector(matrix):
        return len(matrix.flatten())
    else:
        raise ValueError("square matrix or vector expected")


def is_stochastic(matrix, rtol=1e-05, atol=1e-08):
    """Function checks whether a given matrix is stochastic, i.e. a square
    matrix of non-negative elements whose sum per each row is 1.0.

    All comparisons are performed "close-to", np.allclose() method
    is used to compare values.

    Args:
        matrix: a matrix to be tested
        rtol: relative tolerance, see numpy.allclose for reference
        atol: absolute tolerance, see numpy.allclose for reference

    Returns:
        True if the matrix is stochastic, False otherwise
    """
    matrix = np.asarray(matrix)
    return (is_square(matrix) and
            (matrix >= -atol).all() and
            (matrix <= 1.0 + atol).all() and
            np.allclose(matrix.sum(axis=1), np.ones(order_of(matrix)),
                        rtol=rtol, atol=atol))


def is_infinitesimal(matrix, rtol=1e-05, atol=1e-08):
    """Function checks whether a given matrix is infinitesimal, i.e. a square
    matrix of non-negative non-diagonal elements, sum per each row is zero.

    All comparisons are performed in "close-to" fashion, np.allclose() method
    is used to compare values.

    Args:
        matrix: a square matrix to test
        rtol: relative tolerance, see numpy.allclose for reference
        atol: absolute tolerance, see numpy.allclose for reference

    Returns:
        True if the matrix is infinitesimal, False otherwise
    """
    mat = np.asarray(matrix)
    return (is_square(mat) and
            ((mat - np.diag(mat.diagonal().flatten())) >= -atol).all() and
            np.allclose(np.zeros(order_of(mat)), mat.sum(axis=1),
                        atol=atol, rtol=rtol))


def is_subinfinitesimal(matrix, atol=1e-08):
    """Function checks whether a given matrix is sub-infinitesimal,
    i.e. a square matrix of non-negative non-diagonal elements,
    sum per each row is less or equal to zero and at least one sum is strictly
    less than zero.

    Args:
        matrix: a square matrix to test
        atol: absolute tolerance

    Returns:
        True if the matrix is sub-infinitesimal, False otherwise
    """
    mat = np.asarray(matrix)
    rs = mat.sum(axis=1)
    # noinspection PyUnresolvedReferences
    return (is_square(mat) and
            ((mat - np.diag(mat.diagonal().flatten())) >= -atol).all() and
            (rs <= atol).all() and (rs < -atol).any())


def estimate_mmap_rates(mmap: Mmap, n_iters: int = 1000000):
    """
    Estimate packet arrival rates in MMAP process.
    """
    samples = [mmap() for _ in range(n_iters)]
    timestamps = np.asarray([sample[0] for sample in samples]).cumsum()
    marks = np.asarray([sample[1] for sample in samples])

    # Compute intervals between packets:
    prev_time = np.zeros(mmap.num_types)
    intervals = [list() for _ in range(mmap.num_types)]
    for mark, t in zip(marks, timestamps):
        index_ = mark - 1
        intervals[index_].append(t - prev_time[index_])
        prev_time[index_] = t

    # Estimate mean values:
    mean_intervals = np.asarray([np.mean(ints) for ints in intervals])
    return np.power(mean_intervals, -1)


def estimate_ph_rate(ph: Ph, n_iters: int = 1000000):
    """
    Estimate PH distributions rates.
    """
    samples = [ph() for _ in range(n_iters)]
    return 1 / np.mean(samples)
