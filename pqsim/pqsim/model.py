from dataclasses import dataclass
from typing import List, Optional, Tuple, Dict, Sequence

import numpy as np
from pqsim.statistics import FiniteDistribution, VarData, TimeSeries, Series

from .stochastic import Mmap, Ph


@dataclass(frozen=True)
class Params:
    """
    Model parameters.
    """
    arrival: Mmap  # Arrival MMAP process with L packet types
    services: Sequence[Ph]  # Service time distributions of, length = L
    enqueue_prob: Sequence[Sequence[float]]  # Enqueue probability when busy
    capacity: int = 10  # Queue capacity
    num_servers: int = 1  # Number of servers


@dataclass(frozen=True)
class Results:
    """
    Simulation results.
    """
    time: float

    # Total system and queue sizes:
    system_size: FiniteDistribution
    queue_size: FiniteDistribution

    # Number of packets of a given class in the system and in the queue:
    system_size_of: Dict[int, FiniteDistribution]
    queue_size_of: Dict[int, FiniteDistribution]

    # K-th element: probability that K servers are busy
    busy_servers: FiniteDistribution

    # K-th element: probability that arbitrary server is empty (K = 0),
    #   or is serving a packet of class K (K > 0)
    server_duty: FiniteDistribution

    # Total response and wait time:
    response_time: VarData
    wait_time: VarData

    # Response and wait times per priority class:
    response_time_of: Dict[int, VarData]
    wait_time_of: Dict[int, VarData]

    # Statistics of arrival and service times:
    arrivals: VarData
    services: VarData
    arrivals_of: Dict[int, VarData]
    services_of: Dict[int, VarData]

    # Loss probability:
    loss_prob: float
    loss_prob_of: Dict[int, float]


@dataclass(frozen=True)
class Packet:
    """
    Packet is represented with its type and creation timestamp.
    """
    type: int
    created_at: float


@dataclass
class State:
    """
    Simulation model state.
    """
    time: float
    servers: Sequence[Optional[Packet]]
    queue: Sequence[Packet]
    service_end: Sequence[Optional[float]]
    next_arrival_time: float
    next_arrival_type: int
    prev_arrival_of: Dict[int, float]


class Statistics:
    """
    Simulation statistics.
    """

    # pylint: disable=too-many-instance-attributes
    # pylint: disable=too-few-public-methods
    def __init__(self, num_types: int, num_servers: int,
                 window_size: int = 100):
        self._types_range = range(1, num_types + 1)
        self.num_packets_generated = 0

        # Create statistics:
        self.system_size = TimeSeries()
        self.queue_size = TimeSeries()
        self.busy_servers = TimeSeries()
        self.services = Series()
        self.arrivals = Series()
        self.wait_times = Series()
        self.response_times = Series()

        # Per-server statistics:
        self.server_duty = [TimeSeries() for _ in range(num_servers)]

        # These statistics are stored on per-type basis:
        self.system_size_of = {i: TimeSeries() for i in self.types}
        self.queue_size_of = {i: TimeSeries() for i in self.types}
        self.wait_times_of = {i: Series() for i in self.types}
        self.response_times_of = {i: Series() for i in self.types}
        self.services_of = {i: Series() for i in self.types}
        self.arrivals_of = {i: Series() for i in self.types}

        # Scalar statistics:
        self.num_packets_accepted = {i: 0 for i in self.types}
        self.num_packets_dropped = {i: 0 for i in self.types}

    @property
    def types(self):
        return self._types_range

    def commit(self, time: Optional[float] = None):
        self.system_size.commit(time)
        self.queue_size.commit(time)
        self.busy_servers.commit(time)
        self.services.commit()
        self.arrivals.commit()
        self.wait_times.commit()
        self.response_times.commit()
        for series in self.server_duty:
            series.commit(time)
        for i in self.types:
            self.system_size_of[i].commit(time)
            self.queue_size_of[i].commit(time)
            self.wait_times_of[i].commit()
            self.response_times_of[i].commit()
            self.services_of[i].commit()
            self.arrivals_of[i].commit()

    def get_results(self, time: float) -> Results:
        # Process results:
        num_servers = len(self.server_duty)
        max_server_duty_states = max(len(s.pmf) for s in self.server_duty)
        server_duty_pmf_list = []
        for s in self.server_duty:
            s_pmf = s.pmf
            pmf = np.zeros(max_server_duty_states)
            pmf[:s_pmf.shape[0]] = s_pmf
            server_duty_pmf_list.append(pmf)
        server_duty_pmf = sum(server_duty_pmf_list) / num_servers
        num_packets_dropped = sum(self.num_packets_dropped.values())
        num_packets_accepted = sum(self.num_packets_accepted.values())

        # Helper functions:
        def parse_time_series_dict(dict_):
            return {k: FiniteDistribution(v.pmf) for k, v in dict_.items()}

        def get_loss_prob(dropped, accepted):
            total = dropped + accepted
            return dropped / total if total > 0 else 0.0

        return Results(
            time=time,
            system_size=FiniteDistribution(self.system_size.pmf),
            system_size_of=parse_time_series_dict(self.system_size_of),
            queue_size=FiniteDistribution(self.queue_size.pmf),
            queue_size_of=parse_time_series_dict(self.queue_size_of),
            busy_servers=FiniteDistribution(self.busy_servers.pmf),
            server_duty=FiniteDistribution(server_duty_pmf),
            response_time=self.response_times.as_stats(),
            response_time_of={
                k: v.as_stats() for k, v in self.response_times_of.items()},
            wait_time=self.wait_times.as_stats(),
            wait_time_of={
                k: v.as_stats() for k, v in self.wait_times_of.items()},
            arrivals=self.arrivals.as_stats(),
            arrivals_of={k: v.as_stats() for k, v in self.arrivals_of.items()},
            services=self.services.as_stats(),
            services_of={k: v.as_stats() for k, v in self.services_of.items()},
            loss_prob=get_loss_prob(num_packets_dropped, num_packets_accepted),
            loss_prob_of={
                i: get_loss_prob(
                    self.num_packets_dropped[i],
                    self.num_packets_accepted[i]
                ) for i in self.types}
        )


def simulate(params: Params, max_packets=100000) -> Results:
    """
    Run the simulation.
    """
    next_arrival_time, next_packet_type = params.arrival()
    state = State(
        time=0,
        servers=([None] * params.num_servers),
        queue=[],
        service_end=([None] * params.num_servers),
        next_arrival_time=next_arrival_time,
        next_arrival_type=next_packet_type,
        prev_arrival_of={
            pkt_type: 0.0
            for pkt_type in range(1, params.arrival.num_types + 1)
        })

    stats = Statistics(
        num_types=params.arrival.num_types,
        num_servers=params.num_servers,
        window_size=100)

    initialize(state, stats, params)

    # Run simulation loop:
    while (stats.num_packets_generated < max_packets):
        # Select the next event:
        service_end, server = find_min(state.service_end)
        if server is None or state.next_arrival_time < service_end:
            handle_arrival(state, stats, params)
        else:
            handle_service_end(state, stats, server, params)

    # Build results:
    stats.commit(state.time)
    return stats.get_results(state.time)


def initialize(state: State, stats: Statistics, params: Params) -> None:
    """
    Schedule initial arrival, init TimeSeries.
    """
    state.next_arrival_time, state.next_arrival_type = params.arrival()
    stats.system_size.record(0, 0)
    stats.queue_size.record(0, 0)
    stats.busy_servers.record(0, 0)
    for i, _ in enumerate(state.servers):
        stats.server_duty[i].record(0, 0)
    for pkt_type in range(1, params.arrival.num_types + 1):
        stats.system_size_of[pkt_type].record(0, 0)
        stats.queue_size_of[pkt_type].record(0, 0)


def handle_arrival(state: State, stats: Statistics, params: Params) -> None:
    """
    Handle packet arrival event.
    """
    # Current time and packet type, create the packet:
    time, pkt_type = state.next_arrival_time, state.next_arrival_type
    pkt = Packet(pkt_type, time)

    # Generate next arrival time and packet type:
    next_interval, next_pkt_type = params.arrival()
    stats.num_packets_generated += 1

    # Update state
    state.time = time
    state.prev_arrival_of[pkt_type] = time
    state.next_arrival_time = time + next_interval
    state.next_arrival_type = next_pkt_type

    # Record arrivals statistics:
    stats.arrivals.record(next_interval)
    stats.arrivals_of[next_pkt_type].record(
        state.next_arrival_time - state.prev_arrival_of[next_pkt_type])

    # If there is at least one empty server, start serving:
    server = find_empty_server(state.servers)

    # If there are no available server and there is no place in the queue,
    # or queue isn't full, but a random 0..1 value is greater that p_k,
    # then drop the packet:
    queue_size = len(state.queue)
    if server is None and (
            queue_size >= params.capacity or
            np.random.random() > params.enqueue_prob[pkt_type - 1][queue_size]):
        stats.num_packets_dropped[pkt_type] += 1
        return

    # If we are here, then the packet is not lost and goes either to the
    # server, or to the queue, depending on wheather there is an empty server.
    if server is not None:
        start_service(state, stats, params, pkt, server)
    else:
        enqueue_packet(state, stats, pkt)

    # Update system and queue size statistics:
    update_size_statistics(state, stats, params)
    stats.num_packets_accepted[pkt_type] += 1


def handle_service_end(state: State, stats: Statistics, server: int,
                       params: Params) -> None:
    """
    Handle the end of the service at a given server.
    """
    # Extract data about the packet and the server that finish service,
    # record statistics about service:
    time, pkt = state.service_end[server], state.servers[server]

    # Record response time (end-to-end delay):
    response_interval = time - pkt.created_at
    stats.response_times.record(response_interval)
    stats.response_times_of[pkt.type].record(response_interval)

    # Update time:
    state.time = time

    # If there is another packet in the queue, start serving it:
    if state.queue:
        new_pkt = state.queue[0]
        state.queue = state.queue[1:]
        start_service(state, stats, params, new_pkt, server)
    else:
        state.servers[server] = None
        state.service_end[server] = None
        stats.server_duty[server].record(time, 0)

    # Update system and queue size statistics:
    update_size_statistics(state, stats, params)


#
# Event handlers helpers
#
def start_service(state: State, stats: Statistics, params: Params,
                  pkt: Packet, server: int) -> None:
    """
    Helper that puts the packet into a server.
    """
    # Select random interval, schedule service end and store the packet:
    time, pkt_type = state.time, pkt.type
    interval = params.services[pkt_type - 1]()
    state.servers[server] = pkt
    state.service_end[server] = time + interval

    # Record service statistics
    stats.services.record(interval)
    stats.services_of[pkt.type].record(interval)

    # Record packet waiting time
    wait_interval = time - pkt.created_at
    stats.wait_times.record(wait_interval)
    stats.wait_times_of[pkt.type].record(wait_interval)

    # Record server duty
    stats.server_duty[server].record(time, pkt.type)


def enqueue_packet(state: State, stats: Statistics, pkt: Packet) -> None:
    """
    Helper that puts the packet into the queue.
    """
    # Find the place and put the packet to the queue:
    pkt_type = pkt.type
    pos = find_insert_index(state.queue, pkt_type)
    state.queue = state.queue[:pos] + [pkt] + state.queue[pos:]


def update_size_statistics(state: State, stats: Statistics,
                           params: Params) -> None:
    """
    Update overall and per-packet system size and queue size.
    """
    time = state.time
    num_queued = len(state.queue)
    num_serving = count_packets(state.servers)
    stats.system_size.record(time, num_queued + num_serving)
    stats.queue_size.record(time, num_queued)
    stats.busy_servers.record(time, num_serving)
    for pkt_type in range(1, params.arrival.num_types + 1):
        num_queued = count_typed_packets(state.queue, pkt_type)
        num_serving = count_typed_packets(state.servers, pkt_type)
        stats.system_size_of[pkt_type].record(time, num_queued + num_serving)
        stats.queue_size_of[pkt_type].record(time, num_queued)


#
# State functions
#
def find_min(times: List[Optional[float]]) -> Optional[Tuple[float, int]]:
    """Find the minimum time and record index. If not found, return None."""
    min_t, server = None, None
    for i, t in enumerate(times):
        if t is not None and (min_t is None or min_t > t):
            min_t = t
            server = i
    return min_t, server


def find_insert_index(queue: List[Packet], pkt_type: int) -> int:
    """Find the index where to insert the packet of a given type."""
    for i, pkt in enumerate(queue):
        if pkt.type > pkt_type:
            return i
    return len(queue)


def count_typed_packets(packets: List[Optional[Packet]], pkt_type: int) -> int:
    """Count packets of a given type."""
    return len(
        [pkt for pkt in packets if pkt is not None and pkt.type == pkt_type])


def count_packets(array: List[Optional[Packet]]) -> int:
    """Get the number of non-None items."""
    return len([pkt for pkt in array if pkt is not None])


def find_empty_server(array: List[Optional[Packet]]) -> Optional[int]:
    """Find the first empty item index. If not found, return None."""
    for i, pkt in enumerate(array):
        if pkt is None:
            return i
    return None
