from libcpp.vector cimport vector
from libcpp.map cimport map
from pqsim.cqsim.randoms cimport RandomVariable, MarkedProcessVariable


cdef extern from "Statistics.h" namespace "cqsim":
    cdef cppclass SizeDist:
        double mean()
        double var()
        double std()
        double moment(int order)
        vector[double] pmf()

    cdef cppclass VarData:
        double mean
        double std
        double var
        unsigned count
        vector[double] moments


cdef extern from "Model.h" namespace "cqsim":
    cdef cppclass Params:
        MarkedProcessVariable* arrival
        vector[RandomVariable*] services
        vector[vector[double]] enqueueProbs
        size_t capacity
        size_t numServers

    cdef cppclass Results:
        double time
        SizeDist systemSize
        map[int, SizeDist] systemSizeOf
        SizeDist queueSize
        map[int, SizeDist] queueSizeOf
        SizeDist busyServers
        SizeDist serverDuty
        VarData waitTime
        map[int, VarData] waitTimeOf
        VarData responseTime
        map[int, VarData] responseTimeOf
        VarData arrivals
        map[int, VarData] arrivalsOf
        VarData services
        map[int, VarData] servicesOf
        double lossProb
        map[int, double] lossProbOf

    Results Simulate(const Params& params, double maxSimTime)
