import numpy as np
from libcpp.vector cimport vector
from pqsim.cqsim.randoms cimport RandomVariable as CxxRandomVariable, \
    MarkedProcessVariable as CxxMarkedProcessVariable, \
    Randoms as CxxRandomsFactory, MarkedInterval


cdef class RandomVariableWrapper:
    def __init__(self):
        self.variable = NULL

    def __dealloc__(self):
        del self.variable

    cdef set_variable(self, CxxRandomVariable *variable):
        self.variable = variable

    cdef CxxRandomVariable *get_variable(self):
        return self.variable

    cpdef eval(self):
        return self.variable.eval()


cdef class MarkedProcessVariableWrapper:
    def __init__(self):
        self.variable = NULL

    def __dealloc__(self):
        del self.variable

    cdef set_variable(self, CxxMarkedProcessVariable *variable):
        self.variable = variable

    cdef CxxMarkedProcessVariable *get_variable(self):
        return self.variable

    cpdef eval(self):
        cdef MarkedInterval c_ret = self.variable.eval()
        return c_ret.interval, c_ret.mark


cdef class RandomsFactory:
    cdef CxxRandomsFactory* randoms

    def __init__(self):
        self.randoms = new CxxRandomsFactory()

    def __dealloc__(self):
        del self.randoms

    def createPhaseType(self, s, p):
        cdef vector[vector[double]] c_s = s
        cdef vector[double] c_p = p
        cdef CxxRandomVariable *c_var = self.randoms.createPhaseType(c_s, c_p)
        py_var = RandomVariableWrapper()
        py_var.set_variable(c_var)
        return py_var

    def createMarkedMap(self, d, p0):
        cdef vector[vector[vector[double]]] c_d = d
        cdef vector[double] c_p0 = p0
        cdef CxxMarkedProcessVariable *c_var = \
            self.randoms.createMarkedMap(c_d, c_p0)
        py_var = MarkedProcessVariableWrapper()
        py_var.set_variable(c_var)
        return py_var
