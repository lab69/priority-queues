from cython.operator cimport dereference as deref, preincrement as inc
from libcpp.vector cimport vector

from pqsim.cqsim.model cimport Results as CppResults, \
    Params as CppParams, Simulate as CppSimulate, VarData as CppVarData, \
    SizeDist as CppSizeDist
from pqsim.model import Results as PyResults, Params as PyParams
from pqsim.statistics import VarData as PyVarData, \
    FiniteDistribution as PySizeDist
from pqsim.cqsim.randoms cimport MarkedProcessVariableWrapper, \
    RandomVariableWrapper


cdef _build_PyVarData(CppVarData* data):
    if data.moments.size() == 0:
        return PyVarData([1, 1], 0)
    return PyVarData(data.moments, data.count)


cdef _build_PyVarDataDict(map[int, CppVarData]* data):
    result = {}
    cdef map[int, CppVarData].iterator it = data.begin()
    while it != data.end():
        result[deref(it).first] = _build_PyVarData(&(deref(it).second))
        inc(it)
    return result


cdef _build_PySizeDist(CppSizeDist* data):
    cdef vector[double] pmf = data.pmf()
    if pmf.size() == 0:
        return PySizeDist([1])
    return PySizeDist(pmf)


cdef _build_PySizeDistDict(map[int, CppSizeDist]* data):
    result = {}
    cdef map[int, CppSizeDist].iterator it = data.begin()
    while it != data.end():
        result[deref(it).first] = _build_PySizeDist(&(deref(it).second))
        inc(it)
    return result


cdef _build_PyDoubleDict(map[int, double]* data):
    result = {}
    cdef map[int, double].iterator it = data.begin()
    while it != data.end():
        result[deref(it).first] = deref(it).second
        inc(it)
    return result


cdef _build_PyResults(CppResults* cResults):
    return PyResults(
        time=cResults.time,
        system_size=_build_PySizeDist(&cResults.systemSize),
        system_size_of=_build_PySizeDistDict(&cResults.systemSizeOf),
        queue_size=_build_PySizeDist(&cResults.queueSize),
        queue_size_of=_build_PySizeDistDict(&cResults.queueSizeOf),
        busy_servers=_build_PySizeDist(&cResults.busyServers),
        server_duty=_build_PySizeDist(&cResults.serverDuty),
        wait_time=_build_PyVarData(&cResults.waitTime),
        wait_time_of=_build_PyVarDataDict(&cResults.waitTimeOf),
        response_time=_build_PyVarData(&cResults.responseTime),
        response_time_of=_build_PyVarDataDict(&cResults.responseTimeOf),
        arrivals=_build_PyVarData(&cResults.arrivals),
        arrivals_of=_build_PyVarDataDict(&cResults.arrivalsOf),
        services=_build_PyVarData(&cResults.services),
        services_of=_build_PyVarDataDict(&cResults.servicesOf),
        loss_prob=cResults.lossProb,
        loss_prob_of=_build_PyDoubleDict(&cResults.lossProbOf)
    )


def simulate(params, max_packets=100000):
    if not isinstance(params, PyParams):
        raise TypeError(f'expected {PyParams} instance')
    cdef CppParams c_params
    cdef MarkedProcessVariableWrapper cdef_arrival = params.arrival.rnd
    c_params.arrival = cdef_arrival.get_variable()

    cdef RandomVariableWrapper cdef_random_var

    for service in params.services:
        cdef_random_var = service.rnd
        c_params.services.push_back(cdef_random_var.get_variable())

    cdef vector[double] c_probs
    for class_enqeueue_probs in params.enqueue_prob:
        c_probs = class_enqeueue_probs
        c_params.enqueueProbs.push_back(c_probs)

    c_params.capacity = params.capacity
    c_params.numServers = params.num_servers

    cdef CppResults c_results = CppSimulate(c_params, max_packets)
    py_results = _build_PyResults(&c_results)
    return py_results
