from libcpp.vector cimport vector


cdef extern from "Randoms.h" namespace "cqsim":
    cdef cppclass RandomVariable:
        double eval()

    cdef cppclass MarkedInterval:
        double interval
        int mark

    cdef cppclass MarkedProcessVariable:
        MarkedInterval eval()

    cdef cppclass Randoms:
        Randoms()
        Randoms(unsigned seed)

        RandomVariable* createPhaseType(
            const vector[vector[double]]& s,
            const vector[double]& p)

        MarkedProcessVariable* createMarkedMap(
            const vector[vector[vector[double]]]& d,
            const vector[double]& p0)


cdef class RandomVariableWrapper:
    cdef RandomVariable* variable
    cdef set_variable(self, RandomVariable *variable)
    cdef RandomVariable *get_variable(self)
    cpdef eval(self)


cdef class MarkedProcessVariableWrapper:
    cdef MarkedProcessVariable* variable
    cdef set_variable(self, MarkedProcessVariable *variable)
    cdef MarkedProcessVariable *get_variable(self)
    cpdef eval(self)
