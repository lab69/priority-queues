"""
This module provides implementations of random variables for simulation model.

Random variables provide `eval()` method to compute a random value. Note, that
they don't provide any statistical metrics, like moments or lags.

Variables defined here are completely implemented in Python language. Refer
to `cqsim` subpackage for Cython/C++ more efficient implementations.
"""
import numpy as np
from typing import Tuple, Sequence


class RandomVariable:
    def eval() -> float:
        raise NotImplementedError


class MarkedProcessVariable:
    def eval() -> Tuple[int, float]:
        raise NotImplementedError


class RandomsFactory:
    def createMarkedMap(
        self,
        d: Sequence[np.ndarray],
        p0: np.ndarray
    ) -> MarkedProcessVariable:
        """
        Build a random variable for MMAP process.

        Parameters
        ----------
        d : sequence of np.ndarray
            matrices of MMAP
        p0 : np.ndarray
            initial probability distribution

        Returns
        -------
        var : MmapVariable
        """
        return MmapVariable(d, p0)

    def createPhaseType(
        self,
        s: np.ndarray,
        p: np.ndarray
    ) -> RandomVariable:
        """
        Build a PH random variable.

        Parameters
        ----------
        s : np.ndarray
            PH generator (subinfinitesimal matrix)
        p : np.ndarray
            PH initial PMF

        Returns
        -------
        var : PhaseTypeVariable
        """
        return PhaseTypeVariable(s, p)


class MmapVariable(MarkedProcessVariable):
    def __init__(self, d: Sequence[np.ndarray], p0: np.ndarray):
        """
        Constructor of MMAP variable.

        Since computation of initial PMF may be hard in other implementations
        (e.g., in C++) and to unify variable interface, we expect that
        initial PMF is computed outside, probably - in Mmap constructor.

        Moreover, no D or p0 validation is performed here. It is expected
        that data was validated before random variable creation.

        Parameters
        ----------
        d : sequence of np.ndarray
            matrices of MMAP
        p0 : np.ndarray
            initial probability distribution
        """
        # 1) Compute rates and order
        rates = -d[0].diagonal()
        self._order = len(rates)

        # 2) Then, we need to store cumulative transition probabilities P.
        #    P - matrix of shape N x (K*N), where P[I, J] is a prob., that:
        #      - new state will be J (mod N), and
        #      - if J < N, then no packet is generated, or
        #      - if J >= N, then packet of type J // N is generated.
        self._trans_pmf = np.hstack((
            d[0] + np.diag(rates),  # leftmost diagonal is zero
            *d[1:]
        )) / rates[:, None]

        # 3) Define random variables generators:
        # - random generators for time in each state:
        self.__rate_rnd = [Rnd(
            lambda n, r=r: np.random.exponential(1/r, size=n))
            for r in rates
        ]

        # - random generators of state transitions:
        trans_range = np.arange(self._order * len(d))
        self.__trans_rnd = [
            Rnd(lambda n, p0=p: np.random.choice(trans_range, p=p0, size=n))
            for p in self._trans_pmf
        ]

        # 4) Since we have the initial PMF p0, we find the initial state:
        self._state = np.random.choice(np.arange(self._order), p=p0)

    def eval(self) -> Tuple[int, float]:
        """
        Get next random value.
        """
        pkt_type = 0
        interval = 0.0
        # print('> start in state ', self._state)
        i = self._state
        while pkt_type == 0:
            interval += self.__rate_rnd[i]()
            j = self.__trans_rnd[i]()
            pkt_type, i = divmod(j, self._order)
        self._state = i
        return interval, pkt_type


class PhaseTypeVariable(RandomVariable):
    def __init__(self, s: np.ndarray, p: np.ndarray):
        """
        Constructor of PH random variable.

        Parameters
        ----------
        s : np.ndarray
            PH generator (subinfinitesimal matrix)
        p : np.ndarray
            PH initial PMF
        """
        rates = -s.diagonal()
        self._order = len(p)

        self._trans_pmf = np.hstack((
            s + np.diag(rates),
            -s.sum(axis=1)[:, None]
        )) / rates[:, None]

        # Create rnd caches:
        # ------------------
        all_states = np.arange(self._order + 1)
        # - random generator for initial state:
        self.__init_rnd = Rnd(
            lambda n: np.random.choice(all_states[:-1], p=p, size=n))
        # - random generators for time in each state:
        self.__rate_rnd = [Rnd(
            lambda n, r=r: np.random.exponential(1/r, size=n))
            for r in rates
        ]
        # - random generators of state transitions:
        self.__trans_rnd = [Rnd(
            lambda n, p0=p: np.random.choice(all_states, p=p0, size=n)
        ) for p in self._trans_pmf]

    def eval(self) -> float:
        """Get next random value."""
        interval = 0.0
        i = self.__init_rnd()
        while i < self._order:
            interval += self.__rate_rnd[i]()
            i = self.__trans_rnd[i]()
        return interval


class Rnd:
    def __init__(self, fn, cache_size=10000):
        self.__fn = fn
        self.__cache_size = cache_size
        self.__samples = []
        self.__index = cache_size

    def __call__(self):
        if self.__index >= self.__cache_size:
            self.__samples = self.__fn(self.__cache_size)
            self.__index = 0
        x = self.__samples[self.__index]
        self.__index += 1
        return x
