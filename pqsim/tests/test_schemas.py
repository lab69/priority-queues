from pqsim import inputs, schemas


def test_short_dist_input_data_schema():
    # 1) Load from a JSON-encoded string:
    string = '{"cv":1.0, "sk":2.0, "fn":"linear", "hp":34, "lp":42}'
    schema = schemas.ShortDistInputDataSchema()
    data = schema.loads(string)
    assert data == inputs.DistInputData(1.0, 2.0, "linear", 34, 42)

    # 2) Dump to dict and make sure numbers are rounded as expected:
    data = inputs.DistInputData(0.12345678, 5.54321321, "logarithm", 3.4, 4.2)
    assert schema.dump(data) == {
        'cv': 0.1235,
        'sk': 5.5432,
        'fn': 'logarithm',
        'hp': 3.4,
        'lp': 4.2
    }


def test_short_arrival_input_data_schema():
    # 1) Load from a JSON-encoded string:
    string = '{"cv":1.0, "sk":2.0, "fn":"linear", "hp":34, "lp":42, "lg":-1}'
    schema = schemas.ShortArrivalInputDataSchema()
    data = schema.loads(string)
    assert data == inputs.ArrivalInputData(1.0, 2.0, "linear", 34, 42, -1)

    # 2) Dump to dict and make sure numbers are rounded as expected:
    data = inputs.ArrivalInputData(1, 2, "exponent", 3.43214, 4.22998, -.54321)
    assert schema.dump(data) == {
        'cv': 1,
        'sk': 2,
        'fn': 'exponent',
        'hp': 3.4321,
        'lp': 4.23,
        'lg': -0.5432
    }


def test_short_queue_input_data_schema():
    # 1) Load from a JSON-encoded string:
    string = '{"sz": 10, "fn": "bingeom"}'
    schema = schemas.ShortQueueInputDataSchema()
    data = schema.loads(string)
    assert data == inputs.QueueInputData(10, "bingeom")

    # 2) Dump to dict:
    data = inputs.QueueInputData(13, "hyperbola")
    assert schema.dump(data) == {"sz": 13, "fn": "hyperbola"}


def test_short_explicit_dist_input_data_schema():
    # 1) Load from a JSON-encoded string:
    string = '{"cv":1.0, "sk":2.0, "rs":[3, 4, 5]}'
    schema = schemas.ShortExplicitDistInputDataSchema()
    data = schema.loads(string)
    assert data == inputs.ExplicitDistInputData(1.0, 2.0, [3, 4, 5])

    # 2) Dump to dict and make sure numbers are rounded as expected:
    data = inputs.ExplicitDistInputData(
        0.234234, -4.98767, (1.112233, 3.445566, 4.887766, 5.919191)
    )
    assert schema.dump(data) == {
        'cv': 0.2342,
        'sk': -4.9877,
        'rs': [1.1122, 3.4456, 4.8878, 5.9192]
    }


def test_short_explicit_arrival_input_data_schema():
    # 1) Load from a JSON-encoded string:
    string = '{"cv":1.0, "sk":2.0, "rs":[3, 4, 5], "lg":0.8}'
    schema = schemas.ShortExplicitArrivalInputDataSchema()
    data = schema.loads(string)
    assert data == inputs.ExplicitArrivalInputData(1.0, 2.0, [3, 4, 5], 0.8)

    # 2) Dump to dict and make sure numbers are rounded as expected:
    data = inputs.ExplicitArrivalInputData(
        0.234234, -4.98767, (1.112233, 3.445566, 4.887766, 5.919191), 2.39812
    )
    assert schema.dump(data) == {
        'cv': 0.2342,
        'sk': -4.9877,
        'rs': [1.1122, 3.4456, 4.8878, 5.9192],
        'lg': 2.3981
    }


def test_short_implicit_input_data_schema():
    # 1) Load from a JSON-encoded string:
    string = """{
        "m": {
            "ns": 10, 
            "nc": 2, 
            "q": { "sz":13, "fn":"linear" },
            "a": { "cv":2, "sk":7, "fn":"logarithm", "hp":3, "lp":8, "lg":-2 },
            "s": { "cv":0.1, "sk":-5.5, "fn":"hyperbola", "hp":34, "lp":42 }
        }, "mp": 123456
    }"""
    schema = schemas.ShortInputDataSchema()
    data = schema.loads(string)
    assert data.model == inputs.ModelInputData(
        num_servers=10,
        num_classes=2,
        queue=inputs.QueueInputData(13, "linear"),
        arrival=inputs.ArrivalInputData(2, 7, "logarithm", 3, 8, -2),
        service=inputs.DistInputData(0.1, -5.5, "hyperbola", 34, 42),
    )

    # 2) Dump to dict:
    assert schema.dump(data) == {
        "m": {
            "ns": 10,
            "nc": 2,
            "q": {"sz": 13, "fn": "linear"},
            "a": {"cv": 2, "sk": 7, "fn": "logarithm", "hp": 3, "lp": 8,
                  "lg": -2},
            "s": {"cv": 0.1, "sk": -5.5, "fn": "hyperbola", "hp": 34,
                  "lp": 42},
        },
        "mp": 123456
    }


def test_short_explicit_input_data_schema():
    # 1) Load from a JSON-encoded string:
    string = """{
        "m": {
            "ns": 13, 
            "q": { "sz":9, "fn":"always" },
            "a": { "cv":3, "sk":5, "rs":[3.4, 4.2, 5.9], "lg":-2 },
            "s": { "cv":0.2, "sk":2.5, "rs":[10.1, 20.2, 30.3] }
        },
        "mp": 98765
    }"""
    schema = schemas.ShortInputDataSchema()
    data = schema.loads(string)
    assert data.model == inputs.ExplicitModelInputData(
        num_servers=13,
        queue=inputs.QueueInputData(9, "always"),
        arrival=inputs.ExplicitArrivalInputData(3, 5, [3.4, 4.2, 5.9], -2),
        service=inputs.ExplicitDistInputData(0.2, 2.5, [10.1, 20.2, 30.3]),
    )
    assert data.max_packets == 98765

    # 2) Dump to dict:
    assert schema.dump(data) == {
        "m": {
            "ns": 13,
            "q": {"sz": 9, "fn": "always"},
            "a": {"cv": 3, "sk": 5, "rs": [3.4, 4.2, 5.9], "lg": -2},
            "s": {"cv": 0.2, "sk": 2.5, "rs": [10.1, 20.2, 30.3]}
        },
        "mp": 98765
    }
