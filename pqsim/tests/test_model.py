from typing import Optional, Any, Dict
from numpy.testing import assert_allclose
import pytest

from pqsim.stochastic import Mmap, Ph
from pqsim.model import Params, simulate as py_simulate
from pqsim.cqsim.model import simulate as c_simulate
from pqsim.statistics import FiniteDistribution, VarData, VarDataMixin


@pytest.mark.parametrize('sim, comment, default_rtol, default_max_packets', [
    (py_simulate, 'python simulator', .1, 200000),
    (c_simulate, 'C++/cython simulator', .05, 2000000),
])
def test_model(json_data, sim, comment, default_rtol, default_max_packets):
    """
    Check model against data record from JSON file.
    """
    if json_data.get('skip', False):
        return

    experiment_name = \
        f"{json_data['name']} via {comment} " \
        f"({json_data['description']})"
    rtol = json_data.get('rtol', default_rtol)
    max_packets = json_data.get('maxPackets', default_max_packets)

    # Build parameters:
    mmap = Mmap(*json_data['arrival'])
    services = tuple(
        Ph(service['s'], service['p'])
        for service in json_data['services'])
    params = Params(
        arrival=mmap,
        services=services,
        enqueue_prob=json_data['enqueueProb'],
        capacity=json_data['capacity'],
        num_servers=json_data.get('numServers', 1))

    # Run simulation:
    results = sim(params, max_packets)

    # Analyze results:
    _check_finite_distribution(
        record=json_data.get('systemSizeProbs', None),
        res_dist=results.system_size,
        res_dist_dict=results.system_size_of,
        title='system size', experiment_name=experiment_name, rtol=rtol)

    _check_var_data(
        record=json_data.get('systemSizeStats', None),
        res_dist=results.system_size,
        res_dist_dict=results.system_size_of,
        title='system size (var.data)',
        experiment_name=experiment_name, rtol=rtol)

    _check_finite_distribution(
        record=json_data.get('queueSizeProbs', None),
        res_dist=results.queue_size,
        res_dist_dict=results.queue_size_of,
        title='queue size', experiment_name=experiment_name, rtol=rtol)

    _check_var_data(
        record=json_data.get('queueSizeStats', None),
        res_dist=results.queue_size,
        res_dist_dict=results.queue_size_of,
        title='queue size (var.data)',
        experiment_name=experiment_name, rtol=rtol)

    _check_finite_distribution(
        record=json_data.get('numBusyServersProbs', None),
        res_dist=results.busy_servers,
        title='busy servers', experiment_name=experiment_name, rtol=rtol)

    _check_var_data(
        record=json_data.get('numBusyServersStats', None),
        res_dist=results.busy_servers,
        title='busy servers (var.data)',
        experiment_name=experiment_name, rtol=rtol)

    _check_finite_distribution(
        record=json_data.get('serverDutyProbs', None),
        res_dist=results.server_duty,
        title='server duty', experiment_name=experiment_name, rtol=rtol)

    _check_scalars(
        record=json_data.get('lossProbs', None),
        res_dist=results.loss_prob,
        res_dist_dict=results.loss_prob_of,
        title='loss prob.', experiment_name=experiment_name, rtol=rtol)

    _check_var_data(
        record=json_data.get('arrivalStats', None),
        res_dist=results.arrivals,
        res_dist_dict=results.arrivals_of,
        title='arrivals', experiment_name=experiment_name, rtol=rtol)

    _check_var_data(
        record=json_data.get('serviceStats', None),
        res_dist=results.services,
        res_dist_dict=results.services_of,
        title='service intervals',
        experiment_name=experiment_name, rtol=rtol)

    _check_var_data(
        record=json_data.get('responseTimeStats', None),
        res_dist=results.response_time,
        res_dist_dict=results.response_time_of,
        title='response times', experiment_name=experiment_name, rtol=rtol)

    _check_var_data(
        record=json_data.get('waitTimeStats', None),
        res_dist=results.wait_time,
        res_dist_dict=results.wait_time_of,
        title='waiting times', experiment_name=experiment_name, rtol=rtol)


def _check_finite_distribution(
        record: Any,
        res_dist: FiniteDistribution,
        res_dist_dict: Optional[Dict[int, FiniteDistribution]] = None,
        experiment_name: str = '',
        title: str = '',
        rtol=.05):
    """
    Helper to validate PMF from results against the given record in JSON data.

    JSON data can be either:

    - a tuple/list with PMF: this will be matched against `res_dist.pmf`
    - a dict with keys 'total' and 'types'.

    In the latter case, key 'total' stores the expected PMF that is matched
    against `res_dist.pmf`, and 'types' key stores a dict, in which a PMF
    is given for different packet types (sotres `pkt_type -> PMF` items).
    These PMFs will be matched against `res_dist_dict[pkt_type]`, if
    `res_dist_dict` is not None.

    Parameters
    ----------
    record : dict, sequence of float or None
    res_dist : FiniteDistribution
        stores an overall PMF (regardless packet types), e.g. `system_size`
    res_dist_dict : FiniteDistribution, optional
        stores a dictionary of FiniteDistributions per packet type, e.g.
        `system_size_of`. By default, `None`.
    experiment_name : str
        a line built from data['name'] and data['description']
    title : str
        a short line about the metric being checked
    rtol : float
    """
    if record is None:
        return
    if type(record) is dict:
        # If record is a dictionary, assume it may have keys:
        # - 'total': PMF that should match res_dist.pmf
        # - 'types': another dict with `pkt_type -> PMF` items
        expected = record.get("total", None)
        if expected is not None:
            actual_pmf = res_dist.pmf
            common_order = min(len(actual_pmf), len(expected))
            assert_allclose(
                actual_pmf[:common_order],
                expected[:common_order],
                rtol=rtol,
                err_msg=_get_err_msg(experiment_name, title))
        if res_dist_dict is None or 'types' not in record:
            return
        for pkt_type, expected in record['types'].items():
            pkt_type = int(pkt_type)
            actual_pmf = res_dist_dict[pkt_type].pmf
            common_order = min(len(actual_pmf), len(expected))
            assert_allclose(
                actual_pmf[:common_order], expected[:common_order], rtol=rtol,
                err_msg=_get_err_msg(experiment_name, title, pkt_type))
    else:
        # If here, record is not a dictionary, so treat it as PMF
        actual_pmf = res_dist.pmf
        common_order = min(len(actual_pmf), len(record))
        assert_allclose(
            actual_pmf[:common_order],
            record[:common_order],
            rtol=rtol,
            err_msg=_get_err_msg(experiment_name, title))


def _check_scalars(
        record: Any,
        res_dist: float,
        res_dist_dict: Optional[Dict[int, float]] = None,
        experiment_name: str = '',
        title: str = '',
        rtol=.05):
    """
    Helper to validate scalar results.

    JSON data can be either:

    - a float number: this will be matched against `res_dist`
    - a dict with keys 'total' and 'types'.

    In the latter case, key 'total' stores the expected value that is matched
    against `res_dist`, and 'types' key stores a dict, in which a scalar value
    is given for different packet types (sotres `pkt_type -> float` items).
    These values will be matched against `res_dist_dict[pkt_type]`, if
    `res_dist_dict` is not None.

    Parameters
    ----------
    record : dict, float or None
    res_dist : float
        stores scalar value (regardless packet types), e.g. `loss_prob`
    res_dist_dict : dict of int to float, optional
        stores a dictionary of floats per packet type, e.g. `loss_prob_of`.
        By default `None`.
    experiment_name : str
        a line built from data['name'] and data['description']
    title : str
        a short line about the metric being checked
    rtol : float
    """
    if record is None:
        return
    if type(record) is dict:
        # If record is a dictionary, assume it may have keys:
        # - 'total': value that should match res_dist
        # - 'types': another dict with `pkt_type -> float` items
        expected = record.get("total", None)
        if expected is not None:
            assert_allclose(res_dist, expected, rtol=rtol,
                            err_msg=_get_err_msg(experiment_name, title))
        if res_dist_dict is None or 'types' not in record:
            return
        for pkt_type, expected in record['types'].items():
            pkt_type = int(pkt_type)
            assert_allclose(
                res_dist_dict[pkt_type], expected, rtol=rtol,
                err_msg=_get_err_msg(experiment_name, title, pkt_type))
    else:
        # If here, record is not a dictionary, so treat it as PMF
        assert_allclose(
            res_dist, record, rtol=rtol,
            err_msg=_get_err_msg(experiment_name, title))


def _check_var_data(
        record: Any,
        res_dist: VarDataMixin,
        res_dist_dict: Optional[Dict[int, VarDataMixin]] = None,
        experiment_name: str = '',
        title: str = '',
        rtol=.05):
    """
    Helper to validate stats.

    JSON data can be either:

    - a dict with 'avg', 'std': this will be matched against `res_dist`
    - a dict with keys 'total' and 'types'.

    In the latter case, key 'total' stores the expected stats value that is
    matched against `res_dist`, and 'types' key stores a dict, in which
    a stats value is given for different packet types (sotres
    `pkt_type -> Stats` items). These values will be matched against
    `res_dist_dict[pkt_type]`, if `res_dist_dict` is not None.

    Parameters
    ----------
    record : dict or None
    res_dist : float
        stores Stats value (regardless packet types), e.g. `arrivals`
    res_dist_dict : dict, optional
        stores a dictionary of Stats per packet type, e.g. `arrivals_of`.
        By default `None`.
    experiment_name : str
        a line built from data['name'] and data['description']
    title : str
        a short line about the metric being checked
    rtol : float
    """
    if record is None:
        return

    def _check(real_stats: VarData, expected_stats, err_msg):
        if 'avg' in expected_stats:
            assert_allclose(
                real_stats.avg, expected_stats['avg'], rtol=rtol,
                err_msg=err_msg)
        if 'std' in expected_stats:
            assert_allclose(
                real_stats.std, expected_stats['std'], rtol=rtol,
                err_msg=err_msg)

    if ('total' in record) or ('types' in record):
        # If record is a dictionary, assume it may have keys:
        # - 'total': value that should match res_dist
        # - 'types': another dict with `pkt_type -> float` items
        expected = record.get("total", None)
        if expected is not None:
            _check(res_dist, expected, _get_err_msg(experiment_name, title))
        if res_dist_dict is None or 'types' not in record:
            return
        for pkt_type, expected in record['types'].items():
            pkt_type = int(pkt_type)
            _check(res_dist_dict[pkt_type], expected,
                   err_msg=_get_err_msg(experiment_name, title, pkt_type))
    else:
        # If here, record is not a dictionary, so treat it as PMF
        _check(res_dist, record, err_msg=_get_err_msg(experiment_name, title))


def _get_err_msg(experiment_name, check_title, pkt_type=None):
    if pkt_type is None:
        return f"{check_title} at {experiment_name}"
    return f"{check_title} for type={pkt_type} at {experiment_name}"
