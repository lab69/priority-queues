import pytest
from pqsim.model import Packet, find_insert_index


@pytest.mark.parametrize('queue, pkt_type, index', [
    ([], 1, 0),
    ([1, 1, 1], 1, 3),
    ([1, 1, 1], 2, 3),
    ([2, 2, 2], 1, 0),
    ([2, 2, 2], 2, 3),
    ([2, 2, 3, 5], 1, 0),
    ([2, 2, 3, 5], 2, 2),
    ([2, 2, 3, 5], 3, 3),
    ([2, 2, 3, 5], 4, 3),
    ([2, 2, 3, 5], 5, 4),
    ([2, 2, 3, 5], 6, 4),
])
def test_find_insert_index(queue, pkt_type, index):
    packet_queue = [Packet(type_, 0.0) for type_ in queue]
    assert find_insert_index(packet_queue, pkt_type) == index
