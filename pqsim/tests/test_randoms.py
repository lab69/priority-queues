import numpy as np
from numpy.testing import assert_allclose

from pqsim import randoms as py_randoms


#
# RANDOM GENERATOR HELPER
#
def test_random_exponential_generator():
    RATE = 13
    g = py_randoms.Rnd(
        lambda size: np.random.exponential(1/RATE, size=size),
        cache_size=1000)

    samples = np.asarray([g() for _ in range(10000)])

    assert_allclose(samples.mean(), 1 / RATE, rtol=0.05)
    assert_allclose(samples.std(), 1 / RATE, rtol=0.05)
