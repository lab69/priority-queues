import pytest
import numpy as np
from numpy.testing import assert_allclose
from pqsim.stochastic import str_array, parse_array, Ph, Mmap


#
# TEST ARRAYS PARSING AND TO_STRING
#
@pytest.mark.parametrize('array,expected', [
    (np.asarray([]), ''),
    (np.asarray([1]), '1'),
    (np.asarray([[1]]), '1'),
    (np.asarray([10, 20]), '10, 20'),
    (np.asarray([[2, 4, 6], [8, 10, 12]]), '2, 4, 6; 8, 10, 12')
])
def test_str_array(array, expected):
    value = str_array(array)
    assert value == expected


@pytest.mark.parametrize('array,ndim', [
    (np.asarray([]), None),
    (np.asarray([1.23]), 1),
    (np.asarray([[1.23]]), 2),
    (np.asarray([1.23]), None),
    (np.asarray([[2.1, 4.2, 6e-8], [8, 10, 12]]), None)
])
def test_parse_array(array, ndim):
    string = str_array(array)
    if ndim is not None:
        value = parse_array(string, ndim=ndim)
    else:
        value = parse_array(string)
    assert_allclose(array, value)


#
# PHASE-TYPE DISTRIBUTION
#
def test_ph_properties():
    s = [[-2, 1], [3, -5]]
    p0 = [0.5, 0.5]

    ph = Ph(s, p0)

    assert ph.order == 2
    assert_allclose(ph.rates, [2, 5])
    assert_allclose(ph.s, s)
    assert_allclose(ph.init_pmf, p0)


# noinspection DuplicatedCode
def test_ph_generate_exp():
    RATE = 5.0
    ph = Ph.exponential(RATE)
    samples = np.asarray([ph() for _ in range(5000)])
    assert_allclose(samples.mean(), [1/RATE], rtol=0.1)
    assert_allclose(samples.std(), [1/RATE], rtol=0.1)


def test_generation_from_ph_erlang_of_order_3():
    RATE = 3.0
    SCALE = 6
    N = 5000
    ph = Ph.erlang(RATE, SCALE)

    samples = np.asarray([ph() for _ in range(N)])

    assert_allclose(samples.mean(), SCALE / RATE, rtol=0.1)
    assert_allclose(samples.std(), SCALE**0.5 / RATE, rtol=0.1)


#
# MULTI-CLASS MAP
#
def test_mmap_properties():
    d0 = [[-25, 10, 15], [2, -10, 0], [0, 3, -20]]
    d1 = [[0, 0, 0], [0, 3, 0], [3, 8, 0]]
    d2 = [[0, 0, 0], [5, 0, 0], [0, 2, 4]]

    mmap = Mmap(d0, d1, d2)

    assert mmap.order == 3
    assert mmap.num_types == 2

    assert_allclose(mmap.d(0), d0)
    assert_allclose(mmap.d(1), d1)
    assert_allclose(mmap.d(2), d2)

    assert_allclose(mmap.rates, [25, 10, 20])
    assert_allclose(mmap.generator, [[-25, 10, 15], [7, -7, 0], [3, 13, -16]])

    # Stationary state PMF were computed manually:
    assert_allclose(mmap.state_pmf, [28/143, 355/572, 105/572])


@pytest.mark.parametrize('nc', [1, 2, 3])
def test_mmap_generate_sifted_poisson_with_equal_probabilities(nc):
    """Generate MMAP with N classes, each is Poisson flow.

    All classes have the same probability. E.g., if number of classes is two,
    then each classes is generated with probability 1/2.

    Intervals between packets are determined by RATE only, so their mean
    and std.dev. is equal to 1/RATE.

    In this test we check, that:

    - intervals mean and std.dev. are equal to 1/RATE, where RATE
      is the total rate of Poisson flows from all classes.

    - each message class appears with almost equal probability.
    """
    RATE = 5.0
    N = 50000
    mmap = Mmap.exponential(RATE, nc=nc)
    samples = [mmap() for _ in range(N)]

    class_pmf = [len([s for s in samples if s[1] == c+1])/N for c in range(nc)]
    assert_allclose(class_pmf, [1/nc]*nc, rtol=0.1)

    intervals = np.asarray([sample[0] for sample in samples])
    assert_allclose(intervals.mean(), [1/RATE], rtol=0.1)
    assert_allclose(intervals.std(), [1/RATE], rtol=0.1)


@pytest.mark.parametrize('p', [None, [0.2, 0.8]])
def test_mmap_generate_sifted_poisson_with_none_or_different_probabilities(p):
    """Generate MMAP with N classes, each is Poisson flow.

    If probabilities are provided, then each class has the given probability.
    Otherwise, it is assumed that only one class exists.

    In this test we check, that:

    - intervals mean and std.dev. are equal to 1/RATE, where RATE
      is the total rate of Poisson flows from all classes.

    - each message class has the given probability.
    """
    RATE = 5.0
    N = 5000
    NC = len(p) if p is not None else 1

    mmap = Mmap.exponential(RATE, p=p)
    samples = [mmap() for _ in range(N)]

    class_pmf = [len([s for s in samples if s[1] == c+1])/N for c in range(NC)]
    assert_allclose(class_pmf, p if p is not None else [1], rtol=0.1)

    intervals = np.asarray([sample[0] for sample in samples])
    assert_allclose(intervals.mean(), [1/RATE], rtol=0.1)
    assert_allclose(intervals.std(), [1/RATE], rtol=0.1)


def test_mmap_generate_sifted_erlang_with_equal_probabilities():
    """Generate MMAP with N classes, each is Erlang flow.

    All classes have the same probability. E.g., if number of classes is two,
    then each classes is generated with probability 1/2.

    In this test we check, that:

    - intervals mean and std.dev. are equal to K/RATE, where RATE
      is the total rate of Poisson flows from all classes and K is the scale

    - each message class appears with almost equal probability.
    """
    RATE = 5.0
    SCALE = 3
    NC = 2
    N = 20000
    mmap = Mmap.erlang(RATE, SCALE, nc=NC)
    samples = [mmap() for _ in range(N)]

    assert_allclose(mmap.generator, [
        [-RATE, RATE, 0],
        [0, -RATE, RATE],
        [RATE, 0, -RATE]
    ])
    assert_allclose(mmap.d(0), [
        [-RATE, RATE, 0],
        [0, -RATE, RATE],
        [0, 0, -RATE]
    ])
    assert_allclose(mmap.d(1), [[0, 0, 0], [0, 0, 0], [RATE/2, 0, 0]])
    assert_allclose(mmap.d(2), [[0, 0, 0], [0, 0, 0], [RATE/2, 0, 0]])

    intervals = np.asarray([sample[0] for sample in samples])
    assert_allclose(intervals.mean(), [SCALE / RATE], rtol=0.1)
    assert_allclose(intervals.std(), [SCALE**0.5 / RATE], rtol=0.1)


def test_mmap_generate_sifted_erlang_with_different_probabilities():
    """Generate MMAP with N classes, each is Erlang flow.

    Classes have probabilities provided in a vector.

    In this test we check, that:

    - intervals mean and std.dev. are equal to K/RATE, where RATE
      is the total rate of Poisson flows from all classes and K is the scale

    - each message class appears with almost equal probability.
    """
    RATE = 5.0
    SCALE = 3
    N = 20000
    P = [0.2, 0.3, 0.5]
    NC = len(P)
    mmap = Mmap.erlang(RATE, SCALE, p=P)
    samples = [mmap() for _ in range(N)]

    assert_allclose(mmap.d(1), [[0, 0, 0], [0, 0, 0], [0.2 * RATE, 0, 0]])
    assert_allclose(mmap.d(2), [[0, 0, 0], [0, 0, 0], [0.3 * RATE, 0, 0]])
    assert_allclose(mmap.d(3), [[0, 0, 0], [0, 0, 0], [0.5 * RATE, 0, 0]])

    class_pmf = [len([s for s in samples if s[1] == c+1])/N for c in range(NC)]
    assert_allclose(class_pmf, P, rtol=0.1)

    intervals = np.asarray([sample[0] for sample in samples])
    assert_allclose(intervals.mean(), [SCALE / RATE], rtol=0.1)
    assert_allclose(intervals.std(), [SCALE**0.5 / RATE], rtol=0.1)
