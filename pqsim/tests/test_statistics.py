import numpy as np
from numpy.testing import assert_allclose
from pqsim.statistics import est_series_moment, Series, est_std


#
# Helpers tests
# ----------------------------------------------------------------------------
def test_get_std():
    moments = [3.0, 13.0]  # Ex, E(x**2) => std.dev = (E(x**2) - (Ex)**2)**0.5
    assert_allclose(est_std(moments), 2.0)


#
# Numerical scalar series tests
# ----------------------------------------------------------------------------
def test_est_series_moment():
    window = np.asarray([1, 0, 4, 4])
    moments = [4.75, 29.75]
    n = 8

    new_moments = [
        est_series_moment(i + 1, m, window, n)
        for i, m in enumerate(moments)
    ]

    assert_allclose(new_moments, [3.5, 19.0])


# def test_est_err():
#     x = np.asarray([10, 99, 1e-13])
#     y = np.asarray([6, 101, 2*1e-13])
#     expected = [0.5, 0.02, 2e-13]
#     assert_allclose(est_err(x, y), expected, rtol=1e-12)

# def test_est_time_series_moments():
#     window = [
#         TimeValue(20, 2), TimeValue(22, 3), TimeValue(23, 4),
#         TimeValue(34, 3)]
#     moments = [0.75, 1.05]
#     t = 42

#     new_moments = [
#         est_time_series_moment(i+1, m, window, t)
#         for i, m in enumerate(moments)]

#     assert_allclose(new_moments, [2.14, 6.81], rtol=0.01)


# def test_est_time_series_value_prob():
#     window = [TimeValue(20, 2), TimeValue(22, 3), TimeValue(23, 4),
#               TimeValue(34, 3)]
#     t = 42

#     p0 = est_time_series_value_prob(0, window, t=t, prob=8/20)
#     p1 = est_time_series_value_prob(1, window, t=t, prob=9/20)
#     p2 = est_time_series_value_prob(2, window, t=t, prob=3/20)
#     p3 = est_time_series_value_prob(3, window, t=t, prob=0)
#     p4 = est_time_series_value_prob(4, window, t=t, prob=0)

#     assert_allclose(p0, 8/42)
#     assert_allclose(p1, 9/42)
#     assert_allclose(p2, 5/42)
#     assert_allclose(p3, 9/42)
#     assert_allclose(p4, 11/42)


#
# Test Series methods
# ----------------------------------------------------------------------------
def test_series__record_and_moments():
    """
    Test that Series.record() adds data, and moments property returns properly
    estimated moments.
    """
    AVG = 100
    samples = np.random.exponential(AVG, size=10000)
    # on_matched = Mock()

    # noinspection PyTypeChecker
    series = Series(num_moments=2, window_size=100)

    for sample in samples:
        series.record(sample)

    # on_matched.assert_called_once_with(34)
    assert_allclose(series.moments, [AVG, 2 * AVG**2], rtol=0.1)


def test_series__commit():
    """
    Test that calling commit() method estimates moments on Series.
    """
    WINDOW_SIZE = 40
    DATA = [4, 6]*5 + [7, 9]*20  # [10 ITEMS of 4,6] + [40 ITEMS of 7,9]

    series = Series(2, window_size=WINDOW_SIZE)

    # Now everything will go to window. Record first 20 items:
    for i in range(10):
        series.record(DATA[i])

    # Commit and make sure, that first moment is 5, second = sqrt(26):
    series.commit()
    assert_allclose(series.moments[0], 5.0, rtol=0.001)
    assert_allclose(series.moments[1], 26, rtol=0.001)

    # Write next values, and make sure they overwrite window content:
    for i in range(20):
        series.record(DATA[10 + i])
    assert list(series.window) == ([7, 9]*10) + ([0]*20)

    # Continue writing to make it full, and validate moments:
    for i in range(20):
        series.record(DATA[30 + i])

    m1 = np.mean(DATA)
    m2 = (np.var(DATA) + m1**2)
    assert_allclose(series.moments[0], m1, rtol=0.001)
    assert_allclose(series.moments[1], m2, rtol=0.001)

    # Also make sure that calling more commits change nothing:
    series.commit()
    series.commit()
    assert_allclose(series.moments[0], m1, rtol=0.001)
    assert_allclose(series.moments[1], m2, rtol=0.001)


#
# Time-value series tests
# ----------------------------------------------------------------------------
# TSS_SAMPLES_1 = [
#     TimeValue(8, 1), TimeValue(12, 2), TimeValue(15, 1), TimeValue(20, 2),
#     TimeValue(22, 3), TimeValue(23, 4), TimeValue(34, 3), TimeValue(42, 2),
#     TimeValue(48, 3), TimeValue(51, 2), TimeValue(62, 3), TimeValue(69, 4),
# ]

# # TSS_SAMPLES_2 has the same tail as TSS_SAMPLES_1, but shifted on dt=100:
# TSS_SAMPLES_2 = [
#     TimeValue(4, 1), TimeValue(5, 2), TimeValue(9, 3), TimeValue(11, 4),
#     TimeValue(15, 5), TimeValue(90, 4), TimeValue(91, 3), TimeValue(92, 2),
#     TimeValue(100, 0)
# ] + [TimeValue(tv.time + 100, tv.value) for tv in TSS_SAMPLES_1]


# def test_time_series__record_moments_pmf():
#     """
#     Validate TimeSeries.record() adds TimeValue data, and moments and pmf
#     properties contain valid data.
#     """
#     samples = TSS_SAMPLES_2
#     on_matched = Mock()

#     # noinspection PyTypeChecker
#     series = TimeSeries(
#         num_moments=2,
#         window_size=4,
#         skip=8,
#         tol=0.06,
#         on_matched=on_matched,
#         key=13)

#     for tv in samples:
#         series.record(tv.time, tv.value)

#     # on_matched.assert_called_once_with(13)
#     assert_allclose(series.moments, [2.23, 6.43], 0.01)
#     assert_allclose(series.pmf, [8/69, 9/69, 22/69, 19/69, 11/69], 0.01)


# def test_time_series__commit():
#     """
#     Test that commit() method call estimates moments in TimeSeries.
#     """
#     VALUES = [4, 6, 4, 6] + [7, 9]*4 + [100]  # 13 items
#     TIME = [x + 100 for x in range(13)]
#     WINDOW_SIZE = 8
#     SKIP = 5

#     series = TimeSeries(2, window_size=WINDOW_SIZE, skip=SKIP)

#     # 1) Record SKIP huge values with time < TIME[0], make sure they are
#     #    skipped (later, when estimating moments):
#     for time in range(SKIP):
#         series.record(time, 100000)

#     # 2) Record first 5 records and commit them (the last record:
#     for time, value in zip(TIME[:5], VALUES[:5]):  # 4, 6, 4, 6, 7
#         series.record(time, value)

#     series.commit()
#     assert_allclose(series.moments[0], 5.0, rtol=0.0001)
#     assert_allclose(series.moments[1], 26, rtol=0.0001)

#     # 3) Record a couple of records more and make sure they overwrite
#     #    existing window except the last record:
#     for time, value in zip(TIME[5:9], VALUES[5:9]):  # 9, 7, 9, 7
#         series.record(time, value)
#     assert list(series.window) == \
#            [TimeValue(104, 7), TimeValue(105, 9),
#             TimeValue(106, 7), TimeValue(107, 9)] + \
#            [None] * 4

#     # 4) Now record all the rest of the records and make sure
#     for time, value in zip(TIME[9:], VALUES[9:]):
#         series.record(time, value)
#     m1 = np.mean(VALUES[:-1])
#     m2 = np.var(VALUES[:-1]) + m1**2
#     assert_allclose(series.moments[0], m1, rtol=0.0001)
#     assert_allclose(series.moments[1], m2, rtol=0.0001)

#     # 5) Make sure additional commit calls do not change estimates:
#     series.commit()
#     series.commit()
#     assert_allclose(series.moments[0], m1, rtol=0.0001)
