import numpy as np
import pytest
from numpy.testing import assert_allclose
from pqsim import outputs, inputs
from pqsim.model import Results, Params
from pqsim.statistics import VarData, FiniteDistribution
from pqsim.stochastic import Mmap, Ph


@pytest.fixture
def sample_params():
    return Params(
        arrival=Mmap.exponential(23.0, nc=3),
        services=(Ph.exponential(7), Ph.exponential(13), Ph.exponential(15)),
        enqueue_prob=np.ones((3, 5)),
        capacity=4,
        num_servers=2
    )


@pytest.fixture
def sample_results():
    return Results(
        time=0,
        system_size=FiniteDistribution(
            [0.06424600453, 0.1330938058, 0.1337759557, 0.1376418685,
             0.1482693509, 0.1714414553, 0.2115315592]),
        queue_size=FiniteDistribution(
            [0.331115766, 0.1376418685, 0.1482693509, 0.1714414553,
             0.2115315592]),
        busy_servers=FiniteDistribution(
            [0.06424600453, 0.1330938058, 0.8026601897]),
        server_duty=FiniteDistribution(
            [0.1307929074, 0.4357761648, 0.2323695118, 0.201061416]),
        response_time=VarData(
            [0.1943921538, 0.06901995553, 0.03577110004], 78789),
        wait_time=VarData(
            [0.0987412552, 0.03223844002, 0.01681740192], 78791),
        arrivals=VarData(
            [0.04335440458, 0.003751348872, 0.000485073345], 100000),
        services=VarData(
            [0.09565801674, 0.02055228364, 0.007285225591], 78791),
        loss_prob=0.2120421204,
        system_size_of={
            1: FiniteDistribution(
                [0.307254633, 0.3646895737, 0.2180280409, 0.08474803049,
                 0.02123331464, 0.003715272801, 0.0003311345516]),
            2: FiniteDistribution(
                [0.3966586766, 0.3484992835, 0.1771758904, 0.06233599339,
                 0.01362321972, 0.001674988398, 3.194795841e-05]),
            3: FiniteDistribution(
                [0.2818186726, 0.2821047591, 0.2352129873, 0.1443701648,
                 0.04884546628, 0.007270164464, 0.0003777854912])
        },
        queue_size_of={
            1: FiniteDistribution(
                [0.7667296404, 0.1852088216, 0.04102674417, 0.006467637835,
                 0.0005671559486]),
            2: FiniteDistribution(
                [0.6529399215, 0.2328318872, 0.08983531965, 0.0218944837,
                 0.002498387984]),
            3: FiniteDistribution(
                [0.4466707632, 0.2293474322, 0.2007291749, 0.1062985045,
                 0.01695412526])
        },
        response_time_of={
            1: VarData([0.1908519357, 0.06109434162, 0.02745903392], 26358),
            2: VarData([0.1581362959, 0.04437152913, 0.01789418381], 26125),
            3: VarData([0.2339457674, 0.1014400677, 0.06185350997], 26306)
        },
        wait_time_of={
            1: VarData([0.04752094766, 0.006683228271, 0.001501243364], 26360),
            2: VarData([0.08101311729, 0.01983136287, 0.007531484209], 26125),
            3: VarData([0.1676728643, 0.07016782009, 0.04138702633], 26306)
        },
        arrivals_of={
            1: VarData([0.04341376929, 0.0037539156, 0.0004857323175], 33383),
            2: VarData([0.04325795691, 0.003736801505, 0.0004810235453], 33069),
            3: VarData([0.04339040243, 0.003763134426, 0.0004884095899], 33548)
        },
        services_of={
            1: VarData([0.143352533, 0.04084551264, 0.0172821936], 26360),
            2: VarData([0.07712317857, 0.01192102937, 0.002771984149], 26125),
            3: VarData([0.06627290304, 0.008789263857, 0.001749923988], 26306)
        },
        loss_prob_of={
            1: 0.2103528848,
            2: 0.2099851825,
            3: 0.2157505664
        },
    )


def assert_dict_allclose(d1, d2, rtol=1e-3, atol=1e-9):
    assert set(d1.keys()) == set(d2.keys())
    for key in d1.keys():
        assert_allclose(d1[key], d2[key], rtol=rtol, atol=atol,
                        err_msg=f"key: {key}")


# noinspection PyPep8Naming
def test_VarOutputData(sample_results):
    value = sample_results.response_time

    # Expected values are:
    expected = {
        'avg': 0.1943921538092235,
        'std': 0.17672477490264796,
        'skew': 1.850160876388957
    }

    # 1) Create data object:
    data = outputs.VarOutputData.create(value)
    assert_allclose(data.avg, expected['avg'])
    assert_allclose(data.std, expected['std'])
    assert_allclose(data.skew, expected['skew'])

    # 2) Serialize to dictionary:
    serialized = outputs.VarOutputData.schema().dump(data)
    assert_dict_allclose(serialized, expected)

    # 3) Deserialize, but from json:
    deserialized = outputs.VarOutputData.from_json(data.to_json())
    assert_dict_allclose(deserialized.to_dict(), expected)


# noinspection PyPep8Naming
def test_DistOutputData(sample_results):
    value = sample_results.busy_servers

    # Expected values:
    expected = {
        'avg': 1.7384141851495816,
        'std': 0.5671425617718869,
        'skew': -2.065238454746717,
        'pmf': [0.064246, 0.13309381, 0.80266019]
    }

    # 1) Create data object:
    data = outputs.DistOutputData.create(value)
    assert_allclose(data.avg, expected['avg'])
    assert_allclose(data.std, expected['std'])
    assert_allclose(data.skew, expected['skew'])
    assert_allclose(data.pmf, expected['pmf'])

    # 2) Serialize to dictionary:
    serialized = outputs.DistOutputData.schema().dump(data)
    assert_dict_allclose(serialized, expected)

    # 3) Deserialize, but from json:
    deserialized = outputs.DistOutputData.from_json(data.to_json())
    assert_dict_allclose(deserialized.to_dict(), expected)


@pytest.fixture
def expected_result_data():
    return {
        "hp": {
            "pl": 0.2104,
            "ss": 1.1605,
            "qs": 0.2889,
            "nb": 0.4358,
            "rt": 0.1909,
        },
        "lp": {
            "pl": 0.2158,
            "ss": 1.4196,
            "qs": 1.0175,
            "nb": 0.2011,
            "rt": 0.2339,
        },
        "pl": 0.2120,
        "ss": 3.5330,
        "qs": 1.7946,
        "nb": 1.7384,
        "rt": 0.1944,
    }


# noinspection PyPep8Naming
def test_ResultData(sample_results, expected_result_data):
    expected = expected_result_data
    data = outputs.ResultData.create(sample_results)
    schema = outputs.ResultData.schema()
    assert schema.dump(data) == expected
    assert schema.load(expected).to_json()


# noinspection PyPep8Naming
def test_OutputData_uses_short_input_schema(
        sample_results,
        expected_result_data):
    ar = 23.0 / 3
    input_data = inputs.InputData(
        model=inputs.ExplicitModelInputData(
            num_servers=2,
            queue=inputs.QueueInputData(4, "always"),
            arrival=inputs.ExplicitArrivalInputData(1.0, 2.0, [ar]*3, 0.0),
            service=inputs.ExplicitDistInputData(1.0, 2.0, [7, 13, 15]),
        ),
        max_packets=100000
    )
    data = outputs.OutputData(
        inp=input_data,
        out=outputs.ResultData.create(sample_results)
    )
    schema = outputs.OutputData.schema()
    assert schema.dump(data) == {
        "i": {
            "m": {
                "ns": 2,
                "q": {"sz": 4, "fn": "always"},
                "a": {"cv": 1, "sk": 2, "rs": [7.6667, 7.6667, 7.6667],
                      "lg": 0},
                "s": {"cv": 1, "sk": 2, "rs": [7, 13, 15]}
            },
            "mp": 100000
        },
        "o": expected_result_data
    }
