from unittest.mock import patch, call

import numpy as np
import pytest
from numpy.testing import assert_allclose

from pqsim import inputs
from pqsim.stochastic import estimate_mmap_rates, estimate_ph_rate, Mmap, Ph


#
# VALIDATE APPROXIMATION FUNCTIONS
# - approximate_between()
# - approximate_enqueue_probs()
# ----------------------------------------------------------------------------
@pytest.mark.parametrize('fn, y1, yn, n, expected', [
    # Linear function:
    ("linear", 1., 5., 2, [1., 5.]),
    ("linear", 1., 5., 1, [1.]),
    ("linear", 1., 5., 3, [1., 3., 5.]),
    ("linear", 10., 2., 5, [10., 8., 6., 4., 2.]),
    # Parabola:
    ("parabola", 3.4, 4.2, 2, [3.4, 4.2]),
    ("parabola", 3.4, 4.2, 1, [3.4]),
    ("parabola", 10., 37., 4, [10., 13., 22., 37.]),
    ("parabola", 4.2, 2.2, 3, [4.2, 3.7, 2.2]),
    # Hyperbola:
    ("hyperbola", 17., 9., 2, [17., 9.]),
    ("hyperbola", 17., 9., 1, [17.]),
    ("hyperbola", 17., 9., 5, [17., 12., 10.333, 9.5, 9.]),
    ("hyperbola", 5., 8.75, 4, [5., 7.5, 8.333, 8.75]),
    # Exponent:
    ("exponent", 70., 24010., 2, [70., 24010.]),
    ("exponent", 70., 24010., 1, [70.]),
    ("exponent", 70., 24010., 4, [70., 490., 3430., 24010.]),
    ("exponent", 5., 0.3125, 5, [5., 2.5, 1.25, 0.625, 0.3125]),
    # Logarithm:
    ("logarithm", 8., 14.931, 2, [8., 14.931]),
    ("logarithm", 8., 14.931, 1, [8.]),
    ("logarithm", 8., 14.931, 4, [8., 11.466, 13.493, 14.931]),
    ("logarithm", 8., 2.507, 3, [8., 4.534, 2.507]),
])
def test_approximate_between(fn, y1, yn, n, expected):
    """Validate approximate_between() function.
    """
    actual = inputs.approximate_between(fn, y1=y1, yn=yn, n=n)
    assert_allclose(actual, expected, rtol=.01, atol=.001)


def test_approximate_between__bad_args():
    """
    Validate that approximate_between() raise `ValueError` if:

    (1) approximation function is not one of ALL_APPROX_FUNCTIONS
    (2) n is not a positive integer
    """
    # Test that bad function name raises error:
    with pytest.raises(ValueError) as exc_info:
        inputs.approximate_between("bad", y1=1., yn=10., n=2)
    assert str(exc_info.value) == "unsupported approximation function 'bad'"

    # Test that bad n raises error:
    with pytest.raises(ValueError):
        inputs.approximate_between("linear", 1, 2, n=0)
    with pytest.raises(ValueError):
        inputs.approximate_between("linear", 1, 2, n=-1)
    with pytest.raises(ValueError):
        # noinspection PyTypeChecker
        inputs.approximate_between("linear", 1, 2, n=2.3)


@pytest.mark.parametrize('fn, capacity, expected', [
    # Constant 1.0:
    ("always", 0, [1.]),
    ("always", 2, [1., 1., 1.]),
    ("none", 1, [1., 1.]),
    # Linear:
    ("linear", 0, [1.0]),
    ("linear", 4, [1.0, 0.75, 0.5, 0.25, 0.0]),
    ("linear", 2, [1.0, 0.5, 0.0]),
    # Hyperbola:
    ("hyperbola", 0, [1.0]),
    ("hyperbola", 4, [1.0, 0.5, 0.333, 0.25, 0.2]),
    ("hyperbola", 2, [1.0, 0.5, 0.333]),
    # Bingeom:
    ("bingeom", 0, [1.0]),
    ("bingeom", 4, [1.0, 0.5, 0.25, 0.125, 0.0625]),
    ("bingeom", 2, [1.0, 0.5, 0.25]),
])
def test_approximate_enqueue_probs(fn, capacity, expected):
    """Validate approximate_enqueue_probs() function.
    """
    actual = inputs.approximate_enqueue_probs(fn, capacity)
    assert_allclose(actual, expected, rtol=.01, atol=.001)


def test_approximate_enqueue_probs__bad_args():
    """
    Validate approximate_enqueue_probs() raises ValueError if:

    (1) function is not one of ALL_PROBS_FUNCTIONS
    (2) capacity is not a non-negative integer
    """
    # Bad function name raises error:
    with pytest.raises(ValueError) as exc_info:
        inputs.approximate_enqueue_probs("worse", capacity=10)
    assert str(exc_info.value) == \
           "unsupported enqueue probabilities function 'worse'"

    # Capacity must be a non-negative integer
    with pytest.raises(ValueError):
        inputs.approximate_enqueue_probs("always", -1)
    with pytest.raises(ValueError):
        # noinspection PyTypeChecker
        inputs.approximate_enqueue_probs("linear", capacity=4.1)


# VALIDATE build_model_params()
# ----------------------------------------------------------------------------
# noinspection DuplicatedCode
@patch('pqsim.inputs.approximate_between')
@patch('pqsim.inputs.build_mmap')
@patch('pqsim.inputs.build_services')
@patch('pqsim.inputs.build_enqueue_probs')
def test_build_model_params__implicit(
        build_enqueue_probs_mock,
        build_services_mock,
        build_mmap_mock,
        approximate_between_mock
):
    """
    Validate that build_model_params() calls build_mmap(), build_services(),
    build_enqueue_probs() and build_rates() to get rates for arrival and
    service.
    """
    num_classes = 3
    num_servers = 10
    capacity = 5

    arrival_rates = (10.0, 15.0, 20.0)
    service_rates = (42.0, 40.0, 34.0)

    data = inputs.ModelInputData(
        arrival=inputs.ArrivalInputData(
            cv=1, skew=2, lag=0, rate_fn='linear', rate_hp=10., rate_lp=20.),
        service=inputs.DistInputData(
            cv=0.5, skew=4, rate_fn='hyperbola', rate_hp=34., rate_lp=42.),
        queue=inputs.QueueInputData(capacity=capacity, lp_fn='linear'),
        num_classes=num_classes,
        num_servers=num_servers
    )

    approximate_between_mock.side_effect = lambda *args, **kwargs: \
        arrival_rates if kwargs['fn'] == 'linear' else service_rates

    # Define desired values:
    desired_mmap = Mmap.exponential(15, nc=num_classes)
    build_mmap_mock.return_value = desired_mmap

    desired_services = tuple([Ph.exponential(rate) for rate in service_rates])
    build_services_mock.return_value = desired_services

    desired_enqueue_probs = \
        np.random.uniform(0, 1, size=(num_classes, capacity + 1))
    build_enqueue_probs_mock.return_value = desired_enqueue_probs

    # Build:
    model_params = inputs.build_model_params(data)

    # Assert that params have the desired values:
    assert model_params.arrival is desired_mmap
    assert model_params.services == desired_services
    assert_allclose(model_params.enqueue_prob, desired_enqueue_probs)
    assert model_params.capacity == capacity
    assert model_params.num_servers == num_servers

    # Assert mocks were called with proper args:
    build_mmap_mock.assert_called_once_with(
        rates=arrival_rates, cv=data.arrival.cv, skew=data.arrival.skew,
        lag=data.arrival.lag
    )
    build_services_mock.assert_called_once_with(
        rates=service_rates, cv=data.service.cv, skew=data.service.skew
    )
    build_enqueue_probs_mock.assert_called_once_with(
        fn=data.queue.lp_fn, capacity=capacity, num_classes=num_classes
    )
    # Validate that build_rates() was called for both arrival and service:
    approximate_between_mock.assert_has_calls([
        call(fn=data.arrival.rate_fn, y1=data.arrival.rate_hp,
             yn=data.arrival.rate_lp, n=num_classes),
        call(fn=data.service.rate_fn, y1=data.service.rate_hp,
             yn=data.service.rate_lp, n=num_classes),
    ], any_order=True)


# noinspection DuplicatedCode
@patch('pqsim.inputs.approximate_between')
@patch('pqsim.inputs.build_mmap')
@patch('pqsim.inputs.build_services')
@patch('pqsim.inputs.build_enqueue_probs')
def test_build_model_params__explicit(
        build_enqueue_probs_mock,
        build_services_mock,
        build_mmap_mock,
        approximate_between_mock
):
    """
    Validate that build_model_params() calls build_mmap(), build_services()
    and build_enqueue_probs().
    """
    num_classes = 2
    num_servers = 3
    capacity = 4

    arrival_rates = (1., 2.)
    service_rates = (11., 13.)

    data = inputs.ExplicitModelInputData(
        arrival=inputs.ExplicitArrivalInputData(1, 2, arrival_rates, lag=0.1),
        service=inputs.ExplicitDistInputData(.1, .5, rates=service_rates),
        queue=inputs.QueueInputData(capacity=capacity, lp_fn='linear'),
        num_servers=num_servers
    )

    # Define desired values:
    desired_mmap = Mmap.exponential(1.5, nc=num_classes)
    build_mmap_mock.return_value = desired_mmap

    desired_services = tuple([Ph.exponential(rate) for rate in service_rates])
    build_services_mock.return_value = desired_services

    desired_enqueue_probs = \
        np.random.uniform(0, 1, size=(num_classes, capacity + 1))
    build_enqueue_probs_mock.return_value = desired_enqueue_probs

    # Build:
    model_params = inputs.build_model_params(data)

    # Assert that params have the desired values:
    assert model_params.arrival is desired_mmap
    assert model_params.services == desired_services
    assert_allclose(model_params.enqueue_prob, desired_enqueue_probs)
    assert model_params.capacity == capacity
    assert model_params.num_servers == num_servers

    # Assert mocks were called with proper args:
    build_mmap_mock.assert_called_once_with(
        rates=arrival_rates, cv=data.arrival.cv, skew=data.arrival.skew,
        lag=data.arrival.lag
    )
    build_services_mock.assert_called_once_with(
        rates=service_rates, cv=data.service.cv, skew=data.service.skew
    )
    build_enqueue_probs_mock.assert_called_once_with(
        fn=data.queue.lp_fn, capacity=capacity, num_classes=num_classes
    )
    # Make sure that nothing was approximated:
    approximate_between_mock.assert_not_called()


def test_build_model_params__explicit__arrival_and_service_rates_need_match():
    """
    Validate that when build_model_params() is called with explicit input data,
    arrival and service rates arrays should have the same sizes, and
    this size will be used as the num_classes value.
    """
    # 1) Try to use different sizes for rates:
    bad_data = inputs.ExplicitModelInputData(
        arrival=inputs.ExplicitArrivalInputData(1, 2, (3, 4), lag=0),
        service=inputs.ExplicitArrivalInputData(1, 2, (3, 4, 5)),
        queue=inputs.QueueInputData(5),
        num_servers=3)
    with pytest.raises(ValueError) as exc_info:
        inputs.build_model_params(bad_data)
    assert str(exc_info.value) == \
           "sizes of arrival rates and service rates should match"

    # 2) Use good data and check that enqueue probabilities matrix will
    #    have this number of rows:
    capacity = 5
    good_data = inputs.ExplicitModelInputData(
        arrival=inputs.ExplicitArrivalInputData(1, 2, (3, 4), lag=0),
        service=inputs.ExplicitArrivalInputData(1, 2, (3, 4)),
        queue=inputs.QueueInputData(capacity),
        num_servers=3)
    model_params = inputs.build_model_params(good_data)
    probs = np.asarray(model_params.enqueue_prob)
    assert probs.shape[0] == 2


#
# VALIDATE BUILDERS OF MMAP, SERVICE AND QUEUE PROBS
# ----------------------------------------------------------------------------
@pytest.mark.parametrize('cv, skew, lag, rates', [
    (1.0, 2.0, 0.0, [0.3, 0.7]),
    (3.4, 8.0, 1.0, [1.0, 2.1, 3.5, 7.0]),
    (4.2, 23.4, -1.0, [0.5, 2.3]),
    (0.3, -0.5, 0.0, [10.0, 23.0, 4.0]),
])
def test_build_mmap(cv, skew, lag, rates):
    """Validate build_mmap() function with explicitly defined rates.
    """
    mmap = inputs.build_mmap(rates, cv, skew, lag)
    actual_rates = estimate_mmap_rates(mmap, n_iters=100000)
    assert_allclose(actual_rates, rates, rtol=.05)


@patch('pqsim.inputs._validate_rates')
def test_build_mmap__validates_rates(validate_rates_mock):
    """Check that build_mmap() calls _validate_rates().
    """
    rates = (1., 2., 3.)
    inputs.build_mmap(rates, 4, 5, 0)
    validate_rates_mock.assert_called_once_with(rates)


@pytest.mark.parametrize('cv, skew, rates', [
    (1.0, 2.0, [0.3, 0.7]),
    (3.4, 8.0, [1.0, 2.1, 3.5, 7.0]),
    (4.2, 23.4, [0.5, 2.3]),
    (0.3, -0.5, [10.0, 23.0, 4.0]),
])
def test_build_services(cv, skew, rates):
    """Validate build_services() function with explicitly defined rates.
    """
    services = inputs.build_services(rates, cv, skew)
    actual_rates = [estimate_ph_rate(ph, n_iters=100000) for ph in services]
    assert_allclose(actual_rates, rates, rtol=.05)


@patch('pqsim.inputs._validate_rates')
def test_build_services__validates_rates(validate_rates_mock):
    """Check that build_services() calls _validate_rates().
    """
    rates = (10., 20., 30.)
    inputs.build_services(rates, 4, 5)
    validate_rates_mock.assert_called_once_with(rates)


def test_validate_rates():
    """
    Validate that rates array:
    (1) is not empty
    (2) all rates are non-negative
    (3) at least one rate is positive
    """
    # Check that rates can not be empty
    with pytest.raises(ValueError) as exc_info:
        inputs._validate_rates(())
    assert str(exc_info.value) == "rates should be non-empty"

    # Check that negative rates cause error:
    with pytest.raises(ValueError) as exc_info:
        inputs._validate_rates((2, -3, 4))
    assert str(exc_info.value) == "rates must be non-negative"

    # Check that at least ont rate must be positive:
    with pytest.raises(ValueError) as exc_info:
        inputs._validate_rates((0, 0))
    assert str(exc_info.value) == "at least one rate must be positive"


@patch('pqsim.inputs.approximate_enqueue_probs')
@patch('pqsim.inputs._validate_positive_integer')
def test_build_enqueue_probs(validate_integer_mock, approx_mock):
    """
    Validate that when probs are not explicitly defined, build_enqueue_probs()
    calls `approximate_enqueue_probs()` function.

    Also check that all other classes will have all probabilities equal to 1.
    """
    num_classes = 3
    capacity = 5
    lp_fn = "linear"

    lp_probs = np.random.uniform(0, 1, capacity + 1)
    approx_mock.return_value = lp_probs

    expected_enqueue_probs = np.vstack((
        np.ones((num_classes - 1, capacity + 1)),
        lp_probs.reshape((1, capacity + 1))
    ))

    actual_probs = inputs.build_enqueue_probs(lp_fn, capacity, num_classes)
    approx_mock.assert_called_once_with(fn=lp_fn, capacity=capacity)

    # Validate results:
    assert_allclose(actual_probs, expected_enqueue_probs)

    # Make sure that num_classes was checked:
    validate_integer_mock.assert_has_calls([call(num_classes)])


#
# VALIDATE generate_input_data()
# ----------------------------------------------------------------------------
@pytest.fixture
def sample_random_bounds():
    return inputs.RandomInputBounds(
        num_classes_min=10,
        num_classes_max=20,
        num_servers_min=4,
        num_servers_max=8,
        capacity_min=0,
        capacity_max=7,
        arrival_rate_min=34.,
        arrival_rate_max=42.,
        arrival_rate_fns=('hyperbola', 'parabola'),
        arrival_cv_min=.5,
        arrival_cv_max=1.5,
        arrival_skew_max=4,
        arrival_lag_min=-.3,
        arrival_lag_max=.3,
        service_rate_min=42.,
        service_rate_max=55.,
        service_cv_min=.8,
        service_cv_max=1.2,
        service_skew_max=3.,
        service_rate_fns=('logarithm', 'exponent'),
        enqueue_probs_fns=('bingeom', 'hyperbola'),
    )


# noinspection DuplicatedCode
@pytest.mark.parametrize('it', range(10))
def test_generate_input_data__implicit(sample_random_bounds, it):
    """
    Validate that generate_input_data() with explicit_xxx = False doesn't
    set explicit rates or probs.
    """
    p = sample_random_bounds
    d = inputs.generate_random_input(sample_random_bounds)

    assert isinstance(d, inputs.ModelInputData)

    assert p.num_classes_min <= d.num_classes <= p.num_classes_max
    assert p.num_servers_min <= d.num_servers <= p.num_servers_max
    assert p.capacity_min <= d.queue.capacity <= p.capacity_max

    assert p.arrival_rate_min <= d.arrival.rate_hp <= p.arrival_rate_max
    assert p.arrival_rate_min <= d.arrival.rate_lp <= p.arrival_rate_max
    assert d.arrival.rate_fn in p.arrival_rate_fns
    assert p.arrival_cv_min <= d.arrival.cv <= p.arrival_cv_max
    assert d.arrival.skew <= p.arrival_skew_max
    assert p.arrival_lag_min <= d.arrival.lag <= p.arrival_lag_max

    assert p.service_rate_min <= d.service.rate_hp <= p.service_rate_max
    assert p.service_rate_min <= d.service.rate_lp <= p.service_rate_max
    assert d.service.rate_fn in p.service_rate_fns
    assert p.service_cv_min <= d.service.cv <= p.service_cv_max
    assert d.service.skew <= p.service_skew_max

    assert d.queue.lp_fn in p.enqueue_probs_fns


@pytest.mark.parametrize('it', range(10))
def test_generate_input_data__explicit(sample_random_bounds, it):
    """
    Validate that generate_input_data() with explicit_xxx = True set explicit
    rates and probs.
    """
    p = sample_random_bounds
    d = inputs.generate_random_input(sample_random_bounds, explicit=True)

    assert isinstance(d, inputs.ExplicitModelInputData)

    num_classes = len(d.arrival.rates)

    assert p.num_servers_min <= d.num_servers <= p.num_servers_max
    assert p.capacity_min <= d.queue.capacity <= p.capacity_max

    assert isinstance(d.arrival.rates, np.ndarray)
    assert d.arrival.rates.shape == (num_classes,)
    assert (d.arrival.rates >= p.arrival_rate_min).all()
    assert (d.arrival.rates <= p.arrival_rate_max).all()

    assert isinstance(d.service.rates, np.ndarray)
    assert d.service.rates.shape == (num_classes,)
    assert (d.service.rates >= p.service_rate_min).all()
    assert (d.service.rates <= p.service_rate_max).all()

    assert d.queue.lp_fn in p.enqueue_probs_fns


#
# VALIDATE SERIALIZATION
# ----------------------------------------------------------------------------
@pytest.fixture
def sample_model_input_data():
    return inputs.ModelInputData(
        num_classes=3,
        num_servers=10,
        arrival=inputs.ArrivalInputData(.5, -1, "exponent", 3.4, 4.2, -.3),
        service=inputs.DistInputData(2., 4., "hyperbola", 34., 42.),
        queue=inputs.QueueInputData(13, "bingeom"),
    )


@pytest.fixture
def sample_explicit_model_input_data():
    return inputs.ExplicitModelInputData(
        num_servers=10,
        arrival=inputs.ExplicitArrivalInputData(.5, -1, (3., 5.), .1),
        service=inputs.ExplicitDistInputData(2., 4., (34., 42.)),
        queue=inputs.QueueInputData(13, "bingeom"),
    )


def test_deserialize_input_data__implicit(sample_model_input_data):
    """Validate from_json(string) for an implicit model data."""
    string = """{"model": {
        "numClasses": 3,
        "numServers": 10,
        "arrival": {
            "cv": 0.5, "skew": -1.0, "rateFn": "exponent",
            "rateHp": 3.4, "rateLp": 4.2, "lag": -0.3
        },
        "service": {
            "cv": 2.0, "skew": 4.0, "rateFn": "hyperbola",
            "rateHp": 34.0, "rateLp": 42.0
        },
        "queue": {"capacity": 13, "lpFn": "bingeom"}
    }, "maxPackets": 123456}"""

    data = inputs.InputDataSchema().loads(string)
    assert data == inputs.InputData(
        model=sample_model_input_data,
        max_packets=123456
    )


def test_deserialize_input_data__explicit(
        sample_explicit_model_input_data):
    """Validate from_json(string) for an explicit model data."""
    string = """{"model": {
        "numServers": 10,
        "arrival": {"cv": 0.5, "skew": -1.0, "rates": [3.0, 5.0], "lag": 0.1},
        "service": {"cv": 2.0, "skew": 4.0, "rates": [34.0, 42.0]},
        "queue": {"capacity": 13, "lpFn": "bingeom"}
        
    }, "maxPackets": 123456}"""

    data = inputs.InputDataSchema().loads(string)
    assert data == inputs.InputData(
        model=sample_explicit_model_input_data,
        max_packets=123456
    )
