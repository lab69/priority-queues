import os
import json


def pytest_generate_tests(metafunc):
    """
    Use records from all JSON files in directory named as the module
    itself (without .py).
    """
    for fixture in metafunc.fixturenames:
        if fixture == 'json_data':
            # Load associated test data
            tests = load_json_data(metafunc.module)
            metafunc.parametrize(fixture, tests)


def load_json_data(module):
    """
    Fixture responsible for searching a folder with the same name of test
    module and, if available, moving all contents to a temporary directory so
    tests can use them freely.
    """
    filename = module.__file__
    test_dir, _ = os.path.splitext(filename)

    data_filename = os.path.join(test_dir, 'data.json')
    with open(data_filename) as json_file:
        data = json.load(json_file)
        for record in data:
            yield record
