from setuptools import setup, Extension, find_packages, dist
import os

#dist.Distribution().fetch_build_egg("Cython>=0.29.22")

try:
    from Cython.Build import cythonize
except ImportError:
    # Create a closure for deferred import:
    def cythonize(*args, **kwargs):
        from Cython.Build import cythonize
        cythonize(*args, **kwargs)


os.environ['CC'] = 'g++'
os.environ['CXX'] = 'g++'

extensions = [
    Extension(
        "pqsim.cqsim.randoms", [
            "pqsim/cqsim/randoms.pyx",
            "cqsim/Base.cpp",
            "cqsim/Randoms.cpp",
        ],
        include_dirs=['cqsim'],
        language="c++",
        extra_compile_args=["-std=c++14", "-Wno-deprecated", "-O3"],
        extra_link_args=["-std=c++14"]
    ),
    Extension(
        "pqsim.cqsim.model", [
            "pqsim/cqsim/model.pyx",
            "cqsim/Base.cpp",
            "cqsim/Randoms.cpp",
            "cqsim/Statistics.cpp",
            "cqsim/Model.cpp",
            "cqsim/Marshal.cpp",
            "cqsim/Journal.cpp",
        ],
        include_dirs=['cqsim'],
        language="c++",
        extra_compile_args=["-std=c++14", "-Wno-deprecated", "-O3"],
        extra_link_args=["-std=c++14"]
    ),
]

compiler_directives = {
    "language_level": 3,
    "embedsignature": True,
}

setup(
    name='pqsim',
    version='0.2',
    packages=find_packages(exclude=["tests"]),
    author='Andrey Larionov',
    author_email='larioandr@gmail.com',
    license='MIT',
    python_requires=">=3.8",
    include_package_data=True,
    setup_requires=[
        'setuptools>=18.0',
        'Cython>=0.29.22',
    ],
    install_requires=[
        'Click>=7.1.2',
        'numpy>=1.19.2',
        'pyqumo @ git+https://github.com/ipu69/pyqumo.git@main#egg=pyqumo'
    ],
    tests_requires=[
        'pytest',
    ],
    entry_points={
        'console_scripts': [
            'pqs=pqsim.main:cli'
        ]
    },
    ext_modules=cythonize(
        extensions,
        compiler_directives=compiler_directives,
        annotate=True
    ),
)
