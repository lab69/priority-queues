import numpy as np
from analytics.model import MMAPPHSystem
from decouple import config
from analytics.metrics import calc_metrics
import json

json_file = json.loads(config("TASK_PARAMS"))

mmap_system = MMAPPHSystem(
    R=json_file.get("R"),
    N=json_file.get("N"),
    beta_vector_1=np.array(json_file.get("beta_vector_1")),
    beta_vector_2=np.array(json_file.get("beta_vector_2")),
    d0_matrix=np.array(json_file.get("d0_matrix")),
    d1_matrix=np.array(json_file.get("d1_matrix")),
    d2_matrix=np.array(json_file.get("d2_matrix")),
    s1_matrix=np.array(json_file.get("s1_matrix")),
    s2_matrix=np.array(json_file.get("s2_matrix")),
    f_vector=np.array(json_file.get("f_vector")),
    q_vector=np.array(json_file.get("q_vector"))
)

result = mmap_system.solve()
metric = calc_metrics(mmap_system)

res = {
    "packets_count": metric.packets_count,
    "priority_packets_count": metric.priority_packets_count,
    "busy_devices": metric.busy_devices,
    "priority_busy_devices": metric.priority_busy_devices,
    "packets_in_buffer": metric.packets_in_buffer,
    "priority_packets_in_buffer": metric.priority_packets_in_buffer,
    "loss_probability_base_packets": metric.loss_probability_base_packets,
    "loss_probability_priority_packets": metric.loss_probability_priority_packets,
    "sum_loss_probability": metric.sum_loss_probability,
}

print(json.dumps(res, indent=4))

## TODO add write result to file
