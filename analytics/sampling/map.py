from sampling import helper
import numpy as np


def simulate(d0, d1, intensity_vector):
    state_index = 0  # always 0
    current_time = 0
    result = []
    generations_count = 0
    prob_matrix = helper.get_prob_matrix(d0, d1)
    generation_time = 0
    while generations_count < 10000:
        next_time = helper.generate_next_event_time(intensity_vector[state_index])
        state = helper.get_next_state(state_index, prob_matrix, prob_matrix.shape[0])
        current_time += next_time
        state_index = state[1]
        if state[0] == 1:
            result.append(current_time - generation_time)
            generation_time = current_time
            generations_count += 1
            state_index = 0
            print("Generation at time {} and count is {}".format(current_time, generations_count))
    array = np.array(result)
    print("Mean: {}".format(np.mean(array)))
    print("Disp: {}".format(np.std(array) ** 2))


d0 = np.array(
    [[-21, 6, 9],
     [0, -12, 7],
     [8, 0, -12]]
)

d1 = np.array(
    [[2, 1, 3],
     [0, 5, 0],
     [0, 4, 0]]
)

#intensity_vector = np.array([21, 12, 12])
#simulate(d0, d1, intensity_vector)
#print(np.array([i for i in range(5)]))
