import numpy as np


def get_prob_matrix(d0, *args):
    diag = -np.diag(d0)
    new_matrix = np.array(d0)
    np.fill_diagonal(new_matrix, 0)
    for d in args:
        new_matrix = np.hstack((new_matrix, d))
    return np.cumsum(new_matrix / diag.reshape((new_matrix.shape[0], 1)), axis=1)


def generate_next_event_time(intensity):
    return np.random.exponential(1 / intensity)


def get_next_state(current_state, cur_matrix, denominator):
    cur_state_row = cur_matrix[current_state]
    max_arg = np.argwhere(cur_state_row >= np.random.uniform())[0][0]
    return divmod(max_arg, denominator)


def generate_begin_state(initial_states):
    value = np.random.uniform()
    states = np.cumsum(initial_states)
    return np.argwhere(states >= value)[0][0]
