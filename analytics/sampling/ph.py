import numpy as np
from sampling import helper


def start_generation(matrix, intensity_vector, initial_states):
    current_time = 0.
    generations_count = 0
    state = helper.generate_begin_state(initial_states)
    prob_matrix = helper.get_prob_matrix(matrix)
    result = []
    generation_time = 0
    while generations_count < 100:
        next_time = helper.generate_next_event_time(intensity_vector[state])
        current_time += next_time
        state = helper.get_next_state(state, prob_matrix, prob_matrix.shape[1])[1]
        if state == matrix.shape[0]:
            result.append(current_time - generation_time)
            generation_time = current_time
            generations_count += 1
            state = helper.generate_begin_state(initial_states)
    array = np.array(result)
    print("Mean: {}".format(np.mean(array)))
    print("Disp: {}".format(np.std(array)**2))


def execute_ph():
    matrix = np.array(
        [[-1.5, 1.0, 0, 0.5, 0],
         [0, -1.2, 0.3, 0, 0.9],
         [0, 0, -3.2, 0.8, 2.4],
         [1.2, 0, 0, -1.2, 0]]
    )
    intensity_vector = np.array([1.5, 1.2, 3.2, 1.2])
    initial_states = np.array([0.2, 0.2, 0.3, 0.3])
    start_generation(matrix, intensity_vector, initial_states)


def hyper_exponential():
    print("###############  HYPER EXPONENTIAL  #################")
    matrix = np.array(
        [[-1.5, 0, 0, 0, 1.5],
         [0, -1.2, 0, 0, 1.2],
         [0, 0, -3.2, 0, 3.2],
         [0, 0, 0, -1.2, 1.2]]
    )
    intensity_vector = np.array([1.5, 1.2, 3.2, 1.2])
    initial_states = np.array([0.2, 0.2, 0.3, 0.3])
    start_generation(matrix, intensity_vector, initial_states)
    diag = np.abs(np.diag(matrix))
    mean = np.sum(initial_states / diag)
    disp = np.sum(2*initial_states / (diag*diag)) - (mean ** 2)
    print("Theoretical Mean: {}".format(mean))
    print("Theoretical Disp: {}".format(disp))


def erlang():
    print("###############  ERLANG  #############################")
    matrix = np.array(
        [[-1.5, 1.5, 0, 0, 0],
         [0, -1.5, 1.5, 0, 0],
         [0, 0, -1.5, 1.5, 0],
         [0, 0, 0, -1.5, 1.5]]
    )
    intensity_vector = np.array([1.5, 1.5, 1.5, 1.5])
    initial_states = np.array([1, 0, 0, 0])
    start_generation(matrix, intensity_vector, initial_states)
    print("Theoretical Mean: {}".format(float(matrix.shape[0]) / intensity_vector[0]))
    print("Theoretical Disp: {}".format(float(matrix.shape[0]) / intensity_vector[0] ** 2))


#hyper_exponential()
#print("\n")
#erlang()