from setuptools import setup


setup(
    name='analytics',
    version='0.1',
    py_modules=['analytics'],
    install_requires=[
        'numpy>=1.19.2',
        'python-decouple==3.1',
        'pyqumo>=1.0'
    ],
    tests_requires=[
        'pytest',
        'pathlib'
    ]
)
