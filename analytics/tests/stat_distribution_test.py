import numpy as np
from analytics.model import MMAPPHSystem
from analytics import stat_distribution
from analytics.helpers import math_helper


def test_calc_stat_distribution_normalization():
    mmap_system = MMAPPHSystem(
        R=3,
        N=2,
        beta_vector_1=np.array([0.5, 0.5]),
        beta_vector_2=np.array([0.5, 0.5]),
        d0_matrix=np.array([
            [1, 2],
            [3, 4]
        ]),
        d1_matrix=np.array([
            [5, 6],
            [7, 8]
        ]),
        d2_matrix=np.array([
            [9, 0],
            [1, 2]
        ]),
        s1_matrix=np.array([
            [1, 2],
            [5, 6]
        ]),
        s2_matrix=np.array([
            [1, 2],
            [5, 6]
        ]),
        f_vector=np.array([0.5, 0.2, 0.3]),
        q_vector=np.array([0.5, 0.1, 0.4])
    )

    mmap_system.solve()

    result = 0

    for vector in mmap_system.stat_distribution:
        result += vector.sum()

    np.testing.assert_allclose(1, result)


def test_calc_g_matrices():
    above_diag_blocks = [
        np.eye(3),
        np.array([
            [1, 1, 3],
            [4, 7, 6],
            [7, 8, 9]
        ])
    ]
    diag_blocks = [
        np.eye(3),
        np.array([
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9]
        ]),
        np.array([
            [0, 1, 2],
            [4, 4, 5],
            [6, 7, 8]
        ])
    ]
    sub_diag_blocks = [
        np.array([
            [9, 0, 1],
            [2, 3, 4],
            [5, 6, 7]
        ]),
        np.array([
            [8, 9, 0],
            [1, 2, 3],
            [4, 5, 6]
        ])
    ]

    g_matrices = stat_distribution.calc_g_matrices(
        above_diag_blocks,
        diag_blocks,
        sub_diag_blocks
    )

    assert len(g_matrices) == len(diag_blocks) - 1

    # g1_matrix
    g1_matrix = -np.array([
        [-5, -5, 0],
        [2/3, -1/3, 2],
        [11/3, 14/3, -1]
    ])

    np.testing.assert_allclose(
        g_matrices[-1],
        g1_matrix,
        atol=1.e-15
    )

    # g0_matrix
    g0_matrix = np.dot(np.linalg.inv(
        np.array([
            [17/3, 20/3, -4],
            [8/3, 2/3, 2],
            [-11/3, -11/3, -2]
        ])
    ), np.array([
            [9, 0, 1],
            [2, 3, 4],
            [5, 6, 7]
        ])
    )

    np.testing.assert_allclose(
        g_matrices[0],
        g0_matrix,
        atol=1.e-15
    )


def test_calc_q_highlighted_matrices():
    diag_blocks = [
        np.array([[1, 2],
                  [3, 4]]),
        np.array([[5, 6],
                  [7, 8]]),
        np.array([[9, 0],
                  [1, 2]])
    ]

    above_diag_blocks = [
        np.array([[5, 6],
                  [7, 8]]),
        np.array([[9, 0],
                  [1, 2]])
    ]

    g_matrices = [
        np.array([[1, 2],
                  [3, 4]]),
        np.array([[5, 6],
                  [7, 8]]),
    ]

    q_matrices = stat_distribution.calc_q_highlighted_matrices(
        above_diag_blocks,
        diag_blocks,
        g_matrices
    )

    assert len(q_matrices) == len(diag_blocks)

    # last element of highlighted matrices equals
    # last diag block
    np.testing.assert_allclose(
        q_matrices[-1],
        diag_blocks[-1]
    )

    # Q[0]
    np.testing.assert_allclose(
        q_matrices[0],
        diag_blocks[0] + np.dot(above_diag_blocks[0], g_matrices[0])
    )

    # Q[1]
    np.testing.assert_allclose(
        q_matrices[1],
        diag_blocks[1] + np.dot(above_diag_blocks[1], g_matrices[1])
    )


def test_calc_phi_matrices():
    q_matrices = [
        np.array([[1, 2],
                  [3, 4]]),
        np.array([[5, 6],
                  [7, 8]]),
        np.array([[9, 0],
                  [1, 2]])
    ]
    above_diag_blocks = [
        np.array([[5, 6],
                  [7, 8]]),
        np.array([[9, 0],
                  [1, 2]])
    ]

    phi_matrices = stat_distribution.calc_phi_matrices(q_matrices, above_diag_blocks)

    assert len(phi_matrices) == len(q_matrices) - 1

    # assertion according to formula fo
    # calculation of first phi matrix
    np.testing.assert_allclose(
        phi_matrices[0],
        np.dot(above_diag_blocks[0], -np.linalg.inv(q_matrices[1]))
    )

    # assertion according to formula
    # for calculation i element of phi matrices
    np.testing.assert_allclose(
        phi_matrices[1],
        math_helper.dot(
            phi_matrices[0],
            above_diag_blocks[1],
            -np.linalg.inv(q_matrices[2])
        )
    )
