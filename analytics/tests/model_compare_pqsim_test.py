import pytest
from analytics.model import MMAPPHSystem
from analytics.metrics import calc_metrics
import numpy as np

from pqsim.model import Params, simulate as py_sim
from pqsim.cqsim.model import simulate as c_sim
from pqsim.stochastic import Mmap, Ph
from pyqumo.matrix import cbdiag

RTOL = 1e-1
ATOL = 1e-2


def execute_compare_analytics_and_simulation(mmap_system: MMAPPHSystem, enqueue_prob, sim, comment):
    mmap_system.solve()
    metrics = calc_metrics(mmap_system)

    mmap = Mmap(mmap_system.d0_matrix, mmap_system.d1_matrix, mmap_system.d2_matrix)
    services = (
        Ph(mmap_system.s1_matrix, mmap_system.beta_vector_1), Ph(mmap_system.s2_matrix, mmap_system.beta_vector_2))
    params = Params(
        arrival=mmap,
        services=services,
        enqueue_prob=enqueue_prob,
        capacity=mmap_system.R,
        num_servers=mmap_system.N
    )

    # Run simulation:
    results = sim(params, 1000000)

    print(metrics)

    np.testing.assert_allclose(results.system_size.mean, metrics.packets_count, rtol=RTOL, err_msg=comment)

    np.testing.assert_allclose(results.loss_prob, metrics.sum_loss_probability, rtol=RTOL, err_msg=comment)
    np.testing.assert_allclose(results.loss_prob_of[2], metrics.loss_probability_base_packets, rtol=RTOL,
                               err_msg=comment)
    np.testing.assert_allclose(results.loss_prob_of[1], metrics.loss_probability_priority_packets, rtol=RTOL,
                               err_msg=comment)

    np.testing.assert_allclose(results.busy_servers.mean, metrics.busy_devices, rtol=RTOL, err_msg=comment)

    np.testing.assert_allclose(results.queue_size.mean, metrics.packets_in_buffer, rtol=RTOL,
                               err_msg=comment)
    np.testing.assert_allclose(results.queue_size_of[1].mean, metrics.priority_packets_in_buffer, atol=ATOL,
                               err_msg=comment, rtol=RTOL)
    np.testing.assert_allclose(results.queue_size_of[2].mean,
                               metrics.packets_in_buffer - metrics.priority_packets_in_buffer, atol=ATOL,
                               err_msg=comment, rtol=RTOL)


# d2 is zero matrix
# Test for base packets hyperexponential output
# Priority packets are absent


@pytest.mark.parametrize('d0,d2,R,enqueue_prob', [
    (
            [[-15, 1, 7],
             [0, -14, 5],
             [0, 0, -1]],
            [[1, 2, 4],
             [1, 3, 5],
             [1, 0, 0]],
            3,
            [0.5, 0.5]
    )
])
def test_hyperexponential_for_base_packets(d0, d2, R, enqueue_prob):
    beta = [1 / R for _ in range(R)]
    q_vector = np.array([enqueue_prob[0] for i in range(R)])
    f_vector = np.array([enqueue_prob[1] for i in range(R)])

    mmap_system = MMAPPHSystem(
        R=R,
        N=1,
        beta_vector_1=np.array(beta),
        beta_vector_2=np.array(beta),
        d0_matrix=np.array(d0),
        d2_matrix=np.array(d2),
        d1_matrix=np.zeros(shape=(len(d0[0]), len(d0[0]))),
        s1_matrix=-np.eye(R),
        s2_matrix=-np.eye(R),
        q_vector=q_vector,
        f_vector=f_vector
    )

    for sim, comment in [(c_sim, "C++ simulator"), (py_sim, "python simulator")]:
        execute_compare_analytics_and_simulation(
                mmap_system, 
                [q_vector, f_vector], 
                sim, 
                comment)


@pytest.mark.parametrize('d0,d2,R, enqueue_prob', [
    (
            [[-15, 1, 7],
             [0, -14, 5],
             [0, 0, -1]],
            [[1, 2, 4],
             [1, 3, 5],
             [1, 0, 0]],
            3,
            [1, 1]
    )
])
def test_erlang_for_base_packets(d0, d2, R, enqueue_prob):
    beta = [1 / R for _ in range(R)]
    f_vector = np.array([enqueue_prob[1] for i in range(R)])
    q_vector = np.array([enqueue_prob[0] for i in range(R)])
    blocks = [
        (0, np.asarray([[-1]])),
        (1, np.asarray([[1]]))
    ]

    mmap_system = MMAPPHSystem(
        R=R,
        N=1,
        beta_vector_1=np.array(beta),
        beta_vector_2=np.array(beta),
        d0_matrix=np.array(d0),
        d2_matrix=np.array(d2),
        d1_matrix=np.zeros(shape=(len(d0[0]), len(d0[0]))),
        s1_matrix=cbdiag(R, blocks),
        s2_matrix=cbdiag(R, blocks),
        q_vector=q_vector,
        f_vector=f_vector
    )

    for sim, comment in [(c_sim, "C++ simulator"), (py_sim, "python simulator")]:
        execute_compare_analytics_and_simulation(
                mmap_system, 
                [q_vector, f_vector], 
                sim, 
                comment)


@pytest.mark.parametrize('d0,d1,d2,R,N,enqueue_prob', [
    (
            [[-22, 1, 7],
             [0, -23, 5],
             [0, 0, -2]],
            [[1, 2, 4],
             [1, 3, 5],
             [1, 0, 0]],
            [[1, 2, 4],
             [1, 3, 5],
             [1, 0, 0]],
            5,
            2,
            [1, 1]
    ),
    (
            [[-22, 1, 7],
             [0, -23, 5],
             [0, 0, -2]],
            [[1, 2, 4],
             [1, 3, 5],
             [1, 0, 0]],
            [[1, 2, 4],
             [1, 3, 5],
             [1, 0, 0]],
            5,
            2,
            [0.2, 0.5]
    )
])
def test_hyperexponential(d0, d1, d2, R, N, enqueue_prob):
    beta = [1 / R for _ in range(R)]
    q_vector = np.array([enqueue_prob[0] for i in range(R)])
    f_vector = np.array([enqueue_prob[1] for i in range(R)])
    mmap_system = MMAPPHSystem(
        R=R,
        N=N,
        beta_vector_1=np.array(beta),
        beta_vector_2=np.array(beta),
        d0_matrix=np.array(d0),
        d2_matrix=np.array(d2),
        d1_matrix=np.array(d1),
        s1_matrix=-np.eye(R),
        s2_matrix=-np.eye(R),
        q_vector=q_vector,
        f_vector=f_vector
    )

    for sim, comment in [(c_sim, "C++ simulator"), (py_sim, "python simulator")]:
        execute_compare_analytics_and_simulation(
                mmap_system, 
                [q_vector, f_vector], 
                sim, 
                comment)


@pytest.mark.parametrize('d0,d1,d2,R,N,enqueue_prob', [
    (
            [[-22, 1, 7],
             [0, -23, 5],
             [0, 0, -2]],
            [[1, 2, 4],
             [1, 3, 5],
             [1, 0, 0]],
            [[1, 2, 4],
             [1, 3, 5],
             [1, 0, 0]],
            5,
            2,
            [1, 1]
    ),
    (
            [[-22, 1, 7],
             [0, -23, 5],
             [0, 0, -2]],
            [[1, 2, 4],
             [1, 3, 5],
             [1, 0, 0]],
            [[1, 2, 4],
             [1, 3, 5],
             [1, 0, 0]],
            5,
            2,
            [0.7, 0.25]
    )
])
def test_hypererlang(d0, d1, d2, R, N, enqueue_prob):
    beta = [1 / R for _ in range(R)]
    q_vector = np.array([enqueue_prob[0] for i in range(R)])
    f_vector = np.array([enqueue_prob[1] for i in range(R)])
    blocks = [
        (0, np.asarray([[-1]])),
        (1, np.asarray([[1]]))
    ]

    mmap_system = MMAPPHSystem(
        R=R,
        N=N,
        beta_vector_1=np.array(beta),
        beta_vector_2=np.array(beta),
        d0_matrix=np.array(d0),
        d2_matrix=np.array(d2),
        d1_matrix=np.array(d1),
        s1_matrix=cbdiag(R, blocks),
        s2_matrix=cbdiag(R, blocks),
        q_vector=q_vector,
        f_vector=f_vector
    )

    for sim, comment in [(c_sim, "C++ simulator"), (py_sim, "python simulator")]:
        execute_compare_analytics_and_simulation(
                mmap_system, 
                [q_vector, f_vector], 
                sim, 
                comment)


