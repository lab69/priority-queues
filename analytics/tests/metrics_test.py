import numpy as np
from analytics.model import MMAPPHSystem
from analytics.metrics import calc_metrics


system = {
    "R": 3,
    "N": 1,
    "beta_vector_1": [0.34, 0.33, 0.33],
    "beta_vector_2": [0.34, 0.33, 0.33],
    "d0_matrix": [[-1, 0, 0],
                  [0, -8, 0],
                  [0, 0, -2]],
    "d1_matrix": [[1, 0, 0],
                  [0, 3, 5],
                  [1, 1, 0]],
    "d2_matrix": [[0, 0, 0],
                  [0, 0, 0],
                  [0, 0, 0]],
    "s1_matrix": [[-1, 1, 0],
                  [0, -1, 1],
                  [0, 0, -1]],
    "s2_matrix": [[-0.0001, 0, 0],
                  [0, -0.0001, 0],
                  [0, 0, -0.00001]],
    "f_vector": [0, 0, 0],
    "q_vector": [1, 1, 1]
}


def test_base_equality():
    mmap_system = MMAPPHSystem(
        R=system.get("R"),
        N=system.get("N"),
        beta_vector_1=np.array(system.get("beta_vector_1")),
        beta_vector_2=np.array(system.get("beta_vector_2")),
        d0_matrix=np.array(system.get("d0_matrix")),
        d1_matrix=np.array(system.get("d1_matrix")),
        d2_matrix=np.array(system.get("d2_matrix")),
        s1_matrix=np.array(system.get("s1_matrix")),
        s2_matrix=np.array(system.get("s2_matrix")),
        f_vector=np.array(system.get("f_vector")),
        q_vector=np.array(system.get("q_vector"))
    )

    mmap_system.solve()
    metric = calc_metrics(mmap_system)

    np.testing.assert_allclose(metric.priority_packets_count, metric.priority_busy_devices + metric.priority_packets_in_buffer)
    np.testing.assert_allclose(metric.packets_count, metric.busy_devices + metric.packets_in_buffer)
