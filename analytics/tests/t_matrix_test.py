import numpy as np
from analytics.model import MMAPPHSystem
from analytics.matrices import t_matrix
from analytics.helpers import matrix_size_helper

mmap_system = MMAPPHSystem(
    R=3,
    N=3,
    beta_vector_1=np.array([0.5, 0.2, 0.3]),
    beta_vector_2=np.array([0.5, 0.2, 0.3]),
    d0_matrix=np.array([
        [-27, 6, 9],
        [0, -39, 7],
        [8, 0, -36]
    ]),
    d1_matrix=np.array([
        [2, 1, 3],
        [0, 25, 0],
        [8, 4, 0]
    ]),
    d2_matrix=np.array([
        [2, 1, 3],
        [0, 5, 0],
        [8, 24, 0]
    ]),
    s1_matrix=np.array([
        [1, 2, 3],
        [5, 6, 8],
        [9, 0, 2]
    ]),
    s2_matrix=np.array([
        [1, 2, 3],
        [5, 6, 7],
        [9, 0, 1]
    ]),
    f_vector=np.array([0.5, 0.2, 0.3]),
    q_vector=np.array([0.5, 0.3, 0.2])
)
mmap_system.solve()


def test_calculate_t_n_n_blocks():
    blocks = t_matrix.calc_t_n_n_blocks(mmap_system)

    for index, block in enumerate(blocks):
        assert block.shape == matrix_size_helper.t_i_diagonal_block_shape(
            mmap_system.M1,
            mmap_system.M2,
            index,
            mmap_system.tilda_W
        )


def test_calculate_t_sub_blocks():
    result = t_matrix.calc_t_sub_blocks(mmap_system)


def test_calculate_t_above_blocks():
    result = t_matrix.calc_t_above_blocks(mmap_system)


def test_calculate_t_N_N_block():
    block = t_matrix.calc_t_N_N_block(mmap_system)

    assert block.shape == matrix_size_helper.t_i_diagonal_block_shape(
        mmap_system.M1,
        mmap_system.M2,
        mmap_system.N,
        mmap_system.tilda_W
    )


def test_calculate_t_matrix():
    result = t_matrix.calc_t_matrix(mmap_system)
    assert result.shape == matrix_size_helper.t_matrix_shape(
        mmap_system.N,
        mmap_system.M1,
        mmap_system.M2,
        mmap_system.tilda_W
    )
