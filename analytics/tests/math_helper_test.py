import numpy as np
from analytics.helpers import math_helper


def test_kron_for_two_args():
    result = math_helper.kron(
        np.array([
            [1, 2],
            [3, 4]
        ]),
        np.array([
            [0, 5],
            [6, 7]
        ])
    )

    np.testing.assert_allclose(
        result,
        np.array([
            [0, 5, 0, 10],
            [6, 7, 12, 14],
            [0, 15, 0, 20],
            [18, 21, 24, 28]
        ])
    )


def test_kron_for_three_args_1():
    first = np.array([1, 2])
    second = np.array([1, 1])
    third = np.array([[1, 1],
                      [1, 1]])

    result = math_helper.kron(first, second, third)
    np.testing.assert_allclose(
        result,
        np.array([[1, 1, 1, 1, 2, 2, 2, 2],
                  [1, 1, 1, 1, 2, 2, 2, 2]])
    )


def test_kron_for_three_args_2():
    first = np.array([[1, 2],
                      [3, 4]])
    second = np.array([[0, 1],
                       [2, 3]])
    third = np.array([[0, 1],
                      [2, 3]])

    result1 = np.array([[0, 1, 0, 2],
                        [2, 3, 4, 6],
                        [0, 3, 0, 4],
                        [6, 9, 8, 12]])

    np.testing.assert_allclose(
        result1,
        np.array([[0, 1, 0, 2],
                  [2, 3, 4, 6],
                  [0, 3, 0, 4],
                  [6, 9, 8, 12]])
    )

    np.testing.assert_allclose(
        math_helper.kron(first, second),
        np.array([[0, 1, 0, 2],
                  [2, 3, 4, 6],
                  [0, 3, 0, 4],
                  [6, 9, 8, 12]])
    )

    result = math_helper.kron(first, second, third)

    np.testing.assert_allclose(
        math_helper.kron(result1, third),
        math_helper.kron(first, second, third)
    )

    np.testing.assert_allclose(
        result,
        np.array([
            [0, 0, 0, 1, 0, 0, 0, 2],
            [0, 0, 2, 3, 0, 0, 4, 6],
            [0, 2, 0, 3, 0, 4, 0, 6],
            [4, 6, 6, 9, 8, 12, 12, 18],
            [0, 0, 0, 3, 0, 0, 0, 4],
            [0, 0, 6, 9, 0, 0, 8, 12],
            [0, 6, 0, 9, 0, 8, 0, 12],
            [12, 18, 18, 27, 16, 24, 24, 36]
        ])
    )


def test_dot_for_three_matrices():
    first = np.array([[1, 2, 3],
                      [4, 5, 6],
                      [7, 8, 9]])

    second = np.array([[0, 1, 2],
                       [3, 4, 5],
                       [6, 7, 8]])

    third = np.array([[9, 0, 1],
                      [2, 3, 4],
                      [5, 6, 7]])

    result1 = math_helper.dot(first, second)

    np.testing.assert_allclose(
        result1,
        np.array([[24, 30, 36],
                  [51, 66, 81],
                  [78, 102, 126]])
    )

    result = math_helper.dot(first, second, third)

    np.testing.assert_allclose(
        result,
        np.array([[456, 306, 396],
                  [996, 684, 882],
                  [1536, 1062, 1368]])
    )