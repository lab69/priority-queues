import numpy as np
from analytics.helpers import math_helper


def test_return_type():
    result = math_helper.calc_quantity(2, 2, 2, 2, 4)
    assert isinstance(result, np.ndarray)


def test_simple_test1():
    result = math_helper.calc_quantity(2, 2, 2, 2, 4)
    assert len(result) == 5


def test_zeros():
    assert math_helper.calc_combinations(0, 0) == 1


def test_ones():
    assert math_helper.calc_combinations(1, 1) == 1


def test_fifth_two():
    assert math_helper.calc_combinations(5, 3) == 10
