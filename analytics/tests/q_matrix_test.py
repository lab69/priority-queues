import numpy as np
from analytics.model import MMAPPHSystem
from analytics.matrices import q_matrix
from analytics.helpers import math_helper, matrix_size_helper

mmap_system = MMAPPHSystem(
    R=3,
    N=3,
    beta_vector_1=np.array([0.5, 0.2, 0.3]),
    beta_vector_2=np.array([0.5, 0.2, 0.3]),
    d0_matrix=np.array([
        [-27, 6, 9],
        [0, -39, 7],
        [8, 0, -36]
    ]),
    d1_matrix=np.array([
        [2, 1, 3],
        [0, 25, 0],
        [8, 4, 0]
    ]),
    d2_matrix=np.array([
        [2, 1, 3],
        [0, 5, 0],
        [8, 24, 0]
    ]),

    s1_matrix=np.array([
        [1, 2, 3],
        [5, 6, 7],
        [9, 0, 1]
    ]),
    s2_matrix=np.array([
        [1, 2, 3],
        [5, 6, 7],
        [9, 0, 1]
    ]),
    f_vector=np.array([0.5, 0.2, 0.3]),
    q_vector=np.array([0.5, 0.3, 0.2])
)
mmap_system.solve()


def test_q_0_1_block():
    result = q_matrix.calc_q_0_1(mmap_system)
    shape = matrix_size_helper.q_0_1_block_shape(
        mmap_system.M1,
        mmap_system.M2,
        mmap_system.N,
        mmap_system.tilda_W
    )

    assert result.shape == shape


def test_calculate_q_1_0():
    result = q_matrix.calc_q_1_0(mmap_system)
    shape = matrix_size_helper.q_1_0_block_shape(
        mmap_system.M1,
        mmap_system.M2,
        mmap_system.N,
        mmap_system.tilda_W
    )

    assert result.shape == shape


def test_calculate_q_above_blocks():
    blocks = q_matrix.calc_q_above_blocks(mmap_system)

    for index, block in enumerate(blocks):
        assert block.shape == matrix_size_helper.q_i_above_block_shape(
            index + 1,
            mmap_system.M1,
            mmap_system.M2,
            mmap_system.N,
            mmap_system.tilda_W
        )


def test_calculate_q_diagonal_blocks():
    blocks = q_matrix.calc_q_diagonal_blocks(mmap_system)

    for index, block in enumerate(blocks):
        assert block.shape == matrix_size_helper.q_i_diagonal_block_shape(
            index + 1,
            mmap_system.M1,
            mmap_system.M2,
            mmap_system.N,
            mmap_system.tilda_W
        )


def test_calculate_q_r_r_block():
    block = q_matrix.calc_q_r_r_block(mmap_system)

    assert block.shape == matrix_size_helper.q_i_diagonal_block_shape(
        mmap_system.R,
        mmap_system.M1,
        mmap_system.M2,
        mmap_system.N,
        mmap_system.tilda_W
    )


def test_calculate_q_sub_blocks():
    blocks = q_matrix.calc_q_sub_blocks(mmap_system)

    for index, block in enumerate(blocks):
        assert block.shape == matrix_size_helper.q_i_sub_block_shape(
            index + 2,
            mmap_system.M1,
            mmap_system.M2,
            mmap_system.N,
            mmap_system.tilda_W
        )
