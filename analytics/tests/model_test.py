import numpy as np
import time
from analytics.model import MMAPPHSystem
from analytics.helpers import matrix_size_helper

start_time = time.time()
mmap_system = MMAPPHSystem(
    R=3,
    N=5,
    beta_vector_1=np.array([0.5, 0.2, 0.3]),
    beta_vector_2=np.array([0.5, 0.2, 0.3]),
    d0_matrix=np.array([
        [-27, 6, 9],
        [0, -39, 7],
        [8, 0, -36]
    ]),
    d1_matrix=np.array([
        [2, 1, 3],
        [0, 25, 0],
        [8, 4, 0]
    ]),
    d2_matrix=np.array([
        [2, 1, 3],
        [0, 5, 0],
        [8, 24, 0]
    ]),
    s1_matrix=np.array([
        [1, 2, 3],
        [5, 6, 8],
        [9, 0, 2]
    ]),
    s2_matrix=np.array([
        [1, 2, 3],
        [5, 6, 7],
        [9, 0, 1]
    ]),
    f_vector=np.array([0.5, 0.2, 0.3]),
    q_vector=np.array([0.5, 0.3, 0.2])
)


def test_calculate_t_matrix():
    metrics = mmap_system.solve()

    shape = matrix_size_helper.q_full_matrix_shape(
        mmap_system.N,
        mmap_system.M1,
        mmap_system.M2,
        mmap_system.W,
        mmap_system.R
    )

    assert mmap_system.get_q_matrix().shape == shape
