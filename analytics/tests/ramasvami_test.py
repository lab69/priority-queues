import numpy as np
from analytics import ramasvami

generator = np.array([
    [1, 2, 3, 4],
    [5, 6, 7, 8],
    [9, 0, 1, 2],
    [3, 4, 5, 6]
])


def test_block_concatenation():
    diag = [
        np.array([[5]]),
        5 * np.eye(2)
    ]
    sub_diag = [
        np.array([[9], [1]]),
        np.array([[9, 0], [0, 9], [0, 2]])
    ]
    assert (ramasvami.concat_l_blocks(diag, sub_diag) == np.array([
        [5., 0., 0.],
        [9., 5., 0.],
        [1., 0., 5.],
        [0., 9., 0.],
        [0., 0., 9.],
        [0., 0., 2.]
    ])).all()


def test_block_concatenation_second():
    diag = [
        np.array([[5]]),
        5 * np.eye(2),
        2 * np.eye(3)
    ]
    sub_diag = [
        np.array([[9], [1]]),
        np.array([[9, 0], [0, 9], [0, 2]]),
        np.array([[3, 0, 0], [0, 2, 9], [0, 0, 9], [0, 0, 3]]),
    ]
    assert (ramasvami.concat_l_blocks(diag, sub_diag) == np.array(
        [[5., 0., 0., 0., 0., 0.],
         [9., 5., 0., 0., 0., 0.],
         [1., 0., 5., 0., 0., 0.],
         [0., 9., 0., 2., 0., 0.],
         [0., 0., 9., 0., 2., 0.],
         [0., 0., 2., 0., 0., 2.],
         [0., 0., 0., 3., 0., 0.],
         [0., 0., 0., 0., 2., 9.],
         [0., 0., 0., 0., 0., 9.],
         [0., 0., 0., 0., 0., 3.]]
    )).all()


def test_calculate_l_i_matrices():
    result = ramasvami.calc_l_matrices(np.array([
        [6, 7, 8],
        [10, 11, 12],
        [13, 14, 15]
    ]), 3)
    np.testing.assert_allclose(result[0], np.array([
        [30, 0, 0],
        [13, 20, 0],
        [0, 26, 10],
        [0, 0, 39]]
    ))
    np.testing.assert_allclose(result[1], np.array([[20, 0], [13, 10], [0, 26]]))
    np.testing.assert_allclose(result[2], np.array([[10], [13]]))


def test_calculate_l_i_j_matrices():
    result = ramasvami.calc_l_i_matrices(generator, 3)

    # assert that j = M - 2 as in calculus
    assert len(result) == generator.shape[0] - 2

    # verify all L_{j}_{i}
    # L_{1}_{0} (as equal j = 0, i = 0)
    np.testing.assert_allclose(result[0][0], np.array(
        [[0, 0, 0],
         [4, 0, 0],
         [0, 8, 0],
         [0, 0, 12]]
    ))

    # L_{1}_{1} (as equal j = 0, i = 1)
    np.testing.assert_allclose(result[0][1], np.array(
        [[0, 0],
         [4, 0],
         [0, 8]]
    ))

    # L_{1}_{2} (as equal j = 0, i = 2)
    np.testing.assert_allclose(result[0][2], np.array(
        [[0],
         [4]]
    ))

    # L_{2}_{0} (as equal j = 1, i = 0)
    np.testing.assert_allclose(result[1][0], np.array(
        [[15, 0, 0, 0, 0, 0],
         [9, 10, 0, 0, 0, 0],
         [3, 0, 10, 0, 0, 0],
         [0, 18, 0, 5, 0, 0],
         [0, 3, 9, 0, 5, 0],
         [0, 0, 6, 0, 0, 5],
         [0, 0, 0, 27, 0, 0],
         [0, 0, 0, 3, 18, 0],
         [0, 0, 0, 0, 6, 9],
         [0, 0, 0, 0, 0, 9]]
    ))

    # L_{2}_{0} (as equal j = 1, i = 1)
    np.testing.assert_allclose(result[1][1], np.array(
        [[10, 0, 0],
         [9, 5, 0],
         [3, 0, 5],
         [0, 18, 0],
         [0, 3, 9],
         [0, 0, 6]]
    ))

    # L_{2}_{0} (as equal j = 1, i = 2)
    np.testing.assert_allclose(result[1][2], np.array(
        [[5],
         [9],
         [3]]
    ))

    # for j in range(0, generator_matrix.shape[0] - 2):
    #     for i in range(0, 3):
    #         print("i={}, j={}".format(i, j))
    #         print(result[j][i])


def test_calculate_u_i_j_matrices():
    result = ramasvami.calc_u_i_j_matrices(generator, 3)

    # assert that j = M - 2 as in calculus
    assert len(result) == generator.shape[0] - 2

    # verify all L_{j}_{i}
    # U_{1}_{1} (as equal j = 0, i = 0)
    np.testing.assert_allclose(result[0][0], np.array(
        [[7, 8, 0, 0],
         [0, 7, 8, 0],
         [0, 0, 7, 8]]
    ))

    # U_{1}_{2} (as equal j = 0, i = 1)
    np.testing.assert_allclose(result[0][1], np.array(
        [[14, 16, 0],
         [0, 14, 16]]
    ))

    # U_{1}_{3} (as equal j = 0, i = 2)
    np.testing.assert_allclose(result[0][2], np.array(
        [[21, 24]]
    ))

    # U_{2}_{1} (as equal j = 1, i = 0)
    np.testing.assert_allclose(result[1][0], np.array(
        [[2, 3, 4, 0, 0, 0, 0, 0, 0, 0],
         [0, 2, 0, 3, 4, 0, 0, 0, 0, 0],
         [0, 0, 2, 0, 3, 4, 0, 0, 0, 0],
         [0, 0, 0, 2, 0, 0, 3, 4, 0, 0],
         [0, 0, 0, 0, 2, 0, 0, 3, 4, 0],
         [0, 0, 0, 0, 0, 2, 0, 0, 3, 4]]
    ))

    # U_{2}_{2} (as equal j = 1, i = 1)
    np.testing.assert_allclose(result[1][1], np.array(
        [[4, 6, 8, 0, 0, 0],
         [0, 4, 0, 6, 8, 0],
         [0, 0, 4, 0, 6, 8]]
    ))

    # U_{2}_{3} (as equal j = 1, i = 2)
    np.testing.assert_allclose(result[1][2], np.array(
        [[6, 9, 12]]
    ))

    # for j in range(0, generator.shape[0] - 2):
    #     for i in range(0, 3):
    #         print("i={}, j={}".format(i, j))
    #         print(result[j][i])


def test_calculate_a_i_matrices():
    result_a_matrices = ramasvami.calc_a_matrices(generator[1:, 1:], 3)
    result_l_matrices = ramasvami.calc_l_i_matrices(generator[1:, 1:], 3)
    result_u_matrices = ramasvami.calc_u_i_j_matrices(generator[1:, 1:], 3)

    np.testing.assert_allclose(result_a_matrices[1], np.array([
        [0, 7, 8],
        [0, 0, 2],
        [4, 5, 0]
    ]))

    np.testing.assert_allclose(result_a_matrices[2], np.array([
        [0, 2. * 7., 2. * 8., 0, 0, 0],
        [0, 0, 2, 7., 8., 0],
        [4, 5, 0, 0, 7., 8.],
        [0, 0, 0, 0, 4, 0],
        [0, 4, 0, 5, 0, 2],
        [0, 0, 8, 0, 10, 0]
    ]))

    np.testing.assert_allclose(result_a_matrices[3], np.array([
        [0, 21, 24, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 2, 14, 16, 0, 0, 0, 0, 0],
        [4, 5, 0, 0, 14, 16, 0, 0, 0, 0],
        [0, 0, 0, 0, 4, 0, 7, 8, 0, 0],
        [0, 4, 0, 5, 0, 2, 0, 7, 8, 0],
        [0, 0, 8, 0, 10, 0, 0, 0, 7, 8],
        [0, 0, 0, 0, 0, 0, 0, 6, 0, 0],
        [0, 0, 0, 4, 0, 0, 5, 0, 4, 0],
        [0, 0, 0, 0, 8, 0, 0, 10, 0, 2],
        [0, 0, 0, 0, 0, 12, 0, 0, 15, 0]
    ]))

    # print("####################### A MATRICES ##########################################")
    # print(result_a_matrices)
    #
    # print("####################### L MATRICES ##########################################")
    # print(result_l_matrices)
    #
    # print("####################### U MATRICES ##########################################")
    # print(result_u_matrices)


def test_p_matrices():
    result = ramasvami.calc_p_matrices(np.array([0.5, 0.5]), 4)

    np.testing.assert_allclose(result[0], np.array([[0.5, 0.5]]))

    np.testing.assert_allclose(result[1], np.array([
        [.5, .5, 0],
        [0, .5, .5]
    ]))

    np.testing.assert_allclose(result[2], np.array([
        [.5, .5, 0, 0],
        [0, .5, .5, 0],
        [0, 0, .5, .5]
    ]))

    np.testing.assert_allclose(result[3], np.array([
        [.5, .5, 0, 0, 0],
        [0, .5, .5, 0, 0],
        [0, 0, .5, .5, 0],
        [0, 0, 0, .5, .5]
    ]))

    result = ramasvami.calc_p_matrices(np.array([1, 0]), 4)

    np.testing.assert_allclose(result[0], np.array([[1, 0]]))

    np.testing.assert_allclose(result[1], np.array([
        [1, 0, 0],
        [0, 1, 0]
    ]))

    np.testing.assert_allclose(result[2], np.array([
        [1, 0, 0, 0],
        [0, 1, 0, 0],
        [0, 0, 1, 0]
    ]))

    np.testing.assert_allclose(result[3], np.array([
        [1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 1, 0]
    ]))

