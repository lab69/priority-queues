import pytest
from analytics.metrics_model import MetricsModel
from analytics.model import MMAPPHSystem
from analytics.metrics import calc_metrics
import numpy as np
from numpy.testing import assert_allclose
from pyqumo.random import PhaseType
from pyqumo.arrivals import MarkovArrival
from pyqumo.cqumo.sim import simulate_gg1n
from pyqumo.sim.gg1 import Results
from pyqumo.matrix import cbdiag

RTOL = 11e-2


def compare_analytics_and_simulation(analytics_metric: MetricsModel, simulation: Results):
    # characteristics simulation and model are similar
    assert_allclose(simulation.system_size.mean, analytics_metric.packets_count, rtol=RTOL)
    #assert_allclose(simulation.system_size.mean, analytics_metric.priority_packets_count, rtol=RTOI)

    # length queue of packets count in buffer
    assert_allclose(simulation.queue_size.mean, analytics_metric.packets_in_buffer, rtol=RTOL)
    # assert_allclose(simulation.queue_size.mean, analytics_metric.priority_packets_in_buffer, rtol=RTOI)

    # busy devices
    assert_allclose(simulation.busy.mean, analytics_metric.busy_devices, rtol=RTOL)
    # assert_allclose(simulation.busy.mean, analytics_metric.priority_busy_devices, rtol=RTOI)

    # loss probability
    #assert_allclose(simulation.loss_prob, analytics_metric.loss_probability_priority_packets, rtol=RTOI)
    assert_allclose(simulation.loss_prob, analytics_metric.sum_loss_probability, rtol=RTOL)

    # base packets loss probability is equal to 0
    # assert_allclose(0, analytics_metric.loss_probability_base_packets, rtol=1e-10)


def execute_compare_simulation_and_analytic(mmap_system: MMAPPHSystem,
                                            phase_type: PhaseType,
                                            markovial_arival: MarkovArrival,
                                            R: int):
    mmap_system.solve()
    metric = calc_metrics(mmap_system)

    simulation_result = simulate_gg1n(markovial_arival, phase_type, R, max_packets=1000000)

    compare_analytics_and_simulation(metric, simulation_result)


@pytest.mark.parametrize('d0,d1,R_range', [
    (
            [[-1, 0, 0],
             [0, -8, 0],
             [0, 0, -2]],
            [[1, 0, 0],
             [0, 3, 5],
             [1, 1, 0]],
            range(2, 6)
    ),
    (
            [[-5, 1, 1],
             [0, -9, 0],
             [0, 0, -1]],
            [[1, 2, 0],
             [1, 3, 5],
             [1, 0, 0]],
            range(2, 6)
    ),
    (
            [[-15, 1, 7],
             [0, -14, 5],
             [0, 0, -1]],
            [[1, 2, 4],
             [1, 3, 5],
             [1, 0, 0]],
            range(2, 6)
    )
])
def test_hyperexponential_for_priority_packets(d0, d1, R_range):
    for R in R_range:
        beta = [1 / R for _ in range(R)]
        mmap_system = MMAPPHSystem(
            R=R,
            N=1,
            beta_vector_1=np.array(beta),
            beta_vector_2=np.array(beta),
            d0_matrix=np.array(d0),
            d1_matrix=np.array(d1),
            d2_matrix=np.zeros(shape=(len(d0[0]), len(d0[0]))),
            s1_matrix=-np.eye(R),
            s2_matrix=-np.eye(R),
            f_vector=np.zeros(R),
            q_vector=np.ones(R)
        )

        hyper_exponential = PhaseType.hyperexponential(rates=[1 for _ in range(R)], probs=np.array(beta))
        markovial_arival = MarkovArrival(d0, d1)

        execute_compare_simulation_and_analytic(mmap_system, hyper_exponential, markovial_arival, R)


@pytest.mark.parametrize('d0,d2,R_range', [
    (
            [[-1, 0, 0],
             [0, -8, 0],
             [0, 0, -2]],
            [[1, 0, 0],
             [0, 3, 5],
             [1, 1, 0]],
            range(2, 6)
    ),
    (
            [[-5, 1, 1],
             [0, -9, 0],
             [0, 0, -1]],
            [[1, 2, 0],
             [1, 3, 5],
             [1, 0, 0]],
            range(2, 6)
    ),
    (
            [[-15, 1, 7],
             [0, -14, 5],
             [0, 0, -1]],
            [[1, 2, 4],
             [1, 3, 5],
             [1, 0, 0]],
            range(2, 6)
    )
])
def test_hyperexponential_for_base_packets(d0, d2, R_range):
    for R in R_range:
        beta = [1 / R for _ in range(R)]
        mmap_system = MMAPPHSystem(
            R=R,
            N=1,
            beta_vector_1=np.array(beta),
            beta_vector_2=np.array(beta),
            d0_matrix=np.array(d0),
            d2_matrix=np.array(d2),
            d1_matrix=np.zeros(shape=(len(d0[0]), len(d0[0]))),
            s1_matrix=-np.eye(R),
            s2_matrix=-np.eye(R),
            q_vector=np.zeros(R),
            f_vector=np.ones(R)
        )

        hyper_exponential = PhaseType.hyperexponential(rates=[1 for _ in range(R)], probs=np.array(beta))
        markovial_arival = MarkovArrival(d0, d2)

        execute_compare_simulation_and_analytic(mmap_system, hyper_exponential, markovial_arival, R)


@pytest.mark.parametrize('d0,d1,R_range', [

    (
            [[-5, 1, 1],
             [0, -9, 0],
             [0, 0, -1]],
            [[1, 2, 0],
             [1, 3, 5],
             [1, 0, 0]],
            range(2, 6)
    ),
    (
            [[-9, 1, 1],
             [0, -9, 0],
             [0, 0, -1]],
            [[1, 2, 4],
             [1, 3, 5],
             [1, 0, 0]],
            range(2, 6)
    )
])
def test_erlang_for_priority_packets(d0, d1, R_range):
    blocks = [
        (0, np.asarray([[-1]])),
        (1, np.asarray([[1]]))
    ]

    for R in R_range:
        beta = [1 / R for _ in range(R)]

        mmap_system = MMAPPHSystem(
            R=R,
            N=1,
            beta_vector_1=np.array(beta),
            beta_vector_2=np.array(beta),
            d0_matrix=np.array(d0),
            d1_matrix=np.array(d1),
            d2_matrix=np.zeros(shape=(len(d0[0]), len(d0[0]))),
            s1_matrix=cbdiag(R, blocks),
            s2_matrix=cbdiag(R, blocks),
            f_vector=np.zeros(R),
            q_vector=np.ones(R)
        )

        erlang = PhaseType.erlang(shape=R, rate=1)
        markovial_arival = MarkovArrival(d0, d1)

        execute_compare_simulation_and_analytic(mmap_system, erlang, markovial_arival, R)

@pytest.mark.parametrize('d0,d2,R_range', [

    (
            [[-5, 1, 1],
             [0, -9, 0],
             [0, 0, -1]],
            [[1, 2, 0],
             [1, 3, 5],
             [1, 0, 0]],
            range(2, 6)
    ),
    (
            [[-9, 1, 1],
             [0, -9, 0],
             [0, 0, -1]],
            [[1, 2, 4],
             [1, 3, 5],
             [1, 0, 0]],
            range(2, 6)
    )
])
def test_erlang_for_base_packets(d0, d2, R_range):
    blocks = [
        (0, np.asarray([[-1]])),
        (1, np.asarray([[1]]))
    ]

    for R in R_range:
        beta = [1 / R for _ in range(R)]

        mmap_system = MMAPPHSystem(
            R=R,
            N=1,
            beta_vector_1=np.array(beta),
            beta_vector_2=np.array(beta),
            d0_matrix=np.array(d0),
            d2_matrix=np.array(d2),
            d1_matrix=np.zeros(shape=(len(d0[0]), len(d0[0]))),
            s1_matrix=cbdiag(R, blocks),
            s2_matrix=cbdiag(R, blocks),
            q_vector=np.zeros(R),
            f_vector=np.ones(R)
        )

        erlang = PhaseType.erlang(shape=R, rate=1)
        markovial_arival = MarkovArrival(d0, d2)

        execute_compare_simulation_and_analytic(mmap_system, erlang, markovial_arival, R)
