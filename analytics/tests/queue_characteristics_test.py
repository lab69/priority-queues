import numpy as np
from analytics import queue_characteristics
import math

d0 = np.array(
    [[-3, 1],
     [1, -3]]
)
d1 = np.array(
    [[0, 1],
     [1, 0]]
)
d2 = np.array(
    [[0, 1],
     [1, 0]]
)


def test_dispersion_value():
    result = queue_characteristics.calc_variance(np.array([0.5, 0.5]), 1, 1, d0, d1, d2)
    assert np.testing.assert_allclose(result, 1) is None
    result = queue_characteristics.calc_variance(np.array([0.5, 0.5]), 1, 2, d0, d1, d2)
    assert np.testing.assert_allclose(result, 1) is None


def test_correlation_value():
    result = queue_characteristics.calc_correlation(np.array([0.5, 0.5]), 1, 1, d0, d1, d2)
    np.testing.assert_allclose(math.fabs(result), 0., atol=1e-7)
    result = queue_characteristics.calc_correlation(np.array([0.5, 0.5]), 1, 2, d0, d1, d2)
    np.testing.assert_allclose(math.fabs(result), 0., atol=1e-7)
