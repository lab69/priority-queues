import numpy as np


def calc_variance(teta, l, k, *matrices) -> float:
    """
        function calculate vector of possible states
        :param teta - stationary distribution
        :param l - lambda for k-type of request
        :param k - type of request
        :param matrices - args with array D_{k} matrices
    """
    result_matrix = np.zeros(shape=matrices[0].shape)
    for index, matrix in enumerate(matrices):
        if index != k:
            result_matrix -= matrix
    result_matrix = np.linalg.inv(result_matrix)
    teta = teta.reshape(result_matrix.shape[0])
    return 2 * np.sum(np.dot(teta, result_matrix)) / l - (1 / (l ** 2))


def calc_correlation(teta, l, k, *matrices) -> float:
    """
        function correlation of k-type queue
        :param teta - stationary distribution
        :param l - lambda for k-type of request
        :param k - type of request
        :param matrices - args with array D_{k} matrices
    """
    varience = calc_variance(teta, l, k, *matrices)
    result_matrix = np.zeros(shape=matrices[0].shape)
    for index, matrix in enumerate(matrices):
        if index != k:
            result_matrix += matrix
    result_matrix = np.linalg.inv(result_matrix)
    teta = teta.reshape(result_matrix.shape[0])
    result = np.dot(
        np.dot(teta, result_matrix),
        np.dot(matrices[k], result_matrix)
    )
    multiply_result = np.sum(result)
    return ((multiply_result / l) - (1/(l ** 2))) / varience
