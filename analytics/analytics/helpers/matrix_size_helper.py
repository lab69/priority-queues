import numpy as np
from analytics.helpers import math_helper
from typing import Tuple


# noinspection PyPep8Naming
def q_0_1_block_shape(M1: int, M2: int, N: int, W: int) -> Tuple[int, int]:
    """
    Function calculates Q_{0,1} matrix shape

    Parameters
    ----------
    M1
    M2
    N
    W

    Returns
    -------

    """

    shape_x = 0
    shape_y = 0

    for n in range(0, N + 1):
        shape_y += math_helper.calc_combinations(n + M1 - 1, M1 - 1) * \
                   math_helper.calc_combinations(N - n + M2 - 1, M2 - 1)
        for r in range(0, n + 1):
            shape_x += math_helper.calc_combinations(r + M1 - 1, M1 - 1) * \
                       math_helper.calc_combinations(n - r + M2 - 1, M2 - 1)

    shape_y *= 2 * W
    shape_x *= W

    return shape_x, shape_y


def q_1_0_block_shape(M1: int, M2: int, N: int, W: int) -> Tuple[int, int]:
    """
    Function calculates Q_{1,0} matrix shape

    Parameters
    ----------
    M1
    M2
    N
    W

    Returns
    -------
    Tuple[int, int]
    """

    shape_x = 0
    shape_y = 0

    for n in range(0, N + 1):
        shape_x += math_helper.calc_combinations(n + M1 - 1, M1 - 1) * \
                   math_helper.calc_combinations(N - n + M2 - 1, M2 - 1)
        for r in range(0, n + 1):
            shape_y += math_helper.calc_combinations(r + M1 - 1, M1 - 1) * \
                       math_helper.calc_combinations(n - r + M2 - 1, M2 - 1)

    shape_y *= W
    shape_x *= 2 * W

    return shape_x, shape_y


def q_i_above_block_shape(i: int, M1: int, M2: int, N: int, W: int) -> Tuple[int, int]:
    """
    Function calculates Q_{i, i+1} block shape
    where i = 0..R-1

    Parameters
    ----------
    i
    M1
    M2
    N
    W

    Returns
    -------
    Tuple[int, int]
    """

    shape = 0

    for r in range(0, N + 1):
        shape += math_helper.calc_combinations(r + M1 - 1, M1 - 1) * \
                 math_helper.calc_combinations(N - r + M2 - 1, M2 - 1)

    return W * (i + 1) * shape, W * (i + 2) * shape


def q_i_sub_block_shape(i: int, M1: int, M2: int, N: int, W: int) -> Tuple[int, int]:
    """
    Function calculates Q_{i+1, i} block shape
    where i = 0..R-1

    Parameters
    ----------
    i
    M1
    M2
    N
    W

    Returns
    -------
    Tuple[int, int]
    """

    shape = 0

    for r in range(0, N + 1):
        shape += math_helper.calc_combinations(r + M1 - 1, M1 - 1) * \
                 math_helper.calc_combinations(N - r + M2 - 1, M2 - 1)

    return W * (i + 1) * shape, W * i * shape


def q_i_diagonal_block_shape(i: int, M1: int, M2: int, N: int, W: int) -> Tuple[int, int]:
    """
    Function calculates Q_{i, i} block shape
    where i = 1..R

    Parameters
    ----------
    i
    M1
    M2
    N
    W

    Returns
    -------
    Tuple[int, int]
    """

    shape = 0

    for r in range(0, N + 1):
        shape += math_helper.calc_combinations(r + M1 - 1, M1 - 1) * \
                 math_helper.calc_combinations(N - r + M2 - 1, M2 - 1)

    return W * (i + 1) * shape, W * (i + 1) * shape


def t_i_diagonal_block_shape(M1: int, M2: int, n: int, W: int) -> Tuple[int, int]:
    """
    Function calculates T_{i, i} blocks element shape of matrix T

    i=0..N-1

    Parameters
    ----------
    M1
    M2
    n
    W

    Returns
    -------
    Tuple[int, int]
    """

    shape = 0

    for r in range(0, n + 1):
        shape += math_helper.calc_combinations(r + M1 - 1, M1 - 1) * \
                 math_helper.calc_combinations(n - r + M2 - 1, M2 - 1)

    return W * shape, W * shape


def t_matrix_shape(N: int, M1: int, M2: int, W: int) -> Tuple[int, int]:
    """
    Function calculates T matrix shape

    Parameters
    ----------
    N
    M1
    M2
    W

    Returns
    -------
    Tuple[int, int]
    """
    shape = 0
    for n in range(0, N + 1):
        for r in range(0, n + 1):
            shape += math_helper.calc_combinations(r + M1 - 1, M1 - 1) * \
                     math_helper.calc_combinations(n - r + M2 - 1, M2 - 1)
    shape *= W
    return shape, shape


def q_full_matrix_shape(N: int, M1: int, M2: int, W: int, R: int) -> Tuple[np.ndarray, np.ndarray]:
    """
    Function calculates shape of full Q matrix

    Parameters
    ----------
    N
    M1
    M2
    W
    R

    Returns
    -------
    Tuple[np.ndarray, np.ndarray]
    """
    shape = np.sum(math_helper.calc_quantity(N, M1, M2, W, R))
    return shape, shape
