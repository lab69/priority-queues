from analytics.helpers import matrix_size_helper
from analytics.model import MMAPPHSystem
import matplotlib.pyplot as plt
import numpy as np
import time

M1 = 5
M2 = 5
W = 5
R = 5
N = 5

sizes = [2, 3, 4, 5, 6, 7, 8, 9, 10, 15, 20, 25]


def save_fig(x_list, y_list, x_label, y_label, filename):
    fig = plt.figure()
    ax = fig.add_subplot()

    ax.set_xlabel(x_label)
    ax.set_ylabel(y_label)

    ax.plot(x_list, y_list, "rx", linestyle="solid")

    #ax.set_yticks([10 ** i for i in range(1, 11)], minor=False)
    ax.set_xticks(x_list, minor=False)

    ax.grid(True)
    plt.show()

    fig.savefig(filename)


def calculate_count_devices_dependency():
    results = []

    print("Count devices.png dependency")

    for size in sizes:
        result = matrix_size_helper.q_full_matrix_shape(size, M1, M2, W, R)
        results.append(result[0])
        print(result[0])

    save_fig(sizes, results, 'Количество приборов', 'Порядок матрицы генератора', "devices")

    print("\n")


def calculate_ph_process_size_dependency():
    results = []

    print("Ph process size dependency")

    for size in sizes:
        result = matrix_size_helper.q_full_matrix_shape(N, M1, M2, size, R)
        results.append(result[0])
        print(result[0])

    save_fig(sizes, results, 'Порядок PH', 'Порядок матрицы генератора', "ph-size")

    print("\n")


def calculate_mmap_process_process_size_dependency():
    results = []

    print("MMAP process base size dependency")

    for size in sizes:
        result = matrix_size_helper.q_full_matrix_shape(N, size, size, W, R)
        results.append(result[0])
        print(result[0])

    save_fig(sizes, results, 'Порядок MMAP', 'Порядок матрицы генератора', "mmap-size")

    print("\n")


def calculate_mmap_process_and_ph():
    results = []

    print("MMAP process base and ph size dependency")

    for size in sizes:
        result = matrix_size_helper.q_full_matrix_shape(N, size, size, size, R)
        results.append(result[0])
        print(result[0])

    save_fig(sizes, results, 'Порядок MMAP * Порядок PH', 'Порядок матрицы генератора', "mmap-ph-size")

    print("\n")


def calculate_buffer_size():
    results = []

    print("MMAP process base and ph size dependency")

    for size in sizes:
        result = matrix_size_helper.q_full_matrix_shape(N, M1, M2, W, size)
        results.append(result[0])
        print(result[0])

    save_fig(sizes, results, 'Размер буфера', 'Порядок матрицы генератора', "buffer")

    print("\n")


def calculate_times_depend_on_devices():
    N = 5
    mmap_system = MMAPPHSystem(
        R=5,
        N=N,
        beta_vector_1=np.array([0.5, 0.2, 0.1, 0.1, 0.1]),
        beta_vector_2=np.array([0.5, 0.2, 0.1, 0.1, 0.1]),
        d0_matrix=np.array([
            [-27, 6, 9, 1, 2],
            [0, -39, 7, 3, 4],
            [8, 0, -36, 5, 6],
            [8, 0, 14, -28, 6],
            [8, 0, 36, -50, 6]
        ]),
        d1_matrix=np.array([
            [-27, 6, 9, 1, 2],
            [0, -39, 7, 3, 4],
            [8, 0, -36, 5, 6],
            [8, 0, 14, -28, 6],
            [8, 0, 36, -50, 6]
        ]),
        d2_matrix=np.array([
            [-27, 6, 9, 1, 2],
            [0, -39, 7, 3, 4],
            [8, 0, -36, 5, 6],
            [8, 0, 14, -28, 6],
            [8, 0, 36, -50, 6]
        ]),
        s1_matrix=np.array([
            [1, 2, 3, 1, 2],
            [5, 6, 8, 3, 4],
            [9, 0, 2, 5, 6],
            [9, 8, 9, 10, 16],
            [91, 1, 2, 3, 4]
        ]),
        s2_matrix=np.array([
            [1, 2, 3, 1, 2],
            [5, 6, 8, 3, 4],
            [9, 0, 2, 5, 6],
            [9, 8, 9, 10, 16],
            [91, 1, 2, 3, 4]
        ]),
        f_vector=np.array([0.5, 0.2, 0.1, 0.1, 0.1]),
        q_vector=np.array([0.5, 0.2, 0.1, 0.1, 0.1])
    )

    start_time = time.time()
    mmap_system.solve()
    print("For {} devices time = {}".format(N, time.time() - start_time))


#calculate_times_depend_on_devices()

#calculate_buffer_size()
# calculate_count_devices_dependency()
calculate_ph_process_size_dependency()
# calculate_mmap_process_process_size_dependency()
# calculate_mmap_process_and_ph()
