from dataclasses import dataclass


@dataclass(frozen=True)
class MetricsModel:
    packets_count: float
    priority_packets_count: float
    busy_devices: float
    priority_busy_devices: float
    packets_in_buffer: float
    priority_packets_in_buffer: float
    loss_probability_base_packets: float
    loss_probability_priority_packets: float
    sum_loss_probability: float