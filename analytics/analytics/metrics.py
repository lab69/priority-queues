import numpy as np
import analytics.helpers.math_helper as mh
from enum import Enum
from analytics.metrics_model import MetricsModel


class PacketType(Enum):
    PRIORITY = "priority"
    BASE = "base"


def calc_metrics(mmap_system) -> MetricsModel:
    return MetricsModel(
        packets_count=calc_packets(mmap_system),
        priority_packets_count=calc_priority_packets(mmap_system),
        busy_devices=calc_busy_devices(mmap_system),
        priority_busy_devices=calc_priority_busy_devices(mmap_system),
        packets_in_buffer=calc_packets_in_buffer(mmap_system),
        priority_packets_in_buffer=calc_priority_packets_in_buffer(mmap_system),
        loss_probability_base_packets=calc_loss_probability(mmap_system, PacketType.BASE),
        loss_probability_priority_packets=calc_loss_probability(mmap_system, PacketType.PRIORITY),
        sum_loss_probability=calc_sum_loss_probability(mmap_system),
    )


def calc_u_n_matrix(mmap_system, n) -> np.ndarray:
    W = mmap_system.tilda_W
    N = mmap_system.N
    M1 = mmap_system.M1
    M2 = mmap_system.M2

    y_shape = 0
    sub_zero_matrix_x_shape = 0
    above_zero_matrix_x_shape = 0

    for r in range(0, n + 1):
        y_shape += mh.calc_combinations(r + M1 - 1, M1 - 1) * \
                   mh.calc_combinations(n - r + M2 - 1, M2 - 1)

    y_shape *= W

    for k in range(0, n):
        for m in range(0, k + 1):
            above_zero_matrix_x_shape += mh.calc_combinations(m + M1 - 1, M1 - 1) * \
                                         mh.calc_combinations(k - m + M2 - 1, M2 - 1)

    above_zero_matrix_x_shape *= W

    for k in range(n + 1, N + 1):
        for m in range(0, k + 1):
            sub_zero_matrix_x_shape += mh.calc_combinations(m + M1 - 1, M1 - 1) * \
                                       mh.calc_combinations(k - m + M2 - 1, M2 - 1)

    sub_zero_matrix_x_shape *= W

    return np.vstack((
        np.vstack((
            np.zeros(shape=(above_zero_matrix_x_shape, y_shape)),
            np.eye(y_shape)
        )),
        np.zeros(shape=(sub_zero_matrix_x_shape, y_shape))
    ))


def calc_u_n_r_vector(mmap_system, n, r) -> np.ndarray:
    assert r <= n

    M1 = mmap_system.M1
    M2 = mmap_system.M2
    W = mmap_system.tilda_W

    above_zero_vector_size = 0
    ones_vector_size = W * mh.calc_combinations(r + M1 - 1, M1 - 1) * \
                       mh.calc_combinations(n - r + M2 - 1, M2 - 1)
    sub_zero_vector_size = 0

    for m in range(0, r):
        above_zero_vector_size += mh.calc_combinations(m + M1 - 1, M1 - 1) * \
                                  mh.calc_combinations(n - m + M2 - 1, M2 - 1)

    above_zero_vector_size *= W

    for m in range(r + 1, n + 1):
        sub_zero_vector_size += mh.calc_combinations(m + M1 - 1, M1 - 1) * \
                                mh.calc_combinations(n - m + M2 - 1, M2 - 1)

    sub_zero_vector_size *= W

    return np.concatenate(
        (np.zeros(above_zero_vector_size),
         np.ones(ones_vector_size),
         np.zeros(sub_zero_vector_size))
    )


def calc_p_i_k_r(mmap_system, i, k, r) -> np.ndarray:
    assert k <= i

    W = mmap_system.tilda_W
    M1 = mmap_system.M1
    M2 = mmap_system.M2
    N = mmap_system.N

    size = 0

    for n in range(0, N + 1):
        size += mh.calc_combinations(n + M1 - 1, M1 - 1) * \
                mh.calc_combinations(N - n + M2 - 1, M2 - 1)

    size *= W
    u_vector = calc_u_n_r_vector(mmap_system, N, r)

    return np.dot(
        mmap_system.stat_distribution[i],
        np.concatenate((
            np.zeros(k * size),
            u_vector,
            np.zeros((i - k) * size)
        ))
    )


def calc_p_i_k(mmap_system, i, k) -> int:
    result = 0
    for count in range(0, mmap_system.N + 1):
        result += calc_p_i_k_r(mmap_system, i, k, count)
    return result


def calc_p_i(mmap_system, i) -> float:
    res = 0
    for k in range(0, i + 1):
        res += calc_p_i_k(mmap_system, i, k)

    assert abs(res - mmap_system.stat_distribution[i].sum()) < .00001
    return res


def calc_p_0_n_vector(mmap_system, n) -> np.ndarray:
    assert n <= mmap_system.N

    return np.dot(
        mmap_system.stat_distribution[0],
        calc_u_n_matrix(mmap_system, n)
    )


def calc_p_0_n_r(mmap_system, n, r) -> np.ndarray:
    assert n <= mmap_system.N
    assert r <= n

    return np.dot(
        calc_p_0_n_vector(mmap_system, n),
        calc_u_n_r_vector(mmap_system, n, r)
    )


def calc_p_0_n(mmap_system, n) -> float:
    result = 0
    for _item in range(0, n + 1):
        result += calc_p_0_n_r(mmap_system, n, _item)
    return result


def calc_packets(mmap_system) -> float:
    """
    Calculate average packets count in system
    Args:
        mmap_system:

    Returns:
        packets count: float
    """
    N = mmap_system.N
    R = mmap_system.R

    result = 0

    for n in range(1, N + 1):
        result += n * calc_p_0_n(mmap_system, n)

    for i in range(1, R + 1):
        result += (i + N) * calc_p_i(mmap_system, i)

    return result


def calc_priority_packets(mmap_system) -> float:
    """
    Calculate average priority packets count in system
    Args:
        mmap_system:

    Returns:
        packets count: float
    """

    result = 0
    N = mmap_system.N
    R = mmap_system.R

    for n in range(1, N + 1):
        for r in range(1, n + 1):
            result += r * calc_p_0_n_r(mmap_system, n, r)

    for i in range(1, R + 1):
        for k in range(1, i + 1):
            result += (N + k) * calc_p_i_k(mmap_system, i, k)

    return result


def calc_busy_devices(mmap_system) -> float:
    """
    Calculate average busy devices in system

    Args:
        mmap_system:

    Returns:
        count of busy devices
    """

    N = mmap_system.N
    R = mmap_system.R

    result = 0
    for i in range(1, N + 1):
        result += i * calc_p_0_n(mmap_system, i)

    for i in range(1, R + 1):
        result += N * calc_p_i(mmap_system, i)

    return result


def calc_priority_busy_devices(mmap_system) -> float:
    """
    Function calculates average busy devices in system by priority packets

    Parameters
    ----------
    mmap_system

    Returns
    -------

    """

    result = 0
    N = mmap_system.N
    R = mmap_system.R

    for n in range(1, N + 1):
        for r in range(1, n + 1):
            result += r * calc_p_0_n_r(mmap_system, n, r)

    for i in range(1, R + 1):
        for k in range(0, i + 1):
            for r in range(1, N + 1):
                result += r * calc_p_i_k_r(mmap_system, i, k, r)

    return result


def calc_packets_in_buffer(mmap_system) -> float:
    """
    Function calculates average priority packets count in buffer

    Parameters
    ----------
    mmap_system

    Returns
    -------
    float
    """

    res = 0
    for _item in range(1, mmap_system.R + 1):
        res += _item * calc_p_i(mmap_system, _item)

    return res


def calc_priority_packets_in_buffer(mmap_system) -> float:
    """
    Function calculates average priority packets count in buffer

    Parameters
    ----------
    mmap_system

    Returns
    -------
    float
    """

    result = 0
    R = mmap_system.R

    for i in range(1, R + 1):
        for k in range(1, i + 1):
            result += k * calc_p_i_k(mmap_system, i, k)

    return result


def calc_loss_probability(mmap_system, packet_type: PacketType) -> float:
    """
    Function calculates sum probability of packet loss in mmap_system

    Parameters
    ----------
    mmap_system
    packet_type: PacketType

    Returns
    -------
    float
    """

    assert packet_type == PacketType.BASE or packet_type == PacketType.PRIORITY

    R = mmap_system.R
    N = mmap_system.N
    W = mmap_system.tilda_W
    M1 = mmap_system.M1
    M2 = mmap_system.M2
    eye = np.eye(W)

    eye_matrix = np.kron(
        eye,
        np.ones(shape=(mh.calc_combinations(N + M2 - 1, M2 - 1), 1))
    )

    for i in range(1, N + 1):
        eye_matrix = np.vstack((
            eye_matrix,
            mh.kron(
                eye,
                np.ones(
                    shape=(
                        mh.calc_combinations(i + M1 - 1, M1 - 1) *
                        mh.calc_combinations(N - i + M2 - 1, M2 - 1),
                        1
                    )
                )
            )
        ))

    if packet_type == PacketType.BASE:
        intensity = mmap_system.calc_intensity_base_packets()
        d_matrix = mmap_system.d2_matrix
        probs_vector = mmap_system.f_vector
    else:
        intensity = mmap_system.calc_intensity_priority_packets()
        d_matrix = mmap_system.d1_matrix
        probs_vector = mmap_system.q_vector

    vectors_sum = (1 - probs_vector[0]) * np.dot(calc_p_0_n_vector(mmap_system, N), eye_matrix) + \
                  np.dot(
                      mmap_system.stat_distribution[-1],
                      mh.kron(np.ones(shape=(R + 1, 1)), eye_matrix)
                  )

    for i in range(1, R):
        vectors_sum += (1 - probs_vector[i]) * mh.dot(
            mmap_system.stat_distribution[i],
            mh.kron(np.ones(shape=(i + 1, 1)), eye_matrix)
        )

    if intensity == 0:
        return 0

    return mh.dot(
        (1 / intensity) * vectors_sum,
        d_matrix
    ).sum()


def calc_sum_loss_probability(mmap_system) -> float:
    """
    Function calculates sum loss packet probability in mmap_system

    Parameters
    ----------
    mmap_system

    Returns
    -------
    float
    """
    intensity_1 = mmap_system.calc_intensity_base_packets()
    intensity_2 = mmap_system.calc_intensity_priority_packets()
    intensity_sum = mmap_system.calc_sum_intensity()

    p1_loss = calc_loss_probability(mmap_system, PacketType.BASE)
    p2_loss = calc_loss_probability(mmap_system, PacketType.PRIORITY)

    return (p1_loss * intensity_1 + p2_loss * intensity_2) / intensity_sum
