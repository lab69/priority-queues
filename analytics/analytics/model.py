import numpy as np
import analytics.matrices.q_matrix as q_matrix
import analytics.matrices.t_matrix as t_matrix
import analytics.matrices.delta_matrix as delta_matrix
from analytics.helpers import matrix_helper, math_helper
from analytics import stat_distribution, ramasvami, metrics
from typing import List


class MMAPPHSystem:
    """
        Class to keep base parameters of system
    """

    def __init__(self, R: int, N: int,
                 beta_vector_1: np.ndarray, beta_vector_2: np.ndarray,
                 d0_matrix: np.ndarray, d1_matrix: np.ndarray, d2_matrix: np.ndarray,
                 s1_matrix: np.ndarray, s2_matrix: np.ndarray,
                 f_vector: np.ndarray, q_vector: np.ndarray):
        assert beta_vector_1.shape[0] == s1_matrix.shape[0]
        assert beta_vector_2.shape[0] == s2_matrix.shape[0]
        assert d0_matrix.shape == d1_matrix.shape
        assert d1_matrix.shape == d2_matrix.shape
        assert f_vector.shape[0] == R
        assert q_vector.shape[0] == R

        self.W = d1_matrix.shape[0] - 1
        self.tilda_W = d1_matrix.shape[0]

        self.R = R
        self.N = N

        self.beta_vector_1 = beta_vector_1
        self.beta_vector_2 = beta_vector_2

        self.f_vector = f_vector
        self.q_vector = q_vector

        self.d0_matrix = d0_matrix
        self.d1_matrix = d1_matrix
        self.d2_matrix = d2_matrix

        self.s1_matrix = s1_matrix
        self.s2_matrix = s2_matrix

        self.M1 = self.s1_matrix.shape[0]
        self.M2 = self.s2_matrix.shape[0]

        sum_s1_matrix = - self.s1_matrix.sum(axis=1)
        sum_s1_matrix = np.reshape(sum_s1_matrix, (self.s1_matrix.shape[0], 1))

        self.s1_full_matrix = np.vstack((
            np.zeros(shape=(1, self.s1_matrix.shape[1] + 1)),
            np.hstack((
                sum_s1_matrix,
                self.s1_matrix
            ))
        ))

        sum_s2_matrix = - self.s2_matrix.sum(axis=1)
        sum_s2_matrix = np.reshape(sum_s2_matrix, (self.s2_matrix.shape[0], 1))

        self.s2_full_matrix = np.vstack((
            np.zeros(shape=(1, self.s2_matrix.shape[1] + 1)),
            np.hstack((
                sum_s2_matrix,
                self.s2_matrix
            ))
        ))

        self.above_diag_blocks = []
        self.diag_blocks = []
        self.sub_diag_blocks = []
        self.stat_distribution = []

        # Dicts for saving ramasvami calculation
        self.s1_l_matrices = {}
        self.s2_l_matrices = {}
        self.s1_a_matrices = {}
        self.s2_a_matrices = {}

        self.p1_matrices = []
        self.p2_matrices = []

    def solve(self):
        """
        Function calculate Q matrix of system
        (calculates T matrix and Q_{i}{j} matrices)
        Calculates base metrics of system

        Parameters
        ----------

        Returns
        -------

        """

        self.above_diag_blocks = [q_matrix.calc_q_0_1(self)] + q_matrix.calc_q_above_blocks(self)
        self.sub_diag_blocks = [q_matrix.calc_q_1_0(self)] + q_matrix.calc_q_sub_blocks(self)

        diag_blocks = [t_matrix.calc_t_matrix(self)] + q_matrix.calc_q_diagonal_blocks(self) + \
                      [q_matrix.calc_q_r_r_block(self)]

        delta_vectors = delta_matrix.calc_delta_matrices(
            self.sub_diag_blocks,
            diag_blocks,
            self.above_diag_blocks
        )

        for index, block in enumerate(diag_blocks):
            diag_blocks[index] = block + np.diag(np.array(delta_vectors[index]))

        self.diag_blocks = diag_blocks

        self.stat_distribution = stat_distribution.calc_stat_distribution(self.above_diag_blocks,
                                                                          self.diag_blocks,
                                                                          self.sub_diag_blocks)

        return self.stat_distribution

    def get_q_matrix(self) -> np.ndarray:
        """
        Function return full Q matrix of system

        Returns
        -------

        """

        return matrix_helper.concat_diag_blocks(
            self.diag_blocks
        ) + matrix_helper.concat_sub_diag_blocks(
            self.sub_diag_blocks,
            first_zero_above_block_height=self.diag_blocks[0].shape[0],
            last_zero_right_block_width=self.diag_blocks[-1].shape[1]
        ) + matrix_helper.concat_above_diag_blocks(
            self.above_diag_blocks,
            first_zero_left_block_width=self.diag_blocks[0].shape[1],
            last_zero_block_height=self.diag_blocks[-1].shape[0]
        )

    def calc_intensity_priority_packets(self) -> np.ndarray:
        """
        Method calculates intensity of
        base packets generation in MMAP process

        Returns
        -------
        np.ndarray
        """

        d1_matrix = self.d1_matrix
        stat_dist = math_helper.calc_stat_distribution_vector(self.d0_matrix + d1_matrix + self.d2_matrix)
        return np.dot(stat_dist, d1_matrix).sum()

    def calc_intensity_base_packets(self) -> np.ndarray:
        """
        Method calculates intensity of priority packets
        generation in MMAP process

        Returns
        -------
        np.ndarray
        """

        d2_matrix = self.d2_matrix
        stat_dist = math_helper.calc_stat_distribution_vector(self.d0_matrix + d2_matrix + self.d1_matrix)
        return np.dot(stat_dist, d2_matrix).sum()

    def calc_sum_intensity(self) -> np.ndarray:
        """
        Method calculates sum intensity of packets
        generation in MMAP process

        Returns
        -------
        np.ndarray
        """

        d_matrix = self.d1_matrix + self.d2_matrix
        stat_dist = math_helper.calc_stat_distribution_vector(self.d0_matrix + d_matrix)
        return np.dot(stat_dist, d_matrix).sum()

    def get_s1_l_i_matrices(self, index) -> List[np.ndarray]:
        assert index <= self.N

        if self.s1_l_matrices.get(index) is None:
            self.s1_l_matrices[index] = self.__calc_s1_l_matrices(index)

        return self.s1_l_matrices[index]

    def get_s2_l_i_matrices(self, index) -> List[np.ndarray]:
        assert index <= self.N

        if self.s2_l_matrices.get(index) is None:
            self.s2_l_matrices[index] = self.__calc_s2_l_matrices(index)

        return self.s2_l_matrices[index]

    def get_s1_a_i_matrices(self, index) -> List[np.ndarray]:
        assert index <= self.N

        if self.s1_a_matrices.get(index) is None:
            self.s1_a_matrices[index] = self.__calc_s1_a_matrices(index)

        return self.s1_a_matrices[index]

    def get_s2_a_i_matrices(self, index) -> List[np.ndarray]:
        assert index <= self.N

        if self.s2_a_matrices.get(index) is None:
            self.s2_a_matrices[index] = self.__calc_s2_a_matrices(index)

        return self.s2_a_matrices[index]

    def get_beta_1_p_i(self, index) -> np.ndarray:
        assert index <= self.N

        if len(self.p1_matrices) == 0:
            self.p1_matrices = self.__calc_p1_matrices()

        return self.p1_matrices[index]

    def get_beta_2_p_i(self, index) -> np.ndarray:
        assert index <= self.N

        if len(self.p2_matrices) == 0:
            self.p2_matrices = self.__calc_p2_matrices()

        return self.p2_matrices[index]

    def __calc_s1_l_matrices(self, index: int) -> List[np.ndarray]:
        """
        Method calculates L matrices for ramasvami method
        L matrices by index for S1 matrix,
        s1_full_matrix field

        L(n,S_{1})  where n=index
        Returns
        -------
        List[np.ndarray]
        """
        assert index <= self.N
        return ramasvami.calc_l_matrices(self.s1_full_matrix, index)

    def __calc_s2_l_matrices(self, index: int) -> List[np.ndarray]:
        """
        Method calculates L matrices for ramasvami method
        L matrices by index for S2 matrix,
        s2_full_matrix field

        L(n,S_{2})  where n=index
        Returns
        -------
        List[np.ndarray]
        """
        assert index <= self.N
        return ramasvami.calc_l_matrices(self.s2_full_matrix, index)

    def __calc_s1_a_matrices(self, index: int) -> List[np.ndarray]:
        """
        Method calculates A matrices by ramasvami method
        for n = index
        A matrices are calculated for matrix S_{1}

        Returns
        -------
        List[np.ndarray]
        """
        assert index <= self.N
        return ramasvami.calc_a_matrices(self.s1_matrix, index)

    def __calc_s2_a_matrices(self, index: int) -> List[np.ndarray]:
        """
        Method calculates A matrices by ramasvami method
        for n = index
        A matrices are calculated for matrix S_{2}

        Returns
        -------
        List[np.ndarray]
        """
        assert index <= self.N
        return ramasvami.calc_a_matrices(self.s2_matrix, index)

    def __calc_p1_matrices(self) -> List[np.ndarray]:
        """
        Method calculates P matrices by ramasvami method
        and caches it is in field of object

        P matrices are calculated for both initial probability vectors:

        beta_vector_1: P(Beta_{1})

        Matrices are saved in p1_matrices field

        Returns
        -------
        List[np.ndarray]
        """

        return ramasvami.calc_p_matrices(self.beta_vector_1, self.N)

    def __calc_p2_matrices(self) -> List[np.ndarray]:
        """
        Method calculates P matrices by ramasvami method
        and caches it is in field of object

        P matrices are calculated for both initial probability vectors:

        beta_vector_2: P(Beta_{2})

        Matrices are saved in p1_matrices field

        Returns
        -------
        List[np.ndarray]
        """
        return ramasvami.calc_p_matrices(self.beta_vector_2, self.N)
