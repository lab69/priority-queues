import numpy as np
from analytics.helpers import matrix_helper
from typing import List


def get_tau_k(S: np.ndarray, k: int) -> np.ndarray:
    """
    Function returns submatrix depends on k

        |1 2 3 4|                       |6 7 8|
        |5 6 7 8|                       |1 0 1|
    S = |9 1 0 1|   ---> (k = 1) ---->  |3 4 5|
        |2 3 4 5|


        |1 2 3 4|
        |5 6 7 8|                       |0 1|
    S = |9 1 0 1|   ---> (k = 2) ---->  |4 5|
        |2 3 4 5|


    Parameters
    ----------
    S - matrix ndarray
    k - count of columns and rows are removed from matrix

    Returns
    -------
    np.ndarray

    """

    return S[k:, k:]


def calc_l_matrices(s_matrix: np.ndarray, N: int) -> List[np.ndarray]:
    """
    Function calculates L matrices list by S matrix and N parameters

    Parameters
    ----------
    s_matrix - S matrix of system
    N - param of system, count of devices

    Returns
    -------
    List[np.ndarray]
    Returns
    -------
    List[np.ndarray
    """

    r_j = s_matrix.shape[0] - 1
    l_matrices = []

    for i in range(0, N):
        l_matrices.append(np.array([[(N - i) * s_matrix[r_j][0]]]))

    for w in range(1, r_j):
        for i in range(0, N):
            sub_diag_elements = [l_matrices[N - x] for x in range(1, N - i + 1)]
            diag_elements = [np.eye(sub_diag_elements[x - i].shape[1]) * (N - x) * s_matrix[r_j - w][0] for x in range(i, N)]
            l_matrices[i] = concat_l_blocks(diag_elements, sub_diag_elements)

    l_matrices.append(np.array([[0]]))

    return l_matrices


def calc_l_i_matrices(s_matrix: np.ndarray, N: int) -> List[List[np.ndarray]]:
    """
    Function calculates 2-d list of L matrices
    for each iteration from 0 to M-2, where M - is shape of S matrix

    Parameters
    ----------
    s_matrix - S matrix
    N - param of system, count of devices

    Returns
    -------
    List[List[np.ndarray]]
    """

    M = s_matrix.shape[0]
    return [calc_l_matrices(get_tau_k(s_matrix, M - 3 - j), N) for j in range(0, M - 2)]


def calc_u_matrices(s_matrix: np.ndarray, N: int) -> List[np.ndarray]:
    """
    Function calculates U matrices by S matrix of system and N parameter system


    Parameters
    ----------
    s_matrix - S matrix of system
    N - count of devices

    Returns
    -------
    List[np.ndarray]

    """

    r_j = s_matrix.shape[0] - 1
    u_matrices = [np.array([[s_matrix[0][r_j]]]) for _ in range(0, N)]

    for w in range(1, r_j):
        for i in range(0, N):
            above_diag_elements = [u_matrices[N - x] for x in range(1, N - i + 1)]
            diag_elements = [np.eye(above_diag_elements[x - i].shape[0]) * s_matrix[0][r_j - w] for x in range(i, N)]
            u_matrices[i] = concat_u_blocks(diag_elements, above_diag_elements)

    return [(index + 1) * u_matrix for index, u_matrix in enumerate(u_matrices)]


def calc_u_i_j_matrices(s_matrix: np.ndarray, N: int) -> List[List[np.ndarray]]:
    """
    Function calculates List of List u matrices for each iteration from 0 to M-2,
    where M = s_matrix.shape - 2

    Parameters
    ----------
    s_matrix - S matrix of system
    N - parameter of system

    Returns
    -------
    List[List[np.ndarray]]
    """

    M = s_matrix.shape[0]
    return [calc_u_matrices(get_tau_k(s_matrix, M - 3 - j), N) for j in range(0, M - 2)]


def calc_a_matrices(s_matrix: np.ndarray, N: int) -> List[np.ndarray]:
    """
    Function calculates A matrices of system

    Parameters
    ----------
    s_matrix
    N

    Returns
    -------
    List[np.ndarray]
    """

    M = s_matrix.shape[0]

    s_m_m_1 = s_matrix[M - 1][M - 2]
    s_m_1_m = s_matrix[M - 2][M - 1]
    a_matrices = []

    for i in range(1, N + 1):
        above_diag_blocks = [np.array([[(i - x) * s_m_1_m]]) for x in range(0, i)]
        diag_blocks = [np.array([[0]]) for _ in range(0, i + 1)]
        sub_diag_blocks = [np.array([[x * s_m_m_1]]) for x in range(1, i + 1)]
        a_matrices.append(
            concat_a_blocks(
                diag_blocks,
                above_diag_blocks,
                sub_diag_blocks
            )
        )

    for j in range(0, M - 2):
        t_matrix = get_tau_k(s_matrix, M - 3 - j)

        l_matrices = calc_l_matrices(t_matrix, N)
        u_matrices = calc_u_matrices(t_matrix, N)

        prev_a_matrices = a_matrices[:]

        for i in range(1, N + 1):
            above_diag_blocks = [(i - x) * u_matrices[N - x - 1] / (N - x) for x in range(0, i)]
            diag_blocks = prev_a_matrices[0:i]
            sub_diag_blocks = [l_matrices[N - x - 1] for x in range(0, i)]
            diag_blocks.insert(0, np.zeros(shape=(above_diag_blocks[0].shape[0], sub_diag_blocks[0].shape[1])))
            a_matrices[i - 1] = concat_a_blocks(
                diag_blocks,
                above_diag_blocks,
                sub_diag_blocks
            )

    a_matrices.insert(0, np.array([[0]]))

    return a_matrices


def calc_p_matrices(beta_vector: np.ndarray, N: int) -> List[np.ndarray]:
    """
    Function calculate P matrices of system

    Parameters
    ----------
    beta_vector
    N

    Returns
    -------
    List[np.ndarray]
    """
    M = beta_vector.size

    beta_m_1 = beta_vector[-2]
    beta_m = beta_vector[-1]
    p_matrices = []

    diag_p_zero_matrix_blocks = [np.array([[beta_m_1]]), np.array([[beta_m_1]])]
    above_diag_p_zero_matrix_blocks = [np.array([[beta_m]]), np.array([[beta_m]])]

    for i in range(1, N):
        p_matrices.append(
            matrix_helper.concat_diag_blocks(diag_p_zero_matrix_blocks, additional_right_zeros=1) +
            matrix_helper.concat_above_diag_blocks(above_diag_p_zero_matrix_blocks, first_zero_left_block_width=1)
        )

        diag_p_zero_matrix_blocks.append(np.array([[beta_m_1]]))
        above_diag_p_zero_matrix_blocks.append(np.array([[beta_m]]))

    for j in range(1, M - 1):
        p_prev_matrices = p_matrices[:]

        for i in range(1, N):
            above_diag_blocks = [p_prev_matrices[x] for x in range(0, i)]

            # add beta vector

            sub_beta_vector = beta_vector[M - j - 1:]
            above_diag_blocks.insert(0, np.array([[x for x in sub_beta_vector]]))

            diag_blocks = [beta_vector[M - j - 2] * np.eye(above_diag_blocks[x].shape[0]) for x in range(0, i + 1)]

            p_matrices[i - 1] = matrix_helper.concat_diag_blocks(
                diag_blocks,
                additional_right_zeros=above_diag_blocks[-1].shape[1]) + \
                                matrix_helper.concat_above_diag_blocks(
                                    above_diag_blocks,
                                    first_zero_left_block_width=1
                                )

    p_matrices.insert(0, np.reshape(beta_vector, (1, beta_vector.shape[0])))

    return p_matrices


def concat_l_blocks(diag_blocks: List[np.ndarray],
                    sub_diag_blocks: List[np.ndarray]) -> np.ndarray:
    """
    Function concatenates blocks into single L matrix

                diag_blocks = [D1, D2, D3]
                sub_diag_blocks = [S1, S2, S3]

                it concatenates into block matrix:

                |D1 O  O   O|
                |S1 D2 O   O|
                |O  S2 D3  O|
                |O  O  S2 D3|

    Parameters
    ----------
    diag_blocks
    sub_diag_blocks

    Returns
    -------

    """

    assert len(diag_blocks) == len(sub_diag_blocks)

    sub_diag_matrix = matrix_helper.concat_sub_diag_blocks(sub_diag_blocks)
    diag_matrix = matrix_helper.concat_diag_blocks(
        diag_blocks,
        additional_sub_zeros=sub_diag_blocks[-1].shape[0]
    )

    diag_matrix, sub_diag_matrix = matrix_helper.align_matrices_shapes(diag_matrix, sub_diag_matrix)

    return sub_diag_matrix + diag_matrix


def concat_u_blocks(diag_blocks: List[np.ndarray],
                    above_diag_blocks: List[np.ndarray]) -> np.ndarray:
    """
    Function concatenates blocks into single U matrix

                diag_blocks = [D1, D2, D3]
                above_diag_blocks = [A1, A2, A3]

                it concatenates into block matrix:

                |D1 A1 O O |
                |O D2 A2 O |
                |O  O D3 A3|

    Parameters
    ----------
    diag_blocks
    above_diag_blocks

    Returns
    -------
    np.ndarray
    """

    assert len(diag_blocks) == len(above_diag_blocks)

    above_diag_matrix = matrix_helper.concat_above_diag_blocks(
        above_diag_blocks,
        first_zero_left_block_width=diag_blocks[0].shape[1]
    )
    diag_matrix = matrix_helper.concat_diag_blocks(diag_blocks)

    diag_matrix, above_diag_matrix = matrix_helper.align_matrices_shapes(diag_matrix, above_diag_matrix)

    return above_diag_matrix + diag_matrix


def concat_a_blocks(diag_blocks: List[np.ndarray],
                    above_diag_blocks: List[np.ndarray],
                    sub_diag_blocks: List[np.ndarray]) -> np.ndarray:
    """
    Function concatenates blocks into single A matrix

                diag_blocks = [D1, D2, D3]
                above_diag_blocks = [A1, A2]
                sud_diag_blocks = [S1, S2]

                it concatenates into block matrix:

                |D1 A1 O |
                |S1 D2 A2|
                |O  S2 D3|
    Parameters
    ----------
    diag_blocks
    above_diag_blocks
    sub_diag_blocks

    Returns
    -------
    np.ndarray
    """

    assert len(diag_blocks) - 1 == len(sub_diag_blocks)
    assert len(diag_blocks) - 1 == len(above_diag_blocks)

    above_diag_matrix = matrix_helper.concat_above_diag_blocks(
        above_diag_blocks,
        first_zero_left_block_width=diag_blocks[0].shape[1],
        last_zero_block_height=diag_blocks[-1].shape[0]
    )
    sub_diag_matrix = matrix_helper.concat_sub_diag_blocks(
        sub_diag_blocks,
        first_zero_above_block_height=diag_blocks[0].shape[0],
        last_zero_right_block_width=diag_blocks[-1].shape[1]
    )
    diag_matrix = matrix_helper.concat_diag_blocks(diag_blocks)
    diag_matrix, above_diag_matrix = matrix_helper.align_matrices_shapes(diag_matrix, above_diag_matrix)

    sum_matrix = diag_matrix + above_diag_matrix
    sum_matrix, sub_diag_matrix = matrix_helper.align_matrices_shapes(sum_matrix, sub_diag_matrix)

    return sum_matrix + sub_diag_matrix
