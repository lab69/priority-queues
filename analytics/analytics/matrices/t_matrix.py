import numpy as np
from analytics.helpers import math_helper as mh, matrix_helper
from typing import List


def calc_t_matrix(mmap_system) -> np.ndarray:
    """
    Calculate T matrix is (0, 0) element of Q matrix of system

    Parameters
    ----------
    mmap_system:

    Returns
    -------
    np.ndarray

    """

    diag_blocks = calc_t_n_n_blocks(mmap_system)
    diag_blocks.append(calc_t_N_N_block(mmap_system))

    sub_diag_blocks = calc_t_sub_blocks(mmap_system)

    above_diag_blocks = calc_t_above_blocks(mmap_system)

    return matrix_helper.concat_diag_blocks(
        diag_blocks
    ) + matrix_helper.concat_sub_diag_blocks(
        sub_diag_blocks,
        first_zero_above_block_height=diag_blocks[0].shape[0],
        last_zero_right_block_width=diag_blocks[-1].shape[1]
    ) + matrix_helper.concat_above_diag_blocks(
        above_diag_blocks,
        first_zero_left_block_width=diag_blocks[0].shape[1],
        last_zero_block_height=diag_blocks[-1].shape[0]
    )


def calc_t_n_n_blocks(mmap_system) -> List[np.ndarray]:
    """
    Function calculates T_{n}{n} elements of matrix T,
    where n = 0..N-1, T_{N}{N} is calculated in function calc_t_N_N_block

    Parameters
    ----------
    mmap_system

    Returns
    -------
    List[np.ndarray]
    """

    d0_matrix = mmap_system.d0_matrix
    N = mmap_system.N

    result_blocks = []

    for n in range(0, N):
        blocks = []
        for r in range(0, n + 1):
            a_matrices_s1 = mmap_system.get_s1_a_i_matrices(N - n + r)
            a_matrices_s2 = mmap_system.get_s2_a_i_matrices(N - r)
            blocks.append(mh.kron_sum(
                d0_matrix,
                a_matrices_s1[r],
                a_matrices_s2[n - r]
            ))
        result_blocks.append(matrix_helper.concat_diag_blocks(blocks))

    return result_blocks


def calc_t_sub_blocks(mmap_system) -> List[np.ndarray]:
    """
    Function calculates sub blocks T_{n}{n+1}, where n=1..N-1

    Parameters
    ----------
    mmap_system

    Returns
    -------
    List[np.ndarray]
    """

    N = mmap_system.N
    W = mmap_system.tilda_W
    M1 = mmap_system.M1
    M2 = mmap_system.M2

    result_matrices = []

    for n in range(1, N + 1):
        left_upper_blocks = []
        right_upper_blocks = []

        l_matrices_s2 = mmap_system.get_s2_l_i_matrices(N)
        left_upper_blocks.append(
            np.kron(np.eye(W), l_matrices_s2[N - n])
        )

        for r in range(1, n):
            l_matrices_s2 = mmap_system.get_s2_l_i_matrices(N - r)
            l_matrices_s1 = mmap_system.get_s1_l_i_matrices(N - n + r)

            left_upper_blocks.append(
                np.kron(np.eye(W * mh.calc_combinations(r + M1 - 1, M1 - 1)), l_matrices_s2[N - n])
            )

            right_upper_blocks.append(
                mh.kron(
                    np.eye(W),
                    l_matrices_s1[N - n],
                    np.eye(mh.calc_combinations(n - r + M2 - 1, M2 - 1))
                )
            )

        # above part aggregation
        if n > 1:
            above_matrix = matrix_helper.concat_diag_blocks(left_upper_blocks) + \
                           matrix_helper.concat_sub_diag_blocks(
                               right_upper_blocks,
                               first_zero_above_block_height=left_upper_blocks[0].shape[0],
                               last_zero_right_block_width=left_upper_blocks[-1].shape[1]
                           )
        else:
            above_matrix = matrix_helper.concat_diag_blocks(left_upper_blocks)

        # sub part calculation

        l_matrices_s1 = mmap_system.get_s1_l_i_matrices(N)

        if n > 1:
            left_down_part = np.hstack((
                np.zeros(
                    shape=(W * mh.calc_combinations(n + M1 - 1, M1 - 1),
                           W * mh.calc_sum_multiply_combinations(n, M1, M2, 0, n - 2))
                ),
                np.kron(
                    np.eye(W),
                    l_matrices_s1[N - n]
                )
            ))
        else:
            left_down_part = np.kron(
                np.eye(W),
                l_matrices_s1[N - n]
            )

        right_down_part = np.zeros(
            shape=(W * mh.calc_combinations(n + M1 - 1, M1 - 1),
                   W * mh.calc_sum_multiply_combinations(n, M1, M2, 0, n - 1))
        )

        result_matrices.append(
            np.vstack((
                above_matrix,
                left_down_part + right_down_part
            ))
        )

    return result_matrices


def calc_t_above_blocks(mmap_system) -> List[np.ndarray]:
    """
    Function calculates above blocks T_{n}{n-1}, where n=1..N

    Parameters
    ----------
    mmap_system

    Returns
    -------
    List[np.ndarray]
    """

    d1_matrix = mmap_system.d1_matrix
    d2_matrix = mmap_system.d2_matrix
    M1 = mmap_system.M1
    M2 = mmap_system.M2
    W = mmap_system.tilda_W
    N = mmap_system.N

    result_matrices = []
    p_matrices_beta_1 = mmap_system.p1_matrices
    p_matrices_beta_2 = mmap_system.p2_matrices

    for n in range(0, N):
        # left block calculation
        left_blocks = [
            mh.kron(
                d2_matrix,
                np.eye(mh.calc_combinations(r + M1 - 1, M1 - 1)),
                p_matrices_beta_2[n - r]
            ) for r in range(0, n + 1)
        ]

        # right block calculation
        right_blocks = [
            mh.kron(
                d1_matrix,
                p_matrices_beta_1[r],
                np.eye(mh.calc_combinations(n - r + M2 - 1, M2 - 1))
            ) for r in range(0, n)
        ]

        # central block calculation
        central_upper_zero_part = calc_central_zero_upper_part(W, n, M1, M2)
        central_down_zero_part = calc_central_zero_down_part(W, n, M1, M2)
        down_non_zero_part = np.kron(
            d1_matrix,
            p_matrices_beta_1[n]
        )

        central_part = np.vstack((
            central_upper_zero_part,
            np.hstack((
                central_down_zero_part,
                down_non_zero_part
            ))
        ))

        left_part = np.hstack((
            matrix_helper.concat_diag_blocks(left_blocks),
            calc_zero_part_matrix_for_above_blocks(W, n, M1, M2)
        ))

        if n > 0:
            right_part = np.hstack((
                matrix_helper.concat_above_diag_blocks(
                    right_blocks,
                    first_zero_left_block_width=left_blocks[0].shape[1],
                    last_zero_block_height=left_blocks[-1].shape[0]
                ),
                calc_zero_part_matrix_for_above_blocks(W, n, M1, M2)
            ))

            result_matrices.append(
                left_part + central_part + right_part
            )
        else:
            result_matrices.append(left_part + central_part)

    return result_matrices


def calc_central_zero_upper_part(W: int, n: int, M1: int, M2: int) -> np.ndarray:
    shape_x = 0
    shape_y = 0

    for r in range(0, n):
        shape_x += mh.calc_combinations(r + M1 - 1, M1 - 1) * mh.calc_combinations(n - r + M2 - 1, M2 - 1)

    for r in range(0, n + 2):
        shape_y += mh.calc_combinations(r + M1 - 1, M1 - 1) * mh.calc_combinations(n - r + M2, M2 - 1)

    shape_x *= W
    shape_y *= W

    return np.zeros(shape=(shape_x, shape_y))


def calc_central_zero_down_part(W: int, n: int, M1: int, M2: int) -> np.ndarray:
    shape_x = W * mh.calc_combinations(n + M1 - 1, M1 - 1)
    shape_y = 0

    for r in range(0, n + 1):
        shape_y += mh.calc_combinations(r + M1 - 1, M1 - 1) * mh.calc_combinations(n - r + M2, M2 - 1)

    shape_y *= W

    return np.zeros(shape=(shape_x, shape_y))


def calc_zero_part_matrix_for_above_blocks(W: int, n: int, M1: int, M2: int) -> np.ndarray:
    shape_x = 0
    shape_y = W * mh.calc_combinations(n + M1, M1 - 1)

    for r in range(0, n + 1):
        shape_x += mh.calc_combinations(r + M1 - 1, M1 - 1) * mh.calc_combinations(n - r + M2 - 1, M2 - 1)

    shape_x *= W

    return np.zeros(shape=(shape_x, shape_y))


def calc_t_N_N_block(mmap_system, is_sum=False) -> np.ndarray:
    """
    Function calculates last diag block T_{N}{N} of matrix T

    Parameters
    ----------
    mmap_system

    Returns
    -------
    np.ndarray
    """

    if is_sum:
        d_matrix = mmap_system.d0_matrix + \
                   mmap_system.d1_matrix + \
                   mmap_system.d2_matrix
    else:
        d_matrix = mmap_system.d0_matrix + \
               mmap_system.d1_matrix * (1 - mmap_system.q_vector[0]) + \
               mmap_system.d2_matrix * (1 - mmap_system.f_vector[0])

    N = mmap_system.N

    diag_blocks = [
        mh.kron_sum(
            d_matrix,
            mmap_system.get_s1_a_i_matrices(r)[r],
            mmap_system.get_s2_a_i_matrices(N - r)[N - r]
        ) for r in range(0, N + 1)
    ]

    return matrix_helper.concat_diag_blocks(diag_blocks)
