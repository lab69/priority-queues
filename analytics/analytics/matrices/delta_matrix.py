
def calc_delta_matrices(sub_diag_blocks, diag_blocks, above_diag_blocks):
    """
    Function calculate list of delta matrices for diagonal elements
    in matrix Q of system

    Parameters
    ----------
    sub_diag_blocks: list of sub diagonal matrices of Q matrix, Q_{i+1, i}, i=0..R-1
    diag_blocks: list of diagonal matrices of Q matrix, Q_{i, i}, i=0..R
    above_diag_blocks: list of above diagonal matrices of Q matrix, Q_{i, i+1}, i=0..R-1

    Returns
    -------
    List(ndarray)
    """

    delta_vectors = []

    diag_matrices_count = len(diag_blocks)

    # first iteration
    delta_vectors.append(-diag_blocks[0].sum(axis=1) - above_diag_blocks[0].sum(axis=1))

    for index in range(1, diag_matrices_count - 1):
        delta_vectors.append(
            -diag_blocks[index].sum(axis=1) -
            above_diag_blocks[index].sum(axis=1) -
            sub_diag_blocks[index - 1].sum(axis=1)
        )

    delta_vectors.append(-diag_blocks[-1].sum(axis=1) - sub_diag_blocks[-1].sum(axis=1))

    return delta_vectors
