import numpy as np
from analytics.helpers import matrix_helper, math_helper as mh
from analytics.matrices.t_matrix import calc_t_N_N_block
from typing import List


def calc_q_0_1(mmap_system) -> np.ndarray:
    """
    Function calculates Q_{0,1} block of matrix Q

    Parameters
    ----------
    mmap_system

    Returns
    -------
    np.ndarray
    """

    h1_blocks = []
    h2_blocks = []

    d1_matrix = mmap_system.d1_matrix
    d2_matrix = mmap_system.d2_matrix
    W = mmap_system.W + 1
    N = mmap_system.N
    M1 = mmap_system.M1
    M2 = mmap_system.M2
    f0 = mmap_system.f_vector[0]
    q0 = mmap_system.q_vector[0]

    for r in range(0, N + 1):
        eye_matrix = np.eye(
            mh.calc_combinations(r + M1 - 1, M1 - 1) * mh.calc_combinations(N - r + M2 - 1, M2 - 1)
        )
        h1_blocks.append(
            np.kron(
                d2_matrix,
                eye_matrix
            )
        )
        h2_blocks.append(
            np.kron(
                d1_matrix,
                eye_matrix
            )
        )

    h_matrix = np.hstack((
        f0 * matrix_helper.concat_diag_blocks(h1_blocks),
        q0 * matrix_helper.concat_diag_blocks(h2_blocks)
    ))

    return np.vstack((
        calc_zero_matrix_for_q_0_1_block(W, N, M1, M2),
        h_matrix
    ))


def calc_zero_matrix_for_q_0_1_block(W, N, M1, M2) -> np.ndarray:
    shape_x = 0
    shape_y = 0

    for n in range(0, N):
        for r in range(0, n + 1):
            shape_x += mh.calc_combinations(r + M1 - 1, M1 - 1) * mh.calc_combinations(n - r + M2 - 1, M2 - 1)

    for r in range(0, N + 1):
        shape_y += mh.calc_combinations(r + M1 - 1, M1 - 1) * mh.calc_combinations(N - r + M2 - 1, M2 - 1)

    shape_x *= W
    shape_y *= 2 * W

    return np.zeros(shape=(shape_x, shape_y))


def calc_zero_matrix_for_q_1_0_block(W, N, M1, M2) -> np.ndarray:
    shape_x = 0
    shape_y = 0

    for r in range(0, N + 1):
        shape_x += mh.calc_combinations(r + M1 - 1, M1 - 1) * mh.calc_combinations(N - r + M2 - 1, M2 - 1)

    for n in range(0, N):
        for r in range(0, n + 1):
            shape_y += mh.calc_combinations(r + M1 - 1, M1 - 1) * mh.calc_combinations(n - r + M2 - 1, M2 - 1)

    shape_x *= W
    shape_y *= W

    return np.zeros(shape=(shape_x, shape_y))


def calc_f_1_matrix(mmap_system) -> np.ndarray:
    """
    Function calculates F_{1} matrix

    Parameters
    ----------
    mmap_system

    Returns
    -------
    np.ndarray
    """

    W = mmap_system.tilda_W
    N = mmap_system.N
    M1 = mmap_system.M1
    M2 = mmap_system.M2

    diag_blocks = [np.kron(
        np.eye(W * mh.calc_combinations(r + M1 - 1, M1 - 1)),
        np.dot(
            mmap_system.get_s2_l_i_matrices(N - r)[0],
            mmap_system.get_beta_2_p_i(N - r - 1)
        )
    ) for r in range(0, N)]


    zeros_matrix_size = W * mh.calc_combinations(N + M1 - 1, M1 - 1)
    diag_blocks.append(np.zeros(shape=(zeros_matrix_size, zeros_matrix_size)))

    sub_diag_blocks = [mh.kron(
        np.eye(W),
        mmap_system.get_s1_l_i_matrices(r)[0],
        mmap_system.get_beta_2_p_i(N - r)
    ) for r in range(1, N + 1)]

    for index, block in enumerate(sub_diag_blocks):
        # this is assertion fo ability of concatenation of blocks
        assert diag_blocks[index].shape[1] == block.shape[1]
        assert diag_blocks[index + 1].shape[0] == block.shape[0]

    return matrix_helper.concat_diag_blocks(diag_blocks) + matrix_helper.concat_sub_diag_blocks(
        sub_diag_blocks,
        first_zero_above_block_height=diag_blocks[0].shape[0],
        last_zero_right_block_width=diag_blocks[-1].shape[1])


def calc_f_2_matrix(mmap_system) -> np.ndarray:
    """
    Function clculates F_{2} matrix of system

    Parameters
    ----------
    mmap_system

    Returns
    -------
    np.ndarray
    """

    W = mmap_system.tilda_W
    N = mmap_system.N
    M2 = mmap_system.M2

    zero_matrix_size = W * mh.calc_combinations(N + M2 - 1, M2 - 1)

    diag_blocks = [
        mh.kron(
            np.eye(W),
            np.dot(mmap_system.get_s1_l_i_matrices(r)[0], mmap_system.get_beta_1_p_i(r - 1)),
            np.eye(mh.calc_combinations(N - r + M2 - 1, M2 - 1))
        ) for r in range(1, N + 1)
    ]

    diag_blocks.insert(0, np.zeros(shape=(zero_matrix_size, zero_matrix_size)))

    above_diag_blocks = [mh.kron(
        np.eye(W),
        mmap_system.get_beta_1_p_i(r),
        mmap_system.get_s2_l_i_matrices(N - r)[0]
    ) for r in range(0, N)]

    return matrix_helper.concat_diag_blocks(diag_blocks) + matrix_helper.concat_above_diag_blocks(
        above_diag_blocks,
        first_zero_left_block_width=diag_blocks[0].shape[1],
        last_zero_block_height=diag_blocks[-1].shape[0]
    )


def calc_q_1_0(mmap_system) -> np.ndarray:
    """
    Function calculates Q_{1,0} blocks of Q matrix

    Parameters
    ----------
    mmap_system

    Returns
    -------
    np.ndarray
    """

    zero_block = calc_zero_matrix_for_q_1_0_block(
        mmap_system.W + 1, mmap_system.N, mmap_system.M1, mmap_system.M2
    )

    f1_matrix = calc_f_1_matrix(mmap_system)
    f2_matrix = calc_f_2_matrix(mmap_system)

    return np.hstack((
        np.vstack((zero_block, zero_block)),
        np.vstack((f1_matrix, f2_matrix))
    ))


# i, i-1
def calc_q_sub_blocks(mmap_system) -> List[np.ndarray]:
    """
    Function calculates list of Q_{i, i-1}, i=1..R of matrix Q

    Parameters
    ----------
    mmap_system

    Returns
    -------
    List(ndarray)
    """

    W = mmap_system.tilda_W
    R = mmap_system.R
    N = mmap_system.N
    M1 = mmap_system.M1
    M2 = mmap_system.M2

    f1_matrix = calc_f_1_matrix(mmap_system)
    f2_matrix = calc_f_2_matrix(mmap_system)

    def calc_zero_matrix(i):
        size = 0
        for r in range(0, N + 1):
            size += mh.calc_combinations(r + M1 - 1, M1 - 1) * mh.calc_combinations(N - r + M2 - 1, M2 - 1)
        return np.zeros(shape=(W * size, (i - 1) * W * size))

    return [np.vstack((
        np.hstack((f1_matrix, calc_zero_matrix(i))),
        np.kron(np.eye(i), f2_matrix)
    )) for i in range(2, R + 1)]


def calc_q_diagonal_blocks(mmap_system) -> List[np.ndarray]:
    """
    Function calculates list of Q_{i, i}, i=1..R of matrix Q

    Parameters
    ----------
    mmap_system

    Returns
    -------
    List[ndarray]
    """

    return [np.kron(
        np.eye(i + 1),
        calc_t_N_N_block(mmap_system)
    ) for i in range(1, mmap_system.R)]


def calc_q_r_r_block(mmap_system) -> np.ndarray:
    """
    Function calculate Q_{R,R} block

    Parameters
    ----------
    mmap_system

    Returns
    -------
    np.ndarray
    """

    return np.kron(
        np.eye(mmap_system.R + 1),
        calc_t_N_N_block(mmap_system, is_sum=True)
    )


def calc_q_above_blocks(mmap_system) -> List[np.ndarray]:
    """
    Function calculate list of Q_{i, i+1} i=0..R-1

    Parameters
    ----------
    mmap_system

    Returns
    -------
    List[np.ndarray]
    """

    matrices = []

    d1_matrix = mmap_system.d1_matrix
    d2_matrix = mmap_system.d2_matrix
    f_vector = mmap_system.f_vector
    q_vector = mmap_system.q_vector
    W = mmap_system.tilda_W
    R = mmap_system.R
    N = mmap_system.N
    M1 = mmap_system.M1
    M2 = mmap_system.M2

    def calc_zero_matrix(t):
        shape = 0
        for j in range(0, N + 1):
            shape += mh.calc_combinations(j + M1 - 1, M1 - 1) * mh.calc_combinations(N - j + M2 - 1, M2 - 1)
        return np.zeros(shape=(shape * W * (t + 1), shape * W))

    for i in range(1, R):
        left_diag_blocks = []
        right_diag_blocks = []
        left_eye_matrix = np.eye(i + 1)

        for r in range(0, N + 1):
            eye_matrix = np.eye(
                mh.calc_combinations(r + M1 - 1, M1 - 1) * mh.calc_combinations(N - r + M2 - 1, M2 - 1))

            left_diag_blocks.append(
                np.kron(f_vector[i] * d2_matrix, eye_matrix)
            )

            right_diag_blocks.append(
                np.kron(q_vector[i] * d1_matrix, eye_matrix)
            )

        left_matrix = np.hstack((
            np.kron(left_eye_matrix, matrix_helper.concat_diag_blocks(left_diag_blocks)),
            calc_zero_matrix(i)
        ))

        right_matrix = np.kron(
            matrix_helper.add_zeros_cols_left(left_eye_matrix, 1),
            matrix_helper.concat_diag_blocks(right_diag_blocks)
        )

        matrices.append(left_matrix + right_matrix)

    return matrices
