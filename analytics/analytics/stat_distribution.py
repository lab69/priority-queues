import numpy as np
from analytics.helpers import math_helper
from typing import List


def calc_stat_distribution(above_diag_matrices: List[np.ndarray],
                           diag_matrices: List[np.ndarray],
                           sub_diag_matrices: List[np.ndarray]) -> List[np.ndarray]:
    """
    Calculate stat distribution by arg matrices

    Parameters
    ----------
    above_diag_matrices: are matrices Q_{i}{i+1}
    diag_matrices: are matrices T and Q_{i}{i}
    sub_diag_matrices: are matrices Q_{i+1}{i}

    Returns
    -------
    List(ndarray)

        list of p_vectors, p_{i}, i = 0,R, where R - is size of buffer ins system
    """

    g_matrices = calc_g_matrices(above_diag_matrices,
                                 diag_matrices,
                                 sub_diag_matrices)

    q_highlighted_matrices = calc_q_highlighted_matrices(above_diag_matrices,
                                                         diag_matrices,
                                                         g_matrices)

    phi_matrices = calc_phi_matrices(q_highlighted_matrices, above_diag_matrices)
    p_0_vector = calc_p_0_vector(q_highlighted_matrices[0], phi_matrices)
    p_vectors = calc_p_vectors(p_0_vector, phi_matrices)

    p_vectors.insert(0, p_0_vector)

    return p_vectors


def calc_g_matrices(above_diag_blocks: List[np.ndarray],
                    diag_blocks: List[np.ndarray],
                    sub_diag_blocks: List[np.ndarray]) -> List[np.ndarray]:
    """
    Calculate list of g matrices

        G_{i} = (-Q_{i+1,i+1}-Q_{i+1,i+2}G_{i+1})^{-1} * Q_{i+1,i},  i = R-2..0

        G_{R-1} = (-Q_{R,R})^{-1} * Q_{R,R-1}

    Parameters
    ----------
    above_diag_blocks - above diag blocks of matrix Q, matrices Q_{i, i+1}, i = 0..R-1
    diag_blocks - diag blocks of matrix Q, matrices Q_{i, i}, i = 0..R
    sub_diag_blocks - sub diag blocks of matrix Q, matrices Q_{i + 1, i}, i = 0..R-1

    Returns
    -------
    List[np.ndarray]

    """
    R = len(diag_blocks)
    g_matrices = [np.dot(np.linalg.inv(-diag_blocks[-1]), sub_diag_blocks[-1])]

    for i in range(R - 2, 0, -1):
        g_matrices.insert(0, np.dot(
            np.linalg.inv(-diag_blocks[i] - np.dot(above_diag_blocks[i], g_matrices[0])),
            sub_diag_blocks[i - 1]
        ))

    return g_matrices


def calc_q_highlighted_matrices(above_diag_blocks: List[np.ndarray],
                                diag_blocks: List[np.ndarray],
                                g_matrices: List[np.ndarray]) -> List[np.ndarray]:
    """
    Function calculate Q highlighted matrices
    Only Q_{i}{i} highlighted,
    because Q_{i}{i+1} highlighted = Q_{i}{i+1}

    Parameters
    ----------
    diag_blocks - diag blocks of matrix Q, Q_{i, i}, i = 0..R
    above_diag_blocks - above diag blocks of matrix Q, Q_{i, i+1}, i = 0..R-1
    g_matrices

    Returns
    -------
    List[np.ndarray]

    """

    diag_q = [diag_blocks[i] + np.dot(above_diag_blocks[i], g_matrices[i]) for i in range(0, len(diag_blocks) - 1)]
    diag_q.append(diag_blocks[-1])

    return diag_q


def calc_phi_matrices(q_highlighted_matrices: List[np.ndarray],
                      above_diag_matrices: List[np.ndarray]) -> List[np.ndarray]:
    """

    Parameters
    ----------
    q_highlighted_matrices
    above_diag_matrices

    Returns
    -------
    List[np.ndarray]
    """

    matrices = [np.dot(above_diag_matrices[0], np.linalg.inv(-q_highlighted_matrices[1]))]

    for i in range(2, len(q_highlighted_matrices)):
        matrices.append(
            math_helper.dot(
                matrices[-1],
                above_diag_matrices[i - 1],
                np.linalg.inv(-q_highlighted_matrices[i])
            )
        )

    return matrices


def calc_p_0_vector(q_0: np.ndarray, phi_matrices: List[np.ndarray]) -> np.ndarray:
    """
    Function calculates p_{0} vector of stat distribution
    where p0 is solution of p0*Q0 = 0

    Parameters
    ----------
    q_0
    phi_matrices

    Returns
    -------
    np.ndarray
    """

    phi_shape = q_0.shape[0]
    phi_sum_row = np.zeros(phi_shape)

    for matrix in phi_matrices:
        phi_sum_row += matrix.sum(axis=1)

    answer = np.zeros(phi_shape)
    answer[-1] = 1
    single = np.ones(phi_shape) + phi_sum_row

    return np.linalg.solve(
        np.vstack((
            -q_0.transpose()[0: -1],
            single
        )),
        answer
    )


def calc_p_vectors(p_0_vector: np.ndarray, phi_matrices: List[np.ndarray]) -> List[np.ndarray]:
    """
    Function build list of p_{i} i = 1..N, where p_{i} is vector of
    size (i + 1) * W * sum(d_{r} * d_{N-r}, r=0..N)

    Calculation is by formulae

    P_{i} = P_0 * Fi_{i}

    Parameters
    ----------
    p_0_vector: vector of p_{0} state
    phi_matrices: list of matrices, calculated in function calc_phi_matrices

    Returns
    -------
    List[np.ndarray]

    """

    return [np.dot(p_0_vector, matrix) for matrix in phi_matrices]
