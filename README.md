Priority Queues
===============

This project contains simulation models and numerical experiments on studying of a priority queueing system with Markovian arrivals and service times.
